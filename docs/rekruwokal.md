---
id: rekruwokal
title: Rekrutacja na wokalistę
---

### Osoby decyzyjne
Obecnie rekrutację sprawdzają Velian (@_velian_) i Fran (@fran.oldie) – to one podejmują decyzję o nadaniu rangi.

### Przebieg rekrutacji
Podejście do rekrutacji wymaga przygotowania dwóch piosenek: piosenki energicznej oraz piosenki spokojnej. Inspiracji możesz szukać w bazie: [piosenki energiczne](https://www.youtube.com/playlist?list=PLS-W-_hma_d4IQpxbN7TWq5BDEMtxyokW)/[piosenki spokojne](https://www.youtube.com/playlist?list=PLS-W-_hma_d69Ni_KInFxeQAksDrIsw01).    
Możesz, ale NIE MUSISZ wybierać piosenki z bazy - została ona stworzona w celu rozwiania wątpliwości: "Czy ta piosenka jest wystarczająco energiczna/spokojna?".

Jeśli już wiesz, jakimi piosenkami chcesz się pochwalić, przygotuj do każdej piosenki:   
* surowy wokal   
* miks   
* linki do wersji oryginalnej   

Plik pierwszy, surowy wokal, to nagranie Twojego głosu (tylko i wyłącznie), które w żaden sposób nie było modyfikowane (bez postprodukcji, tj. nieodszumione, niewyrównane, bez nałożonych efektów etc.). Plikiem drugim jest miks, który MUSI ZAWIERAĆ WOKAL Z PIERWSZEGO PLIKU oraz podkład (to jest bardzo ważny warunek i musi zostać spełniony – nie przyjmujemy dwóch różnych próbek w obydwu plikach).   

> Piosenki powinny być wykonane po polsku i trwać minimum 1,5 minuty. Pliki możesz wysłać jako linki do hostingu, np. gdrive.   

Przygotowane materiały umieść w swoim poście rekrutacyjnym. Gdzie założyć taki wątek? Na Discordzie NanoKarrin, na forum "Rekrutacja NK".   

Wynik rekrutacji pojawi się w formie wiadomości w założonym przez Ciebie wątku. Jeśli nie odpiszemy po dwóch tygodniach, przypomnij się. Też jesteśmy ludźmi, mamy mnóstwo spraw na głowie, możemy po prostu zapomnieć.

### Jaki może być wynik rekrutacji?
##### Pozytywny
Otrzymujesz rangę Wokalisty NK. Posiadasz umiejętności wokalne oraz dobry mikrofon.

##### Negatywny
Nie otrzymujesz rangi Wokalisty NK. Rekrutacja może być odrzucona z trzech powodów:   
* brak umiejętności wokalnych
* brak dobrego mikrofonu (=jakość nagrań uniemożliwiająca branie udziału w projekcie)
* zamieszczone nagrania nie są zgodne z instrukcją (np. nagranie pierwsze mające zawierać surowy wokal zostało wyrównane)   

Powodzenia!
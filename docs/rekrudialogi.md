---
id: rekrudialogi
title: Rekrutacja na dialogistę
---

## Osoby decyzyjne
Obecnie rekrutację sprawdza nouwak (Discord: @nouwak) oraz Wiwi (@wiwiqueenofkiwi). To oni podejmują decyzję o nadaniu rangi.

## Przebieg rekrutacji
Wybierasz scenkę, która trwa co najmniej 3 minuty i zawiera sporą dawkę dialogów. Obraz scenki powinien zawierać w przeważającej ilości ruch ust postaci. Alternatywnie mogą to być dwie krótsze scenki o łącznym czasie ponad 3 minut.

Piszesz dialogi, zaznaczając postacie, które je mówią. Warto również odnotować czas (np. co scenę albo - jeśli mamy więcej zapału - w każdej linijce). W przypadku większych scenariuszy, gdzie funkcjonuje podział na sceny, można zaznaczyć jakie postaci grają w konkretnej scenie. Przykłady są w materiałach poniżej.

Pamiętaj, że - chociaż praca tłumacza-dialogisty opiera się przede wszystkim na dopasowywaniu tekstu pod ruch ust (tzw. kłapy) - jako tłumacze, musimy starać się, aby informację zawartą w oryginalnym dziele odpowiednio przenieść na nasz rodzimy język i dopasować do polskiego oglądu świata, a nie tylko bezmyślnie tłumaczyć. Czasem trzeba zmienić żart, aby był śmieszny dla szerszej publiczności albo pobawić się w łamigłówki językowe, zamiast dawać przypis "w oryginale się rymowało".

Swoją listę dialogową wraz z linkiem do oryginalnej scenki zamieść w swoim poście rekrutacyjnym na Discordzie. Twoja praca zostanie oceniona i, jeśli będzie taka potrzeba, dodamy komentarze do tłumaczenia. Twoim zadaniem będzie naniesienie poprawek. To też część rekrutacji. Dopiero po tej części podejmiemy decyzję o nadaniu rangi.   

W kwestii formatu pliku - wymagamy [Google Doca](https://drive.google.com/) i udostępnienia linku z uprawnieniami do komentowania. Dzięki temu każdy może otworzyć taki plik bez potrzeby instalacji odpowiedniego edytora tekstu, a komentarze dodatkowo pozwalają na w miarę zorganizowaną dyskusję i sygnalizowanie problemów w konkretnych miejscach scenariusza.



## Materiały

### Przykładowe scenariusze:

[Wersja optymalna](http://www.mediafire.com/download/7olhirsrwmfih2d/Z_kronik_legendarnych_bohaterow_ep_1_v2.pdf) (przejrzysta forma, zaznaczony czas scen i występowanie danych postaci w konkretnych fragmentach)

[Kolejny przykład](http://www.mediafire.com/download/9pvjjag8oqs4oa3/%27Kryzys_motywacyjny%27_Beelzebub_-_scenariusz.pdf) (tym razem scenka)

[Wersja minimalna](http://www.mediafire.com/download/m4txbunpbne28p5/Gai_Rei_Zero.rar) (w paczce ze ścieżką i scenką)

[Wersja hardcore](http://www.mediafire.com/download/n6dca7vfbgewkqk/Scenariusz_Working!!_ep_1.pdf) (zaznaczony czas każdej linijki, dodatkowo linijki ważniejszych postaci oznaczone są kolorami; zaznaczony jest również czas zakończenia danej linijki, ale jest to fanaberia autora [czyt. Dandrova ;P] i raczej odradzam, chyba że ktoś ma za dużo czasu xD).

## Jaki może być wynik rekrutacji?
### Pozytywny
Otrzymujesz rangę "Dialogista". Jest to eanga przydzielana rekrutom, którzy wykazali odpowiednie umiejętności w pisaniu tekstów. Scenariusze Dialogisty nie muszą być sprawdzane przez innego Dialogistę, choć jest to zalecana i powszechnie stosowana praktyka.

### Negatywny
Nie otrzymujesz rangi. Musisz jeszcze popracować nad warsztatem. Kolejną próbę możesz podjęć najwcześniej za miesiąc od momentu rozpatrzenia rekrutacji.

## Dwa słowa od opiekuna
Bardzo często rekrut dopiero rozpoczyna zabawę ze specyficznym rodzajem tłumaczenia, którym jest pisanie dialogów pod dubbing, dlatego też w "[Szkółce dialogowej](dialogiwstep)", którą przygotowaliśmy, znajdziesz materiały, które powinny wprowadzić Cię w temat. Serdecznie polecamy przejrzenie dostępnych lekcji i pisanie do nas z wszelkimi wątpliwościami.   

Nie bój się także prosić Dialogistów NK o sprawdzenie swoich próbnych scenariuszy przed przystąpieniem do rekrutacji. Ćwiczenie czyni mistrza :) Pamiętaj jednak, że właściwy scenariusz rekrutacyjny nie powinien być wcześniej przez nikogo sprawdzony!   
Wyślij go dopiero, kiedy poczujesz się na siłach.

---
id: rekruteksciarz
title: Rekrutacja na tekściarza
---

### Osoby decyzyjne
Opiekunami rekrutacji, czyli osobą sprawującą pieczę nad jej przebiegiem i  regulaminem, są Cruci (Discord: @cruci) oraz maseil (@maseil). 

### Przebieg rekrutacji
Załóż nowy post rekrutacyjny na Discordzie NK, na forum "Rekrutacja". Tam też możesz podejrzeć, jak robili to inni :) 

W post rekrutacyjny wklej dwa teksty piosenek wraz z demami. 
> Demo to nagranie tekstu piosenki zaśpiewanego (najlepiej przez Ciebie) pod podkład lub oryginał (pamiętaj, że muzyka nie może zagłuszać tekstu).
> Zamiast dema możesz dać link do gotowego wykonania piosenki,np. na YT.
Piosenki powinny się od siebie wyraźnie różnić (np. długością, tempem, nastrojem...) i trwać min. 1,5 min. każda (czyli minimalna długość to tzw. tv-size).

Tekst musi:
* być tłumaczeniem (na j. polski) piosenki zagranicznej; nie przyjmujemy autorskich tekstów, to nie ta rola
* być w 100% wykonany przez Ciebie i bez żadnej korekty
* trzymać się struktury akcentowej języka polskiego
* mieć zarys struktury rymowej
* mieć sens (zbliżać się do sensu oryginału, nie jego słów)
* brzmieć naturalnie
* być śpiewalny (czyli m.in. brak trudnych do wymówienia zbitek spółgłoskowych; tempo wymawiania głosek adekwatne do języka wyjściowego)

Wynik rekrutacji pojawi się w formie wiadomości w założonym przez Ciebie wątku. Jeśli nie odpiszemy po dwóch tygodniach, przypomnij się :). Też jesteśmy ludźmi, mamy mnóstwo spraw na głowie, możemy po prostu zapomnieć.

### Jaki może być wynik rekrutacji?
##### Pozytywny
Otrzymujesz rangę "Tekściarz". Jest to ranga przydzielana rekrutom, którzy wykazali odpowiednie umiejętności w pisaniu tekstów. Teksty Tekściarza nie muszą być sprawdzane przez innego Tekściarza, choć jest to zalecana i powszechnie stosowana praktyka.
##### Negatywny
Nie otrzymujesz rangi. Musisz jeszcze popracować nad warsztatem.

### Dwa słowa od opiekuna
Bardzo często rekrut dopiero rozpoczyna zabawę z tłumaczeniem melicznym, dlatego też w "Szkółce tekściarskiej", którą przygotowaliśmy, znajdziesz kilka prac i poradników, które mogą pomóc w procesie twórczym. Serdecznie polecamy przejrzenie dostępnych lekcji i pisanie do nas z wszelkimi wątpliwościami!  
Nie bój się także prosić Tekściarzy NK o sprawdzenie swoich próbnych tekstów przed przystąpieniem do rekrutacji. Ćwiczenie czyni mistrza :).  
Pamiętaj jednak, że właściwy tekst rekrutacyjny nie powinien być wcześniej przez nikogo sprawdzony!  
Teksty rekrutacyjne wyślij dopiero, kiedy poczujesz się na siłach.

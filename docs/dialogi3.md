---
id: dialogi3
title: Zaawansowane dopasowanie
sidebar_label: Dopasowanie
---

## Pauzy

Początkujący często traktują zdania, które postać mówi jednym ciągiem, nie będąc przerywana przez inną postać, jako jedną kwestię. Przykład:

> Właściwie to miałam nadzieję... Nie, nieważne. Bo byś się śmiał.

Sprawą dyskusyjną jest jak zdefiniujemy kwestię, jednak powyżej możemy zaobserwować trzy osobne fragmenty. Wyobraźcie sobie scenkę w której są wypowiedziane powyższe zdania. Najpierw pierwsze, niepewnie, urwane pod koniec. Cicho, niepewnie drugie, a po chwili ostatnie, być może lekko nadąsanym tonem. Dlatego te trzy części trzeba dopasować osobno. Nie można napisać czegoś w rodzaju:

>Tak właściwie... Nie, zapomnij o tym. Nie chcę, żebyś się ze mnie śmiał.

W takim przypadku przerwy w wypowiadanej kwestii (pomiędzy zdaniami) wypadną w zupełnie innych miejscach i nie będą zgadzać się z obrazem, co wywoła dysonans u widza (Co? Dlaczego teraz mówi z zamkniętymi ustami? O, a teraz nie mówi, a usta się ruszają!).

Jeśli chodzi o pauzy, najczęściej występują pomiędzy zdaniami, przy urwaniu wypowiedzi, niedopowiedzeniu albo podobnym zjawisku. Mogą, ale nie muszą występować tam, gdzie normalnie postawiony jest przecinek. Zauważ, że w mowie krótkie zdanie najczęściej jest wypowiadane jednym ciągiem (np. "Wiesz, że to nielegalne?"). Przecinki najczęściej porządkują logicznie strukturę zdania w tekstach pisanych. W mowie mają mniejsze znaczenie (chyba, że to tekst lektora albo narratora, który jest wypowiadany starannie i z zachowaniem wszelkich ę, ą i znaków interpunkcyjnych).

## Obraz a dźwięk

Krótko mówiąc, dialogi i ich poszczególne fragmenty powinny pasować do obrazu, tak by zgadzały się zarówno fragmenty, w których postać mówi, jak i te, w których robi chwilową przerwę.

Dlaczego mówię o obrazie? Ano bywa i tak, że oryginalne audio nie do końca zgrywa się z kłapami. A kto mówi, że pod pewnymi względami nie można być lepszym od oryginału? Warto na to zwracać uwagę przede wszystkim w mocno poszatkowanych wypowiedziach typu:
> Wiesz... chciałem... coś ci... powiedzieć... W sumie... to..

Wtedy widz najbardziej może zwrócić na to uwagę. W mało widocznych miejscach można pozwolić sobie na nieco większą swobodę, ale o tym za chwilę.
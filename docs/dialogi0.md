---
id: dialogi0
title: Technikalia
sidebar_label: Technikalia
---

Ten tekst przedstawi kilka spraw technicznych związanych z pisaniem scenariusza. Są to kwestie praktyczne i dość oczywiste. Jeśli sądzisz, że to już wiesz, możesz opuścić ten rozdział i przejść do następnego, w którym rozpocznie się już stricte nauka pisania dialogów.

## Format
Scenariusz musi być w jakimś formacie tekstowym. Warto jednak wykorzystać formatowanie, by zwiększyć czytelność, więc txt odpada. W praktyce standardem de facto stały się z czasem [Dokumenty Google](https://drive.google.com/).

Dlatego końcowa wersja scenariusza powinna być udostępniona właśnie w postaci dokumentu Google. Umożliwia to sprawną pracę nad dokumentem w wiele osób i łatwą korektę przez wbudowany system komentarzy. Dzięki temu aktorzy nie potrzebują żadnych aplikacji do otwarcia pliku, ani nie muszą nic pobierać, scenariusz jest dostępny online. 

Trzeba pamiętać o ustawieniu [pozwoleń](dialogigdrive) tak, by każdy dysponujący linkiem do dokumentu mógł go komentować (edycja nie jest potrzebna). W ten sposób jeśli aktor stwierdzi, że coś jest nie tak z jakąś kwestią, będzie mógł ją skomentować, a Ty otrzymasz powiadomienie i będziesz mógł/mogła ją poprawić.

## Formatowanie
W zasadzie dowolne, pod warunkiem, że będzie czytelne. Przede wszystkim warto logiczne jednostki scenariusza, np. sceny, oddzielać od siebie enterami lub odpowiednimi nagłówkami, używać prostej i wyraźnej czcionki, a nie jakiejś ozdobnej, ustawić interlinię na nieco większą, by uniknąć zbitych bloków tekstu i generalnie kierować się zdrowym rozsądkiem.

## Zawartość
Na początku powinien znajdować się tytuł scenki oraz link do filmu z oryginalną wersji scenki. Jest to ważne, ponieważ to według niej będą oznaczane czasy w dokumencie. Bez linku aktor może wyszukać scenkę w nieco innej wersji, w której nie będą się mu zgadzały czasy.

Następnie przydaje się lista występujących postaci. Można pokolorować je na różne kolory i w dialogach również kolorować ich kwestie, by odróżniały się od siebie wizualnie i aktor mógł je łatwiej wychwycić. Niestety problem powstaje przy dużej liczbie postaci, gdy trzeba wykorzystać wiele kolorów i niektóre są zbytnio do siebie podobne. Mimo wszystko polecam tę metodę.

Scenariusz, zwłaszcza dłuższy, powinien być podzielony na sceny. Jest to kwestia umowna, gdyż można różnie rozumieć granice scen, jednak najczęściej zmiana miejsca akcji bądź efekt przejścia (np. przez czerń) uznaje się za granice sceny.

Każda scena powinna być opisana czasami granicznymi (początku i końca). Pozwala to aktorom pojawiającym się tylko w niektórych scenach na łatwiejsze nawigowanie między nimi. Można również dopisywać czas do każdej kwestii, ale według mnie jest to zbyt czasochłonne i żmudne w stosunku do zysku, jaki zapewnia. Dodatkowo jeśli występują czasy każdej kwestii, aktorzy mają tendencję do skakania tylko do swoich kwestii i czasem nie poznają przez to pełnego kontekstu wypowiedzi.

Poszczególne kwestie powinny zaczynać się od imienia postaci, która je wypowiada. Jeśli postać mówi kilka zdań "naraz", jest to jedna kwestia. Jeśli pomiędzy nimi są większe przerwy, są to osobne kwestie i warto zaczynać je od nowej linii, by zaznaczyć ten fakt.

Jeśli postać robi pauzę w obrębie kwestii, zwłaszcza w miejscu, w którym normalnie przerwy by nie było, warto to zaznaczyć wielokropkiem albo innym symbolem, np. "Chciałem ci tylko powiedzieć... co mi leży na wątrobie." albo "Każdy z nich | kiedyś zostanie rycerzem.".

Jeśli masz dodatkowe wskazówki co do danej kwestii, możesz je napisać np. w nawiasach kątowych lub kwadratowych. Wybór nie jest ważny, ale warto się go trzymać, by był jednolity w całym dokumencie. Takie wskazówki mogą zawierać, np. informację, że daną kwestię trzeba zacząć mówić wcześniej niż w oryginale, albo jak umiejscowić poszatkowaną (dużo słów z wyraźnymi przerwami między sobą) kwestię w kłapach. To, co oczywiste dla dialogisty nie zawsze musi być oczywiste dla aktora. Nie ma potrzeby natomiast zaznaczać, jak aktor powinien zagrać daną kwestię (np. wesoło, poważnie), bo sam to wychwyci. Chyba, że jest to mocno nieoczywiste. Ale tak czy siak nadzorowanie gry aktorskiej jest raczej zadaniem reżysera niż dialogisty.

Dźwięki nieartykułowane, takie jak śmiech, sapanie czy okrzyki dobrze jest umieszczać w scenariuszu. Przydaje się to, żeby aktor nagrywając na podstawie scenariusza nie zapomniał ich nagrać. Nie trzeba natomiast dokładnie opisywać danego odgłosu. Aktor oglądający scenkę będzie i tak wiedział, jaki odgłos z siebie wydać :) Warto natomiast zaznaczyć we wskazówce dla aktora, jeśli któryś z takich odgłosów chcesz zastąpić kwestią (bo np. coś się nie mieści i można wykorzystać dodatkowy czas, gdy usta są otwarte).
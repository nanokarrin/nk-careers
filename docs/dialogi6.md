---
id: dialogi6
title: Upiększanie
sidebar_label: Upiększanie
---

## Stylizacja

Zabieg, który pozwala dodać postaciom nieco kolorytu. Być może już w oryginale wykazuje jakąś manierę językową, wówczas należy ją odpowiednio przenieść w tłumaczeniu. Być może postać mówi normalnie, ale jest na tyle specyficzna, że np. masz wrażenie "powinien mówić jak żywcem wyjęty z Pana Wołodyjowskiego". No to niech tak mówi!

Poniżej kilka przykładów. Oczywiście nie są to jedyne słuszne zabiegi. Każdy pomysł jest dobry, o ile pasuje do postaci.

Ktoś dodaje "nanodesu" na końcu każdego zdania? Można zmiękczać słowa, by oddać "uroczość" wypowiedzi.

>Jaki piękny koteczek! Taki milusi!

"Watch'ya lookin' at?" Mamy tu gangstera? Może grypsować.

>Czego gały wytrzeszczacie?

Uduchowiony mnich? Nikt mu nie broni posługiwać się archaizacją biblijną.

>Zaprawdę powiadam wam, jadło to nasyciło me ciało.

Małe zastrzeżenie odnośnie stylizacji, a zwłaszcza stosowania gwary, np. śląskiej. Musi być zrozumiała. Stylizacja służy podkoloryzowaniu postaci i nie może doprowadzić do tego, że nie będzie się dało jej zrozumieć. Prawdziwy Ślązak może się zżymać słysząc jakąś postać lekko podstylizowaną na gwarę i twierdzić, że przecież nikt tak nie mówi, ale dzięki temu reszta ludzi nie będzie zniechęcona, nie wiedząc, o czym mowa (no chyba, że taki był cel zabiegu, wówczas jest to usprawiedliwione).

## Żarty

Ciężki temat. Żarty są praktycznie nieprzekładalne z języka na język. A już najgorzej z grami słów, te są właściwie beznadziejnym przypadkiem.

Jedynym rozwiązaniem jest praktycznie wymyślanie nowego żartu, albo stworzenie innej gry słów, która będzie mieć sens w języku polskim.

Jeśli chodzi o żarty to nie jest tak źle, bo istnieje masa stron, na których można znaleźć krótkie dowcipy na różne okazje.

Gorzej jest z grami słów, te są zwykle tak ściśle dopasowane do sytuacji i konkretnych słów, że ciężko znaleźć gotowe rozwiązania, raczej trzeba je wymyślać samemu.

>I don't want people to think I'm a loose woman. - Wcześniej była mowa o tym, że mundurek jest luźny, rozchełstany (loose). Żart polega na drugim znaczeniu - loose woman - kobieta lekkich obyczajów.

>Nie chcę, żeby pomyśleli, że jestem rozwiązła. - Próba oddania. Może oznaczać jakieś rozwiązanie się sznurówek czy czegoś, a z drugiej strony mamy rozwiązłość. Nie idealne, ale jako tako działa.

## Polskie realia

Anime, czy zachodnie kreskówki przedstawiają zupełnie inną kulturę i otoczenie. Czasem trzeba zamienić pewne elementy występujące w dialogach, które odnoszą się do innych stron, np. nazwisk polityków, na odpowiedniki z Polski, żeby widz zrozumiał, o kim właściwie mowa. Przykładowo wzmiankę o znanym amerykańskim celebrycie można zastąpić znanym polskim celebrytą. Taki zabieg nazywa się lokalizacją.

Sztandarowym przykładem w tym aspekcie jest Shrek, przetłumaczony przez Bartosza Wierzbiętę i obfitujący w arsenał różnych elementów, które zostały zamienione na takie, które są dobrze znane Polakom. Przykładowo nazwa jakiegoś deseru została zmieniona na "torcik wedlowski".

Ciężko jest to zawsze zastosować, tym bardziej jeśli coś, co chcieliśmy zlokalizować, jest widoczne na ekranie. Ale tam, gdzie będzie brzmiało naturalnie, jak najbardziej.

Powyższa kwestia jest nieco kontrowersyjna. Niektórzy uważają, że należy pozostawić wszelką orientalność i obcokrajowość, bo tego pragnie widz. Ja uważam, że widz powinien przede wszystkim zrozumieć, o czym mowa. Tak czy siak, lokalizacja to nie wymaganie, a raczej moja sugestia.

## Nazwy wlasne

Tu też pojawiają się kontrowersje. Jeśli chodzi o tłumaczenie nazw własnych, nie ma jedynej słusznej odpowiedzi. Jedni tłumaczą, inni uznają to za niedopuszczalne.

Ja raczej wolę tłumaczyć. Według mnie dużo klimatyczniej brzmi "Tawerna pod Kuternogą" niż "Tavern 'Pegleg'". Tak czy siak - decyzja należy do tłumacza, czyli w tym przypadku Ciebie :)

Choć przy nazwach ataków można się zastanowić. Często oryginał jest japoński, ale nazwy ataków są po angielsku, więc można je również zostawić, żeby zachować tą "obcojęzyczność" w stosunku do oryginału.

Staraj się też zachować spójność. Jak już tłumaczysz nazwy własne, to wszystkie, a nie tylko niektóre. Ewentualnie zdefiniuj konkretne kategorie wraz z dotyczącymi ich regułami, np. tłumaczysz nazwy miejsc, ale nazwy ataków już nie.
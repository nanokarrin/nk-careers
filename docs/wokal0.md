---
id: wokal0
title: Wstęp
sidebar_label: Wstęp
---

Odwiedziłeś właśnie zakątek, który - mam nadzieję! - pomoże Ci rozwinąć swoje umiejętności wokalne. Pamiętaj, że na śpiewanie składa się wiele elementów. Prawidłowy oddech, higiena głosu. Dobra dykcja to również element dobrego śpiewania. Odpowiednie rozśpiewanie - o tym będzie osobna lekcja - to bardzo ważny element przygotowania się do śpiewania, o którym należy pamiętać. Zawsze.

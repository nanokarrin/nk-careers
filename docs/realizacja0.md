---
id: realizacja0
title: Ty też możesz zostać realizatorem dźwięku!
sidebar_label: Wstęp
---

## Czy chcesz nauczyć się jak zmienić swoje nagrania brzmiące tak:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYueFJQZXIybnp6ZFU/preview?usp=sharing">
</iframe>


## Na nagrania które brzmią tak!?

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuY1pGSTdHTXRaVDg/preview?usp=sharing">
</iframe>

## Czy chcesz nauczyć się miksować sprawniej żeby zapobiec niekończącej się pętli zmian i edycji w swoich miksach!?
![](/assets/perfect.jpg)

## Czy marzysz o tym żeby rozumieć memy o realizacji dźwięku!?
![](/assets/hipass.jpg)

![](/assets/busscomp.jpg)

![](/assets/brostudio.jpg)

<br></br>

## Jeżeli tak... to Kurs Realizatorów Fandubbingowych i inne artykuły w szkółce realizacji dźwięku są dla Ciebie!

<br></br>

## Spis treści:
♬ [Słowniczek realizatora](realizacja1)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Przegląd procesu nagrywania do komputera](/docs/realizacja1#1-przegląd-procesu-nagrywania-do-komputera)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Audio Interface](/docs/realizacja1#2-audio-interface)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [AUX Send](/docs/realizacja1#3-aux-bleed)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Bleed](/docs/realizacja1#4-bleed)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Boost](/docs/realizacja1#5-boost)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Bounce](/docs/realizacja1#6-bounce)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Bus](/docs/realizacja1#7-buss)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Chórki](/docs/realizacja1#8-chórki)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Clipping](/docs/realizacja1#9-clipping)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Cut](/docs/realizacja1#10-cut)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [DAW](/docs/realizacja1#11-daw)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Dry](/docs/realizacja1#12-dry)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Eksport Audio](/docs/realizacja1#13-eksport-audio)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [EQ](/docs/realizacja1#14-eq)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Fade in/out](/docs/realizacja1#15-fade-inout)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Grupówka](/docs/realizacja1#16-grupówka)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Headroom](/docs/realizacja1#17-headroom)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Korektor Parametryczny](/docs/realizacja1#18-korektor-parametryczny)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Mastering](/docs/realizacja1#19-mastering)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Mikrofon](/docs/realizacja1#20-mikrofon)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Mixing](/docs/realizacja1#21-mixing)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [MP3](/docs/realizacja1#22-mp3)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Panorama](/docs/realizacja1#23-panorama)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Pasmo Częstotliwościowe](/docs/realizacja1#24-pasmo-częstotliwościowe)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Plugin](/docs/realizacja1#25-plugin)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Pop Filter](/docs/realizacja1#26-pop-filter)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Preset](/docs/realizacja1#27-preset)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Produkcja](/docs/realizacja1#28-produkcja)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Projekt Grupowy](/docs/realizacja1#29-projekt-grupowy)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Proximity Effect](/docs/realizacja1#30-proximity-effect)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Przester](/docs/realizacja1#31-przester)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Reverb](/docs/realizacja1#32-reverb)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Reżyser Piosenek](/docs/realizacja1#33-reżyser-piosenek)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [RMS Loudness](/docs/realizacja1#34-rms-loudness)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Sibilance](/docs/realizacja1#35-sibilance)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Wav](/docs/realizacja1#36-wav)  
♬ [Kurs Realizatorów Fundubbingowych (KRF)](realizacja2)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Co robić przed rozpoczęciem miksowania?](/docs/realizacja2#co-robić-przed-rozpoczęciem-miksowania)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Miksowanie cz. 1](/docs/realizacja2#miksowanie-cz-1)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Miksowanie cz. 2](/docs/realizacja2#miksowanie-cz-2)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Mastering](/docs/realizacja2#mastering)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Post-produkcja – miks cz. 1](/docs/realizacja2#post-produkcja---miks-cz-1)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Post-produkcja – miks cz. 2](/docs/realizacja2#post-produkcja---miks-cz-2)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Post-produkcja – master](/docs/realizacja2#post-produkcja---master)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Demo miksowania piosenki](/docs/realizacja2#demo-miksowania-piosenki)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Przygotowanie balansu głośności](/docs/realizacja2#przygotowanie-balansu-głośności)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Edycja nagrań przed miksem](/docs/realizacja2#edycja-nagrań-przed-miksem)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Proces miksowania cz. 1](/docs/realizacja2#proces-miksowania-cz-1)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Proces miksowania cz. 2](/docs/realizacja2#proces-miksowania-cz-2)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Odsłuch końcowego miksu](/docs/realizacja2#odsłuch-końcowego-miksu)  
♬ [Kompresja, Limitacja, Rewerberacja. Czyli, jak przystąpić do obróbki nagrań wokalnych?](realizacja3)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Kompresor](/docs/realizacja3#presets---wokal)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Presets](/docs/realizacja3#presets)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Presets - wokal](/docs/realizacja3#presets---wokal)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Limiter](/docs/realizacja3#limiter)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Reverb](/docs/realizacja3#reverb)  
♬ [Czyszczenie sybilantów i innych brudów](realizacja4)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Czyszczenie sybliantów](/docs/realizacja4#czyszczenie-sybliantów)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Czyszczenie brudów](/docs/realizacja4#czyszczenie-brudów)  
♬ [Materiały treningowe](realizacja5)  

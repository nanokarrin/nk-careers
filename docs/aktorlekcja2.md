---
id: aktorlekcja2
title: Dykcja
sidebar_label: Dykcja
---

W pracy aktora głosowego nie liczą się jedynie uczucia. Dykcja jest również **bardzo ważna**. Poniżej zamieszczone jest kilka zadań i porad, które na pewno ułatwią Ci życie. Pamiętaj, że systematyczność i trening to klucz do sukcesu. 5% to talent, a 95% to ciężka praca.

## Zadanie 1
Spróbuj przeczytać na głos zamieszczone poniżej tak zwane „wprawki-głupawki”. Dbaj o to, żeby każda głoska pięknie wybrzmiała – nie mamrocz, nie bełkocz. Możesz spróbować się nagrać lub poproś kogoś, żeby Cię wysłuchał. Jeśli bez problemu wymówiłeś te zdania, to najprawdopodobniej masz dobrą dykcję.
* Pop popadii powiada, że chłop pobił sąsiada.
* Trzmiel na trzosie w trzcinie siedzi, z trzmiela śmieją się sąsiedzi.
* Warszawa w żwawej wrzawie w warze wrze o Warszawie.
* Król królowej tarantulę włożył czule pod koszulę.
* Truchtem tratują okrutne krowy Rebeki mebel alabastrowy.
* Dromader z Durbanu turban pożarł panu.
* Teatr gra w grotach Dekamerona, grom gruchnął w konar rododendrona.
* Powstały z wydm widma, w widm zwały wpadł rydwan.
* Puma z gumy ma fumy, a te fumy to z dumy.
* Przy karczmie sterczy warsztat szlifierczy.
* Cienkie talie dalii jak kielichy konwalii.
* Pan ślepo śle, panie pośle!
* Taka kolasa dla golasa to jak melasa dla grubasa.
* Drgawki kawki wśród trawki - sprawką czkawki te drgawki.
* Naiwny nauczyciel licealny nieoczekiwanie zauważył nieostrożnego ucznia, który nieumyślnie upadł na eukaliptus.
* Akordeonista zaiste zainteresował zaufanego augustyniallusa etiudą.
* Koala i boa automatyzują oazę instalując aerodynamiczny aeroplan.
* Paulin w Neapolu pouczył po angielsku zainteresowanego chudeusza, co oznaczają rozmaite niuanse w mozaice.
* Augustyn przeegzaminował Aurelię z geografii, próbując wyegzekwować wiadomości o aurze Australii i Suezie.
* Euforia idioty zaaferowała jednoosobowe audytorium uosobionego intelektualisty.
* Nieoczekiwany nieurodzaj w Europie oraz nieumiejętna kooperacja państw zaalarmowały autorów przeobrażeń.
* Krab na grab się drapie, kruchą gruchę ma w łapie.
* Marne piwo browarne z browaru na Ogarnej - a mówiły kucharki, że najlepsze jest z Warki.
* Podrapie wydra pana brata na trapie.
* Do jutra - burto kutra!
* Pewien dżudok w walce dżudo posiniaczył czyjeś udo.
* To jest wir, a to żwir, w żwiru wirze ginie zbir!
* Żagiel w łopot wpadł. To kłopot!
* Raz lew wpadł w zlew, wprost w wody ciek, i nim kleń-leń wypchnął go zeń, popłynął w ściek!
* To jest stuła - z Tuły stuła, a tu świeżo kwitnie świerzop.
* Raz dywizja telewizji pomagała szukać wizji, a znalazłszy ją w Kirgizji, domagała się prowizji.
* Krążownik przeciął kutra trawers i kuter ma kurz tylko awers!
* Czy szczególny to, proszę pana, zaszczyt, płaszczyć się, by wtaszczyć się na szczyt?
* Czyjeś jelita widzi elita - cóż to za efekt, ten brzuszny defekt!

## Zadanie 2
Polega na nagrywaniu raz dziennie cztery poniżej zamieszczone wierszyki. Staraj się szeroko otwierać usta i z każdym dniem spróbuj mówić je coraz szybciej. Dlaczego jest istotne, żebyś je nagrał? Dzięki temu będziesz mógł zobaczyć, jak bardzo, na przykład, w ciągu tygodnia poprawiłeś swoje umiejętności.

1. **Wiersz „Zbereźnicy ze Zbaraża”**
 <p className="indent">Zbereźnikom ze Zbaraża<br/>
 zaraźliwy zez zagraża:<br/>
 zje, zuch, zwykły zraz zbaraski -<br/>
 zaraz zieją zeń zarazki;<br/>
 zjadacz zrazu, zrazu zdrów -<br/>
 zezem zarażony znów!<br/>
 Zdaniem znawców zagadnienia,<br/>
 ze zwykłego zagapienia<br/>
 zbarażanin zbaraniały<br/>
 (znaki zawsze zabraniały<br/>
 zajadania zrazów!) zrazy<br/>
 zeżre, zdrowe zaś zakazy –<br/>
 „ZRAZ (ZRAZ ZWłASZCZA ZAWIJANY)<br/>
 ZASADNICZO ZAKAZANY!” –<br/>
 zlekceważy. Zatem: zaliż<br/>
 znak zakazu znowu zwalisz,<br/>
 zbirze ze Zbaraża, zjesz<br/>
 zraz złowrogi? – (Zeżre, zwierz.)</p>

2. **Wiersz „Szczezł pszczelarz z Pszczyny”**
 <p className="indent">Chrzestny Krzysztofa pszczelarz z Pszczyny<br/>
 Krzysiowi schrzanił krztynę chrzciny<br/>
 nie wtchrząknął wszka przystawek trzech<br/>
 gdy chrząstkę w szynce przyniósł pech<br/>
 a szynka przyszła aż z Przasnysza<br/>
 przechrząstkowana któż to słyszał<br/>
 najgorszej przysporzyła z bied<br/>
 bo zakrztuszony chrzestny zszedł<br/>
 a po nieszczęściu jak czcić chrzciny<br/>
 gdy szczezł przez chrząstkę pszczelarz z Pszczyny<br/>
 wszczęty tym wstrząs do spazmów klucz<br/>
 zewoluował w wytrzeszcz ócz<br/>
 zaprzepaszczone poncz i mięsa<br/>
 któż wskrzesi chęć choć na krztę kęsa<br/>
 a Krzyś w pieluchach wciąż się śmiał<br/>
 więc tusz że w nich susze miał</p>

3. **Wiersz „Jamnik”**
 <p className="indent">W grząskich trzcinach i szuwarach<br/>
 kroczy jamnik w szarawarach,<br/>
 szarpie kłącza oczeretu<br/>
 i przytracza do beretu,<br/>
 ważkom pęki skrzypu wręcza,<br/>
 traszkom suchych trzcin naręcza,<br/>
 a gdy zmierzchać się zaczyna<br/>
 z jaszczurkami sprzeczkę wszczyna,<br/>
 po czym znika w oczerecie<br/>
 w szarawarach i berecie...</p>

4. **Wiersz „Chrząszcz”**
 <p className="indent">Trzynastego, w Szczebrzeszynie<br/>
 chrząszcz się zaczął tarzać w trzcinie.<br/>
 Wszczęli wrzask Szczebrzeszynianie:<br/>
 „Cóż ma znaczyć to tarzanie?!<br/>
 Wezwać trzeba by lekarza,<br/>
 zamiast brzmieć, ten chrząszcz się tarza!<br/>
 Wszak Szczebrzeszyn z tego słynie,<br/>
 że w nim zawsze chrząszcz brzmi w trzcinie!”<br/>
 A chrząszcz odrzekł niezmieszany:<br/>
 „Przyszedł wreszcie czas na zmiany!<br/>
 Drzewiej chrząszcze w trzcinie brzmiały,<br/>
 teraz będą się tarzały.”</p>

## Zadanie 3
Ćwicz po kolei każdy wariant dykcji.

### **Samogłoski**
Ćwiczenia wyrazów samogłoskowych. Czytaj wyrazy utworzone z samogłosek, zachowując akcent i długość wyrazu.
* kolega – oEa
* gatunkowy – auOy
* wyleniała – yeAa
* łata – Aa
* tatulo – aUo
* lokata – oAa
* tataraki – aaAi
* kita – Ia
* Zbrodnia to niesłychana – Oa o eyAa
* Pani zabiła pana – Ai aIa Aa

### **Samogłoski nosowe**
Wymawiaj następujące wyrazy, zwracając uwagę na to, aby wargi nie wysuwały się do przodu jak do głoski „ł”:
* kęs, chcę, kąsa, kęs-kąsa itd., chcą-są, chcą-kęs, nie chcę kąsać

### **Spółgłoski**
Ł – Wymawiaj starannie, z wysunięciem warg do przodu.
 * płaski, płaszcz, gładzi, głupi, głowa, biegał, miał, zaludnił, uczuł
 * Mówiły jaskółki, że niedobre są spółki
 * Małpy skaczą niedościgle, małpy robią małpie figle
 * Spustoszył Afrykę, uszkodził auto, ukarał Adama, zabrał owies
 * <p className="indent">„Płyną biali obłokowie.<br/>
  Który z nich mi słowo powie,<br/>
  Słowo lotne, światłe słowo,<br/>
  Cichą prawdę obłokową?” ~ B. Ostrowska</p>
 * <p className="indent">„Już jaskółki<br/>
  Odleciały<br/>
  I skowronki<br/>
  Jesień w drogę<br/>
  Je wysłała<br/>
  W ślad za słonkiem” ~ A. Nosalski</p>

* Czytając zwracaj uwagę na grupy typu au-ał:
 * ał – auto, pauza, aura, audycja, Kaukaz, eutanazja, aut, skaut, faun, Zeus, Europa, Paula
 * au – nauka, nauczyciel, zaułek, zaufanie, naumyślnie
 * <p className="indent">„Głuchego głuchy pozwał przed głuchego sędzię.<br/>
  Głuchy w krzyk: „Oddaj krowę, wtedy zgoda będzie”<br/>
  „Jakże to?” – zawołał drugi – „Czy długo czy niedługo,<br/>
  Ale do dziada mego należał ten ugór!”<br/>
  Na to sędzia z wyrokiem: „Dziewka – lada jaka<br/>
  Lecz by nie siać zgorszenia, ożenić chłopaka!”” ~ Puszkin</p>

H
* dach chaty, strach Haliny, brzuch chory, duch huzara, niech chuchnie
* <p className="indent">„Raz śpiącą babcię straszył duch<br/>
  lecz babcia była głucha<br/>
  więc chociaż starał się za dwóch<br/>
  nic się nie bała ducha” ~ A. Marianowicz</p>

### Spółgłoski podwojone wewnątrz wyrazów
Wymowa płynna:
* codzienny, senny, ballada, terror, gamma, panna, Aaron, Kaaba, Jaffa, passat
Pierwsza głoska słabiej:
* oddech, lekko, miękko

### Rozziew samogłoskowy i zbitki spółgłoskowe
Wymowa rozdzielna:
* rozziew samogłoskowy typu:
 * ta alegoria, na ulicy, nie umie, te ekscesy, koło ambasady, oni imponują, mały interesant, przy ulu, ci ignoranci, to okno, o okolicy, tu uwiera
* zbitki spółgłoskowe typu:
 * brat taty, jest ten, wiek klęski, bal lalek, stuk kołowrotka, skok kotka, on należał, patrol lokalny, ból ludzki, ton nudy, król leniwy, but turysty, welon narzeczonej, sól lecznicza, neon na ulicy

Wymowa grup: trz, strz, wstrz, drz. Wymawiaj poprawnie:
* trzon, trzask, trzeba, trzymać, strzelba, strzec, strzecha, jątrzyć, wstrzymać, trzpień, trzpiot, trzcina, drzewo, drzazga, drzemka, drzwi
* wypaczył – wypatrzył, po wieczne – powietrzne, wieczny – wietrzny, szczyt – strzyc, zaczyna - zatrzyma, z Czech – z trzech, z czego – strzegą,
* Trzech Czechów milczy mil trzy.
* Czego trzeba strzelcowi do zestrzelenia cietrzewia drzemiącego w dżdżysty dzień na drzewie?
* Wstrząs wstrząsnął strzelistą kolumną.
* Trzydzieści trzy przyczyny.
* Czy są trzy jajka?

### Dźwięczność
Na końcu wyrazu głoski dźwięczne wymawiaj bezdźwięcznie:
* mózg, stóg, róg, zrób, wódz, rów

W wymowie scenicznej dba się o to, aby nie udźwięczniać głosek wygłosowych wyrazów poprzedzających samogłoski i głoski półotwarte (r, l, ł, m, n, ń):
* brat ojca, weź list, postaw rower, wylew Odry, twarz oszusta, postaw rower,

### Akcent
Zaznacz akcent i przeczytaj wyrazy:
* zapomnieliśmy, wprowadziliby, zrobili, pochowaliście, wymyłyście, matematyka, matematykami, biologia, Ameryka, Afryka, amerykański, afrykański, logika, fonetyka, praktyka, fizyka, pięćset, pięciuset, siedemset, siedmiuset, rzeczpospolita, opera, nauka, nabój, na bój, zator, za tor, aleja, ale ja

Przeczytaj wiersz, akcentując poprawnie:
<p className="indent">Pewien żarłok nienażarty<br/>
Raz wygłodniał nie na żarty<br/>
I wywiesił szyld na płocie<br/>
Że ochotę ma na płocie.<br/>
Tutaj na brak ryb narzeka,<br/>
Bo daleko rybna rzeka.<br/>
Więc się zgłosił pewien żebrak<br/>
I rzekł żarłokowi, że brak<br/>
Płoci, karpi oraz śledzi<br/>
Ale rzeki pilnie śledzi<br/>
I gdy tylko będzie w stanie<br/>
To o świcie z łóżka wstanie,<br/>
Po czym ruszy na Pomorze<br/>
I w zdobyciu ryb pomoże…<br/>
Odtąd żarłok nasz jedynie<br/>
Zamiast smacznych ryb je dynie.</p>

## Zadanie 4
Na koniec coś dla zabawy.
1. Naśladuj warkot silnika motocyklowego na różnych wysokościach dźwięku. Najpierw używaj jedynie warg, potem języka, na koniec warg i języka. Warto parsknąć.
2. Kilkakrotnie jak najgłośniej przeciągle cmoknij.
3. Kilka razy szeroko otwórz i zamknij usta, układając je w pozycji poszczególnych samogłosek:
 * „a”, „o”, „u”, „e”, „i”, „y” – tym razem rób to głośno.
4. Głośno i wyraźnie wypowiadaj połączenia:
 * „abba, obbo, ubbu, ebbe, ibbi, ybby”
 * „assa, osso, ussu, esse, issi, yssy”
5. Wybierz sobie różne spółgłoski i łącz je z samogłoskami według powyższego wzoru. Każda głoska musi być słyszalna.
6. W różnym tempie wypowiedz:
 * „da-ta-za-sa-dza-ca-na-ła”
 * „di-ti-zi-si dzi”
 * „zia-sia-dzia-cia-nia”
 * „ga-ka-ha-cha”
 * „gia-kia-hia-chia”
7. W różnym tempie powiedz:
 * „brim, bram, bram, bram, brom”
 * „trim, tram, tram, tram, trom”
 * „krim, kram, kram, kram, krom"
 * itd.
---
id: dzwiekdialogi
title: Podstawy pracy nad dialogami
sidebar_label: Podstawy dialogów
---

:::note
Autor: Filipus Magnus
:::

## Od czego zacząć?

No przede wszystkim potrzebujesz programu, w którym będziesz mógł obrabiać dźwięk, jednocześnie mając wgląd do obrazu. Jeśli masz już swój ulubiony program do montażu wideo, który oferuje podstawowe narzędzia do obróbki audio, np. Da Vinci Resolve, Adobe Premiere, Sony Vegas itd. to możesz na spokojnie działać w nim. Jeśli gardzisz półśrodkami, warto zaopatrzyć się w **DAW** czyli program przeznaczony stricte do obróbki audio. Cóż, w tym wypadku masz możliwość użycia czegoś więcej niż podstawowe narzędzia, a mianowicie możesz dołożyć wtyczki VST, które nie zawsze są w pełni wspierane przez edytory wideo, a dzięki którym możesz uzyskać ciekawszy efekt, ale o tym innym razem.

Cały proces przedstawię na przykładzie programu Reaper (jeden z DAW-ów), bo sam go używam przy obróbce audio [(jeśli zdecydowałeś się na repera to tu masz link jak włączyć w nim wyświetlanie obrazu)](https://forum.cockos.com/showthread.php?t=170860).

## Odpaliłem program i co dalej

Teraz na jedną ścieżkę wrzucasz oryginalny projekt i tworzysz osobne ścieżki dla każdej postaci. Oryginalny projekt zapewnia podgląd wideo i możliwość porównywania twojej wersji z oryginalną. Przycisk **M** wycisza oryginalną ścieżkę, a **S** pozwala słuchać tylko jej.

![coś takiego](https://imagizer.imageshack.com/img922/7467/s1puK8.png)

Teraz czas żebyś wrzucił nagrania aktorów i ustawił je pod **"kłapy"**, czyli żebyśmy usłyszeli postać dokładnie wtedy, gdy rusza ona "kłapami".

Gdy to już mamy gotowe, warto na ucho wyrównać głośność aktorów. W późniejszym etapie warto będzie sugerować się programowym pomiarem głośności.

![pomiar głośności](https://imagizer.imageshack.com/img924/4016/DNO8Oy.png)

Jednakże teraz się nim nie sugeruj, tylko wyrównaj ręcznie na czuja. Co prawda ten krok można w wielu przypadkach pominąć, jednak to działanie bardziej ci pomoże niż zaszkodzi ;)

## To co dalej?

Bardzo możliwe, że w twojej głowie pojawiła się odpowiedź o treści *"odszum to, tak jak w audacity"*. Cóż, to rozwiązanie stosuje się w ekstremalnych przypadkach, gdy szum w nagraniu autora jest bardzo dokuczliwy. W większości przypadków wystarczy efekt **bramki szumów**, nazywany **gate** lub **noise gate**. Ten efekt ma za zadanie wyciszać dźwięki, które są ciszej niż wskazany przez nas poziom. Musimy ustawić bramkę tak, by tworzyła ciszę w momentach, gdy aktor nic nie mówi, ale żeby również nie zjadała fragmentów jego wypowiedzi i końcówek słów.

![bramka](https://imagizer.imageshack.com/img922/3507/mmGaOu.png)

## To już wszystkie brudy wyczyszczone

Niestety nie. Warto pozbyć się jeszcze dźwięków mieszczących się w częstotliwościach, których żaden człowiek nie jest w stanie z siebie wydobyć. Do tego użyjemy **filtru górnoprzepustowego** nazywanego **highpass filter**. Jeśli przyjrzysz się reaperowej bramce to zauważysz, że posiada ona coś takiego. Dobra, to jak tego użyć? W prawie każdym przypadku bezpieczną wartością jest 100Hz, jest niewielka szansa na to, że którykolwiek z aktorów wyda z siebie niższy dźwięk. Jeśli twój program nie posiada takiego filtra, możesz sobie poradzić za pomocą **korektora graficznego** nazywanego **equalizer**. Tak jak na ilustracji niżej, ściszasz dźwięki poniżej 100Hz.

![equalizer](https://imagizer.imageshack.com/img923/3325/KZdy4f.png)

Za pomocą equalizera możemy pozbyć się też innych brudów, ale to temat na inny poradnik. I jeśli musisz odszumić nagranie, to rób to po equalizacji, lepszego momentu nie ma.

## Czas na równańsko głośności

Z pewnością rzuciło ci się na uszy, że nagrania aktorów w jednym momencie są ciche, a w innym głośniejsze. Zmusza to słuchacza do żonglowania suwakiem głośności. Aby rozwiązać ten problem musimy zmniejszyć **zakres dynamiki dźwięku**, czyli różnicy między najcichszym a najgłośniejszym dźwiękiem. **TYLKO PAMIĘTAJ BY NIE PRZEGIĄĆ PAŁY W DRUGĄ STRONĘ, BO TO TEŻ BĘDZIE NIEPRZYJEMNE W ODSŁUCHU** (mówię to jako osoba, która lubi mocną kompresję).

Czym to się robi? **Kompresorem**. To narzędzie będzie wymagało od ciebie zabawy dwoma wartościami: **threshold**, czyli poziomem od którego kompresor zacznie działać, oraz **ratio** czyli siłą kompresji. W uproszczeniu sygnały głośniejsze niż wyznaczony przez nas **threshold** będą ściszane.

![kompresor](https://imagizer.imageshack.com/img922/8349/SJdpie.png)

## Czy coś jeszcze się robi?

Jeśli sytuacja tego wymaga, czyli chcemy podkreślić w jakim pomieszczeniu znajdują się bohaterowie, lub zaznaczyć że dana kwestia jest wypowiadana w myślach, wtedy stosujemy **pogłos**, nazywany również **reverb**. Jeśli tak jak ja korzystasz z reapera, to olej domyślną wtyczkę i ściągnij np. [orilriver](https://www.kvraudio.com/product/orilriver-by-denis-tihanov).

Zachęcam do wnikliwej zabawy suwaczkami, jednakże na początek najważniejsze są **room size** czyli rozmiar pomieszczenia (LOL), ** decay time** czyli czas pogłosu i **wet** czyli głośność pogłosu.

![pogłos](https://imagizer.imageshack.com/img924/4427/S0JDbE.png)

## Podsumowując

To już wszystko, liczba narzędzi wydaje się niewielka, ale zaufaj mi, czeka cię jeszcze sporo pracy, zanim nauczysz ich się dobrze używać i naprawdę zrozumiesz jak działają. Miłej zabawy z podkładaniem dialogów ;)
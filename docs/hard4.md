---
id: hard4
title: Tablety i piórka
sidebar_label: Tablety i piórka
---

Opiekun listy: Yudeko#4356

| Nick        | Tablet                                | Programy                                                            |
|-------------|---------------------------------------|---------------------------------------------------------------------|
| Namae       | Huion GT191, Wacom Bamboo One CTL-671 | Paint Tool Sai                                                      |
| KaraKopiara | Wacom One M                           | Paint Tool Sai, Photoshop                                           |
| Arika       | Wacom Intuos M PTH-651                | Paint Tool Sai, Photoshop                                           |
| Shirottea   | Huion 610 PRO                         | Paint Tool Sai, Clip Studio Paint                                   |
| Lunastyczna | Wacom Intuos Pen&Touch M              | Paint Tool Sai, Photoshop CS6 Extended, Clip Studio Paint EX, Krita |
| Inlaru      | Huion Kamvas Pro 16                   | Paint Tool Sai, Clip Studio Paint, Photoshop                        |
| Aris        | Wacom Bamboo One CTL-671              | Paint Tool Sai, Krita                                               |
| Lizz        | Wacom Bamboo One CTL-671              | Paint Tool Sai                                                      |
| Zirathel    | Wacom Intuos Paint                    | Paint Tool Sai                                                      |
| Ama         | Huion HS610                           | Photoshop                                                           |
| Makaron     | Huion Inspiroy H580X                  | Clip Studio Paint                                                   |
| Naysa       | Huion Kamvas Pro 13                   | Photoshop, Gimp, Illustrator, Paint Tool Sai                        |
| Yudeko      | Wacom Intuos Comic                    | Clip Studio Paint                                                   |

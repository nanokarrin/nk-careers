---
id: dzwiekslownik
title: Słowniczek pojęć
sidebar_label: Słowniczek
---

## Zajrzyj też
* [Zakamarki audio](https://zakamarkiaudio.pl/slowniczek)

## Słowniczek

### DAW
DAW oznacza Digital Audio Workstation. Program do nagrywania i obrabiania audio, przydatne narzędzie dla dźwiękowca, chociaż programy do montażu wideo takie jak Adobe Premiere czy DaVnci Resolve, również posiadają podstawowe narzędzia do obróbki dźwięku.  Popularne programy komputerowe DAW to: Audacity,
Premiere Pro, n-Track, FL Studio, Reaper, Pro Tools, Ableton, Cubase, Logic.

### De-esser
Narzędzie służące do stłumienia nieprzyjemnego brzmienia sybilantów.

### Delay
Efekt polegający na powtórzeniu danego dźwięku co określony czas, gdzie każde kolejne odtworzenie jest coraz cichsze. Może symulować duże przestrzenie, gdzie dźwięk odbija się od odległej ściany, albo wiele źródeł dźwięków.

### Dry/Wet
Oznaczenia, które oznaczają surowy, nieprzetworzony (dry) i przetworzony (wet) sygnał. Często występują we wtyczkach VST i pozwalają balansować tym, jaki powinien być stosunek głośności efektu do oryginalnego dźwięku, np. w pogłosie.

### Efekt zbliżeniowy
Zmiana barwy dźwięku na cieplejszą w miarę zbliżania się do mikrofonu. Im bliżej mikrofonu się nagrywa, tym więcej niskich częstotliwości zarejestruje.

### Fade-in/out
Proces powodujący stopniowe narastanie (na początku) bądź ściszanie (na końcu) danego dźwięku. Dzięki temu można uniknąć charakterystycznego efektu urwania, jeśli początek lub koniec nie są idealnie ciche.

### Głośność
Siła sygnału akustycznego. Często mówi się o głośności szczytów (peak) w kontekście tego, by nie prowadzić do przesterów. Ważne jest również RMS, czyli średnia głośność sygnału w czasie. Na podstawie tej wartości serwisy streamingowe takie jak YouTube czy iTunes mogą ściszyć dany utwór, jeśli algorytm uzna go za zbyt głośny.

### Headroom
Zapas poziomu jakim dysponujemy. Zarówno podczas nagrań jak i pracy w DAW-ie. Jeśli pracujemy przy dużym headroomie, warto w finalnym kroku znormalizować całość, by nie była za cicha.

### Interfejs audio
Urządzenie, które pozwala na nagrywanie i odtwarzanie sygnału dźwiękowego. Współpracuje z DAW-em i pozwala na pracę z dźwiękiem w możliwie najlepszej jakości i przy minimalnych opóźnieniach. Często pozwala podłączyć mikrofon poprzez złącze XLR i zapewnia zasilanie dla mikrofonu. Ze względu na to, że jest wyspecjalizowane w swojej dziedzinie, oferuje lepszą jakość niż wbudowane karty dźwiękowe.

### Kompresor
Narzędzie służące do zmniejszenia zakresu dynamiki, czyli zmniejszeniu różnic głośności między cichszymi a głośniejszymi fragmentami.

### Korektor parametryczny
Przeważnie znany jako equalizer. Narzędzie pozwalające zmieniać głośność poszczególnych zakresów częstotliwości. Pozwala to na zmianę barwy dźwięku - zarówno delikatną korektę, jak i drastyczne zmiany (np. efekt głosu z telefonu przez ucięcie większości wysokich i niskych tonów).

### Mikrofon
Mikrofon to przyrząd, który służy do rejestracji sygnału dźwiękowego. W praktyce przetwarza fale dźwiękowe na inny rodzaj sygnału, np. analogowy lub cyfrowy.

### MP3
Najbardziej popularny format stratnej kompresji danych. Pozwala znacznie zmniejszyć miejsce zajmowane przez pliki dźwiękowe, kosztem ich jakości. Najlepiej posługiwać się tym formatem tylko przy eksporcie finalnego utworu i stosować wysoki bitrate (320 kbps).

### Normalizacja
Proces w którym zmieniamy głośność nagrania do określonej wartości. W praktyce doprowadzamy najgłośniejszy fragment audio do ustalonej wartości, np. do 0 dBFS. Dzięki temu dźwięk jest tak głośny, jak to możliwe zanim pojawią się przestery.

### Panorama
Odnosi się do pozycji dźwięku w spektrum stereo, czyli tego, czy bardziej słychać go z lewej, czy prawej strony. Jeśli z obu tak samo, to znaczy, że jest w centrum.

### Pogłos
Znany również jako reverb. Jeden z podstawowych efektów stosowanych podczas obróbki dźwięku. Pozwala oddać zarówno otoczenie, w którym znajdują się postacie (np. łazienka, albo ogromna hala) jak i być zabiegiem stylistycznym (jeśli odróżnia myśli od normalnie wypowiadanych kwestii).

### Pop filtr
Umieszczony przed mikrofonem pozwala na tłumienie niepożądanych dźwięków związanych z wydychanym powierzem, przede wszystkim w spółgłoskach wybuchowych. Można go również wykonać samodzielnie używając rajstop i kawałka drutu.

### Przesterowanie
Zjawisko w którym dźwięk jest głośniejszy niż 0dBFS, co powoduje powstanie charakterystycznych, nieprzyjemnych zniekształceń. Potocznie nazywamy przesterem. Przester w większości przypadków jest niepożądanym efektem. Może powstać już na etapie nagrywania, jeśli nagrywany dźwięk był głośny, a wzmocnienie mikrofonu zbyt duże.

### SFX
Efekt dźwiękowy np. dźwięk wybuchu czy uderzenia. Przeważnie w fandubbingu tak nazywamy wszystkie dźwięki, które nie są muzyką, mową albo wokalem. W tym również odgłosy otoczenia, takie jak szum fal.

### Sybilanty
Syczące odgłosy w mowie. Polskie sybilanty to: s, z, c, dz, sz, ż/rz, cz, dż, ć, dź. Jeśli są zbyt głośne, to nagranie brzmi nieprzyjemnie. Do ich redukcji używa się de-esserów.

### VST
Wtyczka, której można użyć w DAW-ie, pozwalająca rozszerzyć możliwości przetwarzania dźwięku. Wtyczki mogą pozwolić na nakładanie efektów, korekcję nagrań, a nawet na generowanie dźwięków czy użycie wirtualnych instrumentów.

### WAV
Bezstratny format audio. Pliki .wav zawierają nieskompresowane audio, które zajmuje więcej miejsca, ale jest możliwie najlepszej cyfrowej jakości.

### Zakres dynamiki 
Różnica głośności między dźwiękiem najcichszym, a najgłośniejszym. Zbyt duży zakres dynamiki w utworze powoduje, że nieprzyjemnie się go słucha (analogicznie jak przy wyjściu z ciemnej piwnicy na pełne słońce nagła zmiana drażni oczy). Z kolei zbyt mały zakres dynamiki uwydatnia wszelkie brudy i sprawia, że utwór brzmi płasko, brakuje w nim energii.

### Zero cyfrowe
Przeważnie oznaczonej jako 0dB lub 0dBFS. Jest to maksymalna głośność przy której dźwięk odtwarzany i zapisywany jest bez zniekształceń. 

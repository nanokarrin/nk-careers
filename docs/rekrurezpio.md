---
id: rekrurezpio
title: Rekrutacja na reżysera piosenek
---
:::tip
Rekrutacja na reżysera dostępna jest jedynie dla członków NK, którzy są w grupie od minimum miesiąca.  
Ostatnia zmiana: 9 lipca 2022 r.  
:::

### Osoby decyzyjne
Opiekunami rekrutacji na reżysera piosenek są Hikari (Hikari#3729) oraz Orzecho (Orzecho#1720).

# Przebieg rekrutacji na Reżysera Piosenek
Rekrutacja jest **dwuetapowa.**  

## Etap pierwszy
W pierwszym etapie wcielasz się w rolę reżysera i oceniasz materiały wysłane Ci przez "Twoją" ekipę - wokal oraz miks. Tutaj sprawdzona zostanie Twoja umiejętność krytycznego słuchania.   
Link do formularza z pytaniami: https://forms.gle/kjpKtf4s4SyviojVA  
Po wysłaniu odpowiedzi załóż na Discordzie konfę miedzy Tobą, Hikari, Orzechem i Pchełką, będzie to wspólne miejsce komunikacji do końca rekrutacji.

Jeśli już działałeś jako reżyser, **pierwszy etap możesz zaliczyć także portfolio**. Załóż wyżej wspomnianą wspólną konfę, wyślij nam linki do swoich projektów i napisz o nich trzy słowa (co było trudne, co poszło szybko, czy masz jakieś wnioski na przyszłość).
Portfolio to 2-3 piosenki po polsku, każda trwająca minimum minutę.

## Etap drugi
Polega on na samodzielnym wyreżyserowaniu piosenki. Nie musisz konsultować się z opiekunami w trakcie projektu, ale warto to robić - unikniesz dzięki temu poprawek do elementów, które zostały już wykorzystane w dalszym etapie (np. wokal warto poprawić, zanim trafi do realizatora). 

### Wzór wątku
Załóż wątek według wzoru w dziale Piosenkowo, z dopiskiem [No.1] przed tytułem (wzór pojawi się automatycznie przy pisaniu nowego postu). W tym etapie zostawiamy Ci wolną rękę - kiedy projekt będzie gotowy, wyślij go nam do weryfikacji. Jeśli planujesz założyć projektowi serwer albo konwersację na discordzie, dobrym pomysłem będzie dodanie do nich opiekunów, w ten sposób będzie łatwiej ocenić Twoją pracę i pomóc w przypadku potencjalnych problemów.<br/> <br/>Jeśli projekt zostanie zaakceptowany, otrzymasz rangę, a projekt zostanie wpisany do kolejki.  
Jeśli nie, zostaniesz poproszony o wprowadzenie koniecznych poprawek. W zależności od jakości piosenki i Twojej pracy z zespołem otrzymasz jedną z dwóch rang: <b>reżysera piosenek</b> lub <b>młodszego reżysera piosenek</b>.

### Uwaga!
* By przećwiczyć zarządzanie całym zespołem, w projekcie rekrutacyjnym powinieneś piastować jedynie rolę reżysera. Jeśli chcesz robić w nim coś więcej, zwróć się do Hikari i Orzecha po okejkę
* Robienie projektów na okazje jest zabronione - poprawki mogą się przeciągnąć
* Nie polecamy czterominutowych grupówek
* Proponuję mniej znane piosenki, które pozwolą na większe pole do popisu, jeśli chodzi o interpretację
* Jeśli nie chcesz czekać na tekst, zajrzyj do wątku z tekstami do wzięcia (jest mnóstwo fajnych tekstów gotowych do wzięcia)
* Potencjalna zmiana projektu rekrutacyjnego już po rozpoczęciu poprzedniego wymaga konsultacji z oboma rekruterami i zgody z ich strony

Do pracy, walcz dzielnie! o/

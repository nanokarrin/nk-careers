---
id: aktorrada3
title: Wyciszanie pomieszczenia
sidebar_label: Wyciszanie pomieszczenia
---

## Czemu ma to służyć?
Robi się to głównie po to, żeby zniwelować pogłos i nie brzmieć jakby się nagrywało w łazience lub w dużym opustoszałym pokoju, gdzie echo niesie się kilometrami. Wytłumienie polepsza też jakość nagrywania oraz niweluje szumy z otoczenia, które zbierają mikrofony.

## Jak wyciszać?
Jest wiele sposobów na wyciszenie miejsca, w którym nagrywacie. Od zwykłego narzucenia koca lub kołdry na siebie i swój mikrofon po oklejenie ścian pokoju gąbką akustyczną. Najlepiej jest samemu poszukać różnych sposobów w internecie, poeksperymentować i sprawdzić, co mieści się w Twoim budżecie (i pokoju) oraz co działa najlepiej w Twoim przypadku, bo każdy z nas mieszka inaczej.

## Przykładowe poradniki
- Panele akustyczne:
 - [DIY Acoustic Panels & Microphone Reflection Screen](https://youtu.be/5Py5mKYxNFc) (po angielsku)
 - [filtr akustyczny - jak zrobić](https://youtu.be/oais9RD86Uo) (po polsku – poniżej FAQ do tego poradnika)

Na pytania odpowiadali członkowie NanoKarrin – Pawlik i konji.

 1. P: *Czy musi być jakaś tkanina? Nie lepiej załatwić folie, deski lub założyć pełnowymiarową płytę pilśniową?*

 O: *Musi być jakiś materiał, aby gąbka, która znajduje się w środku wchłaniała pogłosy. Jeśli zastąpisz tkaninę folią lub deskami, to dźwięk będzie się odbijał i nie będzie wystarczającego efektu. Natomiast płyta pilśniowa jest teoretycznie dźwiękochłonna, więc mógłbyś ją założyć na stronę **zewnętrzną **. Jednak lepiej nie ryzykować niepotrzebnej pracy, skoro można zrobić łatwiej i taniej na materiale, więc nie jest to zalecane.*
 *Co do materiału to **przynajmniej** strona **wewnętrzna** (od strony mikrofonu) paneli z włóknem musi być z materiału, który przepuszcza dźwięk. Dźwięk musi „wejść” do panelu, żeby zostać „pochłoniętym” przez włókna. Im materiał będzie gęściej tkany, tym mniej przepuści. Ponoć gęsto tkane przepuszczają tylko niższe częstotliwości, a wysokie odbijają.*

 2. P: *Nie mogę znaleźć dość dużego materiału na mój projekt. Myślałem o prześcieradle, ceracie lub plandece, bo prawdopodobnie byłyby w interesującym mnie rozmiarze, ale nie wiem czy to odpowiedni materiał. Czy gdyby płyta pilśniowa miała otwory jak w serze szwajcarskim, to robiłoby jakąś różnicę?*
 
 O: *Możesz wypróbować swoje prześcieradła i zobaczyć jak Ci pasuje. Cerata i plandeka się nie nadają. Płyta pilśniowa z otworami też nie nadaje się na stronie wewnętrznej. Wystarczająco duży materiał znajdziesz w Castoramie w dziale z tkaninami sprzedawany na metry o szerokości przeważnie 1,5m. Pamiętaj, żeby materiał był wystarczająco gęsty bo inaczej będzie przepuszczał wełnę i będzie bieda ze zdrowiem.*

 *Przeczytaj też: [Akustyka – Budowa paneli akustycznych](http://essentialmusic.pl/akustyka-budowa-paneli-akustycznych)*

 *Nie jestem też pewien, czy owijanie folią wełny w tych poradnikach to dobry pomysł ze względów akustycznych. Pylenie wełny wolałbym zniwelować gęstym materiałem na obiciu.*
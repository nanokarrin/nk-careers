﻿---
id: hard2
title: Przed mikrofonem
sidebar_label: Jak nagrywać
---

## **Wykonawca przed mikrofonem**
Zacznijmy od materiału wideo, który wyjaśnia, na co zwracać uwagę przy ustawianiu mikrofonu. (kliknij w obrazek)
[![Kliknij i obczaj](https://img.youtube.com/vi/WT5pGO3pmpg/maxresdefault.jpg)](https://www.youtube.com/watch?v=WT5pGO3pmpg)

Niestety materiał wyżej nie był przygotowany z myślą o fandubbingu, dlatego niżej rozwiniemy kilka kwestii.
## Jakiego nagrania oczekuje reżyser

 - **Przede wszystkim nieobrobionego.** Jeśli wiesz co nieco o obróbce nagrań, to fajnie, ale zostaw to dźwiękowcom. Odszumiać też nie wolno.
 - **Nieprzesterowanego.** Gdy dźwięk przekracza zero cyfrowe, czyli poziom 0dB, mamy do czynienia z przesterem (niektóre interfejsy mogą mieć przester na poziomie -6dB). Takie nagranie będzie nie do odratowania. Lepiej jest nagrać się za cicho niż z przesterem.
 - **Bez wybijających się głosek wybuchowych.** Po prostu stawiasz popfiltr w połowie drogi między twoimi ustami a mikrofonem i powinno być dobrze.
 - **Bez pogłosu.** O tym, jak sobie z tym radzić, mówiliśmy na poprzedniej stronie.
## Jak się ustawić
Poradnik wideo powyżej, dosyć dobrze o tym opowiedział, ale brakuje w nim dwóch rzeczy.   
**Po pierwsze: Jaka odległość powinna dzielić nas od mikrofonu?**   
Taka jak na zdjęciach poniżej:
![odległość](https://imagizer.imageshack.com/v2/756x639q90/923/YaDWXi.jpg)
![tak to mniej więcej wygląda](https://imagizer.imageshack.com/v2/1094x410q90/923/h6ev6R.jpg)

Ta odległość działa zarówno na mikrofonach pojemnościowych, jak i dynamicznych.   
Zachowanie tej odległości jest istotne dla wyrównania jakości nagrań wszystkich aktorów. Przyjemniej słucha się projektu, w którym aktorzy nagrywali się w podobnej odległości od mikrofonu. Wyobraź sobie na chwilę, że mikrofon to czyjeś ucho (inaczej słyszysz swojego rozmówcę zależnie od odległości między wami). Z zamkniętymi oczami możesz stwierdzić, czy rozmówca jest bliżej czy dalej od ciebie i podobnie jest z nagraniami.   
Wyjątkiem od tej reguły są nagrania lektorskie i myśli bohaterów, które mogą wymagać przybliżenia się do mikrofonu, jednak lepiej spytać reżysera, czy chce, byś tak to nagrał.

**Po drugie: Ustawianie wzmocnienia**   
Jeśli zaopatrzyłeś się w interfejs, wzmocnienie regulujesz fizycznym potencjometrem. W przypadku większości mikrofonów na USB gain reguluje się z poziomu systemu operacyjnego, czyli wchodzisz w *Panel Sterowania*, następnie *Sprzęt i dźwięk*,*Zarządzaj urządzeniami audio*, wybierasz swój mikrofon, wchodzisz we *Właściwości* i zakładkę *Poziomy*.
![no tak](https://i.ibb.co/xLdjHj0/terfefere.png)
Skoro już wiemy gdzie, przejdźmy do tego jak ustawić.   
Jeśli ci się nie chce, to istnieje szybka, prosta metoda. Krzyknij i ustaw wzmocnienie tak, by zostawić sobie około 3dB zapasu do zera cyfrowego. (Przy okazji pamiętaj o tym, by nie krzyczeć gardłem, bo je sobie zedrzesz. Szkoda gardła. Krzycz z przepony.)   
Jeśli masz ochotę, możesz sobie przygotowac kilka ustawień wzmocnień. Przykładowe dwa:
- Na kwestie mówione normalnie, najlepiej tak by dźwięk mieścił się w przedziale -12dB;-6dB, w ten sposób nagranie nie będzie ciche i wciąż będziesz miał trochę miejsca na głośniejszy dźwięk.   
- Na głośny krzyk. Warto ustawić to tak, by najgłośniejszy dźwięk nagrania zbliżał się do zera cyfrowego z zapasem 1-2dB. Jednak jeśli obawiasz się zmarnowanego nagrania przez przestery, to śmiało zmniejsz wzmocnienie.

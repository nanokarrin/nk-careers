---
id: dzwiekmuzykazakonczenie
title: Zakończenie utworu muzycznego
sidebar_label: Zakończenie muzyki
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/CZRfSSVnnIw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Zmiękczanie końcówek uciętego utworu, żeby nie brzmiał na gwałtownie urwany.
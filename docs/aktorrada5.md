---
id: aktorrada5
title: Udostępnianie nagrań
sidebar_label: Udostępnianie nagrań
---

Każdy reżyser czy to z NanoKarrin czy też spoza grupy ma inne upodobania w kwestii formy dostarczania mu plików z nagraniami. Omówię więc tutaj bardziej szczegółowo, jak to powinno wyglądać, niż jest to opisane w zasadach rekrutacji na aktora, żebyś mógł ułatwić sprawdzanie rekruterom i reżyserom. Zaprezentuje też podstawy udostępniania plików na Google Drive, bo mimo wszystko zwykle na nim są trzymane lub zbierane nagrania do projektów.

## Rekrutacja
Na rekrutację na aktora w NK masz do nagrania dwie rzeczy – wiersz „Krasnoludki” Jana Brzechwy i kwestie do scenki. Dla wiersza wystarczy plik audio, za to do scenki musi być wideo z dźwiękiem. Musisz je wrzucić na jakiś hosting, to znaczy, stronę, która pozwala na odsłuchanie i/lub obejrzenie pliku bez konieczności jego pobierania. Najprostszymi i najpopularniejszymi miejscami, do załadowania Waszych plików jest YouTube i Google Drive. YouTube większość z Was pewnie zna, ale przejdziemy przez podstawy udostępniania tam wideo, żeby nie było później wpadek w rekrutacji, które nadal mają czasem miejsce. Google Drive to taki wirtualny dysk z pamięcią, jaki masz w swoim komputerze czy laptopie. Możesz tam wrzucić pliki z dowolnym formatem, trzymać je tam i udostępniać innym.

## YouTube

### Przesyłanie
Aby przesłać film na YouTube wystarczy, że klikniesz ikonę kamery z plusem, po czym wybierzesz „Prześlij film”.

![Prześlij film](https://i.imgur.com/5SRsNPe.jpg)

Następnie postępuj według instrukcji YT aż do 3. kroku „Widoczność”. Jeśli chcesz, żeby każdy widział Wasz filmik, to wybierz opcję „Publiczny”. Jeśli chcesz, żeby tylko niektóre osoby (np. rekruterzy na aktorów w NanoKarrin) mogli obejrzeć Wasz filmik, to wybierz opcję „Niepubliczny”. *Nie wybieraj w takim wypadku opcji „Prywatny”.* Rekruterzy przy opcji „Prywatny” nie mają możliwości zobaczenia Waszego prywatnego filmiku.

![Widoczność](https://i.imgur.com/TQMC5xf.jpg)

Zawsze sprawdź przed wysłaniem swojej rekrutacji, czy ustawiłeś swój filmik jako publiczny lub niepubliczny, a nie prywatny. Jeśli nie jesteś pewny, to możesz wejść w filmik i się upewnić. Jeśli jest publiczny, to nic nie powinno się wyświetlać pod tytułem filmiku. Jeśli jednak ustawiłeś filmik jako prywatny lub niepubliczny, zobaczysz odpowiednio belkę pod tytułem z kłódką i napisem „Prywatny” lub z łańcuchem i napisem „Niepubliczny”.

![Prywatny](https://i.imgur.com/w5Jaq1z.jpg) ![Niepubliczny](https://i.imgur.com/k5ZVLn5.jpg)

### Prawa autorskie
Jeśli do wrzucenia scenki użyjesz YouTube’a, to musisz mieć na uwadze to, że może on zablokować i/lub usunąć Twoją scenkę ze względu na prawa autorskie. Najlepiej więc wrzucić scenkę parę dni przed złożeniem swojej rekrutacji i dać czas YouTube'owi, aby zareagował. Jeśli po paru dniach nic się nie wydarzy z filmikiem, to możesz podejść do rekrutacji, bo prawdopodobnie YT nie zablokuje Ci scenki. Jeśli jednak to zrobi, to wtedy musisz albo przygotować inną scenkę z nadzieją, że tej następnej nie zablokuje albo, co polecam, wrzucić plik z Twoją gotową scenką na dysk Google. Przykład zablokowania widoczny w YouTube Studio:

![Prawa autorskie](https://i.imgur.com/qstQnhG.jpg)

## Google Drive
Tutaj wystarczy, że masz konto Google z gmailem i wejdziesz na [tę stronę](https://drive.google.com/drive/my-drive), po czym wrzucisz filmik z Twoją scenką. Na dysku Google możesz tworzyć też foldery, tak jak na komputerze, jeśli chcesz. Google Drive pozwala wrzucać też same pliki audio, więc nie musisz tworzyć widea do swoich „Krasnoludków”.

![Dysk Google](https://i.imgur.com/f76U6JP.jpg)

Po wrzuceniu scenki klikasz na nią prawym przyciskiem myszki. Pojawi Ci się okno. Wybierz w nim opcję „Pobierz link do udostępniania”. Pojawi się kolejne okno. Włączy się wtedy opcja udostępniania pliku za pomocą linku.

![Pobierz link do udostępniania](https://i.imgur.com/IhQYwME.jpg)

To znaczy, że suwak z rogu powinien być zielony i przesunięty w prawo automatycznie. Pod spodem powinna się znajdować informacja, że każda osoba posiadająca link będzie mogła zobaczyć Twoją scenkę. Link jest również automatycznie kopiowany, więc możesz go od razu wkleić bez ingerowania w to okno. Jednak jeśli chcesz się upewnić, czy plik będzie widoczny dla innych lub chcesz zmienić jego widoczność, to kliknij „Ustawienia udostępniania”.

![Ustawienia udostępniania](https://i.imgur.com/Yk1a5SY.jpg)

Pokaże się kolejne okno. Tutaj możesz się upewnić, czy plik jest dostępny dla osób posiadających link, ponownie skopiować link do pliku lub zmienić ustawienia udostępniania. Jeśli chcesz wykonać to ostatnie, kliknij „Zmień”.

![Zmień](https://i.imgur.com/FUMiu1B.jpg)

W następnym oknie masz do wyboru ponowne skopiowanie linku oraz wgląd w opcje. Dla scenki wystarczy ustawić „Przeglądający”. „Komentujący” i „Edytor” jest zwykle przydatne w dokumentach, gdzie można zaznaczyć, skomentować lub zedytować jakiś tekst. Potem klikasz „Gotowe” i tyle. Możesz teraz bez problemu wysyłać link do swoich plików na dysku Google bez obawy o zablokowanie ze względu praw autorskich. ;)

![Przeglądający](https://i.imgur.com/1HlrKtr.jpg)

## Pamiętaj chemiku młody…
Powtórzę na koniec – pamiętaj, żeby sprawdzić, czy Twój film na YouTubie nie jest prywatny, a osoby mające link do Twoich plików na Google Drive mają do nich dostęp. Takie sprawdzenie trwa moment, a naprawdę ułatwia pracę rekruterom oceniającym Twoje zgłoszenie, gdy nie muszą czekać, aż naprawisz zły sposób udostępniania Twoich materiałów rekrutacyjnych.

---
id: rekrudzwiek
title: Rekrutacja na dźwiękowca
---

## Osoby decyzyjne
Obecnie rekrutację sprawdza nouwak (Discord: @nouwak) oraz Wiwi (@wiwiqueenofkiwi). To oni podejmują decyzję o nadaniu rangi. <br/><br/>
Jeśli przed podejściem do rekrutacji masz jakieś pytania odnośnie do udźwiękowienia lub nie masz pomysłu na scenkę, a nie jest to uwzględnione ani tutaj, ani w Szkółce NanoKarrin, napisz do nich na Discordzie.

## Kim jest dźwiękowiec?
Dźwiękowiec to osoba, która odtwarza ścieżkę dźwiękową filmu (odgłosy i muzykę), montuje nagrane dialogi i eksportuje gotowe wideo.

## Co należy zrobić?
Musisz zrealizować wybraną przez Ciebie scenkę, która zaprezentuje Twoje umiejętności.

Co to znaczy zrealizować? W gruncie rzeczy chodzi o odtworzenie warstwy audio tak, aby jak najbardziej przypominała oryginalną oraz w jak najlepszym stopniu była miła w odsłuchu dla widza.

Składa się na to:

- pozyskanie i podłożenie soundtracków (staramy się wykorzystywać oryginalne soundtracki, lecz w przypadku ich braku dobieramy na wyczucie odpowiednie utwory)

- pozyskanie, odtworzenie i podłożenie wszelakich efektów dźwiękowych oraz ambientów (można je nagrać samemu, lecz większość da się pozyskać z takich stron jak https://freesound.org/)

- przygotowanie, podłożenie oraz obróbka nagrań od aktorów (w tym m.in. odszumianie, poprawianie timingu-kłap oraz nakładanie efektów)

- finalny miks (wyrównanie poziomów głośności, poszukiwanie przesterów itp.)

- wyrenderowanie całości jako filmu, z obrazem i dźwiękiem dobrej jakości

## Po co to robię?
Wszystkie te elementy są potrzebne, by w pełni ocenić Twoje umiejętności, bo dokładnie tymi rzeczami zajmuje się dźwiękowiec w projektach NK. W szczególności scenka musi zawierać także dialogi. W razie czego możesz zagrać wszystkie postacie samodzielnie albo poprosić o pomoc aktorów na Discordzie - zawsze znajdą się tam pomocni ludzie. Scenka rekrutacyjna może być dowolnej długości, pamiętaj jednak, że musi być dostatecznie długa, by być reprezentatywną, ale jeśli będzie za długa, będzie Ci ją trudniej zrobić.

Wrzuć scenkę w możliwe najlepszej jakości na jakiś ogólnodostępny serwis streamingowy, a linki zamieść w swoim poście rekrutacyjnym na Discordzie. Wynik rekrutacji pojawi się w formie wiadomości w założonym przez Ciebie wątku. Jeśli nie odpiszemy po dwóch tygodniach, przypomnij się. Też jesteśmy ludźmi, mamy mnóstwo spraw na głowie, możemy po prostu zapomnieć.

## Jaki może być wynik rekrutacji?
### Pozytywny
Otrzymujesz rangę "Dźwiękowiec". Jest to ranga przydzielana rekrutom, którzy wykazali odpowiednie umiejętności w tworzeniu warstwy dźwiękowej. Ścieżki Dźwiękowca nie muszą być sprawdzane przez innego Dźwiękowca, choć jest to zalecana praktyka.

### Negatywny
Nie otrzymujesz rangi. Musisz jeszcze popracować nad warsztatem. Kolejną próbę możesz podjęć najwcześniej za miesiąc od momentu rozpatrzenia rekrutacji.

---
id: dialogi1
title: Pisanie pod kłapy
sidebar_label: Kłapy
---


Pora przejść do sedna. Tym, czym zajmuje się dialogista, jest pisanie (kto by pomyślał?) dialogów. Oznacza to po pierwsze przetłumaczenie oryginalnych dialogów (albo wersji angielskiej, japońskiej, niemieckiej, hiszpańskiej, czy jaki tam inny język znacie. Możecie też skorzystać z dostępnych polskich fanowskich napisów, ale wówczas należy przynajmniej wspomnieć o źródle, z którego korzystasz i upewnić się, że jakość tłumaczenia jest dobra).

Po drugie, dialogista musi dopasować tekst do ruchu ust postaci. Ten ruch postaci nazywamy kłapami, ze względu na to, że w animacji często jest on mocno uproszczony i ogranicza się do otwierania i zamykania ust z pewną częstotliwością - takie "kłapanie dziobem".

Żeby nabyć intuicji odnośnie dopasowywania, trzeba po prostu stworzyć kilka scenariuszy. Nie ma to jak praktyka. Jest jednak kilka rzeczy, które mogą się przydać przed zabraniem się do pisania. Przede wszystkim w dopasowaniu liczą się: czas kwestii oraz liczba sylab w niej występujących. Jeśli kwestia jest krótka, najlepiej napisać ją zachowując dokładnie liczbę sylab. Wtedy najczęściej zostanie osiągnięte najlepsze dopasowanie. Natomiast jeśli kwestia jest dłuższa, najlepiej skupić się na dopasowaniu pod jej długość. W przypadku dłuższych kwestii, choć można oczywiście dopasowywać liczbę sylab, nie jest to tak krytyczne, ponieważ ewentualna różnica kilku sylab nie będzie zbyt widoczna - aktor dopasuje się, mówiąc nieco wolniej lub szybciej. W przypadku krótkich kwestii jest po prostu mniejsze pole do manewru i nawet niewielkie zmiany będą wymagać zbytniego przyspieszenia lub zwolnienia i widz odczuje nienaturalność takich wypowiedzi.

Żeby sprawdzić dopasowanie napisanych dialogów, najlepiej przeczytać je równo z oryginalną kwestią. Jeśli skończysz po jej końcu - twoja jest zbyt długa. Jeśli przed końcem - za krótka. Nie musisz grać podczas wypowiadania - chodzi głównie o długość - musisz natomiast mówić w tym samym tempie co oryginalny aktor. To naturalne, że czasem mówi się szybciej - np. gdy jest się zdenerwowanym. Jakaś postać z kolei może być przedstawiona jako mało rozgarnięta i mówić powoli. To oznacza, że możnaby pod jej kwestie dopasować dużo dłuższe kwestie i zdążyć je powiedzieć razem z nią, ale wówczas nie będzie się to zgrywać z obrazem, w którym usta będą poruszały się wolniej. Dlatego ważna jest prędkość mówienia.

## Przykład

Sylaby są ważne, jak wspominałem, głównie w krótkich kwestiach. Weźmy jako przykład taką "No, I'm here.". Trzy sylaby. Przy tłumaczeniu aż ciśnie się, by napisać "Nie, jestem tutaj". Pięć sylab. Za dużo. Łatwo można zmniejszyć o sylabę zamieniając "tutaj" na "tu". Jednak "Nie, jestem tu." brzmi nieco dziwnie. Poprzez inwersję możemy otrzymać "Nie, tu jestem." jeśli chcemy położyć akcent na akcję - bycie, niż konkretne miejsce. Cztery sylaby to nadal za dużo, choć od biedy można by zostawić, jeśli nie znajdzie się nic lepszego. Ale kwestię możemy skrócić jeszcze bardziej. "Nie, jestem." albo "Nie, tutaj." w zależności od tego, jaka była kwestia poprzedniej postaci. Możemy np. rozpatrzyć dialogi:


> \- Hej, jesteś w kuchni?
>
> \- Nie, tutaj.

Natomiast przy innym pytaniu odpowiedź też się zmienia:

> \- Hej, wyszedłeś już?
>
> \- Nie, jestem.

Zwróć też uwagę, że przerwa w kwestii (przecinek) znajduje się zawsze po pierwszej sylabie, jak w oryginale. O tym jednak będzie mowa później, podobnie jak o skracaniu czy wydłużaniu kwestii. Przykład miał głównie pokazać i uświadomić, że pisanie pod kłapy jest bardziej zawiłe niż samo tłumaczenie, ponieważ kłapy wprowadzają spore ograniczenia. Czasem aż świerzbi, żeby coś napisać w określony, ładny sposób, ale kłapy na to nie pozwalają. Potraktuj to jako wyzwanie! :)
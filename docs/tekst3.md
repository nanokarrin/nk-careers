---
id: tekst3
title: Zapis formalny
sidebar_label: Zapis formalny
---

### Sposoby zapisu
Istnieje kilka sposobów zapisania słów piosenki. Weźmy na przykład piosenkę [My Song z Angel Beats] (https://mp3.nanokarrin.pl/Angel_Beats!_-_My_Song_(Moja_piesn).mp3).

##### Najpopularniejszym stylem jest ten **typowy dla wierszy**
>Dziś już prawie zmierzch nadchodzi  
Szukam miejsca, gdzie powoli  
Żal mój zmieniać będę w piękną pieśń tę

+ wers rozpoczęty wielką literą
+ brak znaków interpunkcyjnych na końcu (wyjątek: cudzysłów, znak zapytania)
+ poprawna interpunkcja w środku wersu (przecinki, dwukropki, cudzysłowy)
+ tekst podzielony na zwrotki i refreny

##### Zdarza się, że stosowany jest też styl **typowy dla prozy**
> Dziś już prawie zmierzch nadchodzi.  
Szukam miejsca, gdzie powoli  
żal mój zmieniać będę w piękną pieśń tę.

+ tekst podzielony na zwrotki i refreny
+ początek zdania wielką literą
+ interpunkcja typowa dla zdania (kropki na końcu, przecinki oddzielające czasowniki etc.)

### Który wybrać?
Najważniejsze to wybrać jeden styl i być konsekwentnym w obrębie całej piosenki.  
Natomiast z doświadczenia powiem, że w 99% przypadków używa się stylu pierwszego, bo trudniej o błąd w interpunkcji, no i podzielenie tekstu na krótsze fragmenty, zwrotki i refreny pomoże potem grafikowi w rozstawieniu napisów na filmie :).

### Uwaga! Zły podział!
Wielu początkujących tekściarzy zaznacza frazy muzyczne przecinkiem. Przykładowo:
> Dziś już prawie zmierzch, nadchodzi  
Szukam miejsca gdzie, powoli  
Żal mój zmieniać będę w piękną pieśń, tę

To błąd. Do takiego oddzielania, jeśli już chcemy je zaznaczyć (bo nie jest obowiązkowe) służą pionowe kreski: |
> Dziś już prawie zmierzch | nadchodzi  
Szukam miejsca, gdzie | powoli  
Żal mój zmieniać będę w piękną pieśń | tę

Porównajcie :)

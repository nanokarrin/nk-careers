---
id: rekruilustrator
title: Rekrutacja na ilustratora
---

### Osoby decyzyjne
Obecnie rekrutację sprawdza arika (@arika) i to ona podejmuje decyzję o nadaniu rangi.

### Przebieg rekrutacji
Warunkiem otrzymania rangi ilustratora jest wysłanie trzech ilustracji, które ukazują Twoje umiejętności rysownicze.  
Uwagi dla ilustratorów:  
• Przedstawiona ilustracja musi mieć wielkość minimum 500x500 pikseli.  
• Muszą zostać przedstawione przynajmniej dwie ilustracje bez tła: fullbody kobiety i fullbody mężczyzny.  
• Dodatkowo, do każdej pracy należy przesłać trzy etapy powstawania rysunku (np. szkic, lineart, coloring). Jeśli nie używasz w swoich ilustracjach lineartu lub szkicu, to zamiast nich uwzględnij inny etap.  
• Jeżeli korzystasz z imgura do hostowania, to aby pokazać obrazek w pełnym rozmiarze, wystarczy po wrzuceniu otworzyć go w nowej karcie i skopiować link.

Jak założyć wątek rekrutacyjny?  
Wejdź na Discord NK, poszukaj forum "Rekrutacja NK" i załóż nowy post. Tam też możesz podejrzeć, jak robili to inni :)

Wynik rekrutacji pojawi się w formie wiadomości w założonym przez Ciebie wątku. Jeśli nie odpiszemy po dwóch tygodniach, przypomnij się! Też jesteśmy ludźmi, mamy mnóstwo spraw na głowie, możemy po prostu zapomnieć.

### Jaki może być wynik rekrutacji?
##### Pozytywny
Otrzymujesz rangę Ilustrator NK.
##### Negatywny
Jeśli nie udało Ci się pomyślnie przejść rekrutacji, możesz podejść do niej ponownie po miesiącu. Wykorzystaj ten czas na doskonalenie swoich umiejętności, ponieważ Twoje postępy będą brane pod uwagę. Prosimy również, aby w takim przypadku (wraz z założeniem nowego wątku rekrutacyjnego) wysłać nam link do swojej poprzedniej rekrutacji, ponieważ to ułatwi nam pracę i wystawienie ponownej oceny.
Do rekrutacji można podejść tyle razy ile potrzeba, ponieważ każdy ma szansę na rozwój oraz na dołączenie do grupy. Wróć do nas w momencie, kiedy naprawdę będziemy mogli ocenić Twoje postępy.

### Dwa słowa od rekrutera
Mimo iż wydaje się to dosyć oczywistą sprawą - przypominamy, że prace nie mogą być "z internetu"! Musisz być ich oryginalnym twórcą i zastrzegać sobie do nich wszystkie prawa autorskie. Nie akceptujemy też prac, które są przerysowane z prac innych twórców lub są dosłowną kalką zdjęć czy też innych artów.

Przed dodaniem posta z rekrutacją zastanów się, czy wrzucane przez Ciebie prace są wykonane starannie - bez dziur między konturem a wypełnieniem, bez koloru wychodzącego poza kontury. Jeśli dodajesz pracę bez tła, sprawdź, czy wokół przedstawionej postaci/obiektu nie pozostały niepożądane plamy. Przy tworzeniu ilustracji skup się przede wszystkim na poprawności anatomicznej szkicu i spędź nad tym etapem najwięcej czasu - jeśli szkic jest słaby, każdy kolejny element wychodzi gorzej.  
Jeśli masz wątpliwość, pokaż najpierw swoje prace innym na kanale "ilustratorzy".

---
id: 6nevergonna
title: Rick Astley – Never Gonna Give You Up
sidebar_label: Rick Astley – Never Gonna Give You Up
---
### Info
>Tekst: Liska
>Korekta: Pchełka
>Pobierz mp3: https://mp3.nanokarrin.pl/

<iframe width="560" height="315" src="https://www.youtube.com/embed/9Fybl32_r5U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Tekst
Prawdziwa miłość to skarb  
Znasz jej zasady i znam je ja  
I dobrze wiesz, że żaden inny gość  
Nie da ci tyle, ile mógłbym tobie dać  

Chcę ci dzisiaj wyznać to, co czuję  
Chcę powiedzieć o tym, że  

Nigdy nie opuszczę cię  
Nigdy nie zawiodę cię  
Nie bój się przytulić mnie i 
Odetchnąć    
Przy mnie nie uronisz łez  
Ani cię nie zmoczy deszcz  
Proszę, nie opieraj się 
Pragnieniom    


Moje wyznania masz za trik  
Myślisz, że zranię cię tak jak wszyscy wcześniej  
I widzę, że się starasz oprzeć mi  
Lecz dziś wyjątek zrób, a cię wesprę  

Kiedy znów cię spotkam, nie odpuszczę  
W końcu powiem o tym, że  

Nigdy nie opuszczę cię  
Nigdy nie zawiodę cię  
Nie bój się przytulić mnie i  
Odetchnąć  
Przy mnie nie uronisz łez  
Ani cię nie zmoczy deszcz  
Proszę, nie opieraj się  
Pragnieniom  
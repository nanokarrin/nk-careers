---
id: 2gameboyd
title: Garfield Gameboy’d Zakończenie
sidebar_label: Garfield Gameboy’d Zakończenie
---
### Info
>Tekst: Spidek  
>Korekta: Pchełka  
>Pobierz mp3: https://mp3.nanokarrin.pl/

<iframe width="560" height="315" src="https://www.youtube.com/embed/AeZZB0ky_48" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Tekst
Warstwa po warstwie  
Buduje się więź  
Tak jak ta lazania  
Stworzona z mych łez  
Dni nowych początek  
Goryczy zalążek  
Nadal nie wiem  
Czy zdołam przetrwać grę  
Więc żegnam się  

---
id: 0spistekstow
title: Teksty piosenek - spis
sidebar_label: Spis piosenek
---
## Info
> Jeśli masz tekst do dodania, napisz do opiekuna listy: Discord Pchełka#6770  
> Pobierz mp3: https://mp3.nanokarrin.pl/

## Lista piosenek
### [Psycho Pass ED - Namae No Nai Kaibutsu (Potworze)](1potworze)
### [Garfield Gameboy’d Zakończenie](2gameboyd)
### [La La Land - City of Stars](3cityofstars)
### [Purple Kiss - Zombie](4purplekisszombie)
### [Kingdom Hearts III - Face My Fears ](5facemyfears)
### [Rick Astley – Never Gonna Give You Up](6nevergonna)
### [Wiedźmin – Płoń, rzeźniku, płoń](7plonrzezniku)
### [Landon Pigg - Falling In Love At A Coffee Shop](8fallinginlove)


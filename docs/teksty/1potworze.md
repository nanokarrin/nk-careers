---
id: 1potworze
title: Psychopass - Namae No Nai Kaibutsu (Potworze)
sidebar_label: Psychopass - Namae No Nai Kaibutsu
---
### Info
>Tekst: Mohohan  
>Pobierz mp3: https://mp3.nanokarrin.pl/  

<iframe width="560" height="315" src="https://www.youtube.com/embed/oPzaEmfF4tw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Tekst
Z marzeń upleciona baśń  
Całkiem rozpłynęła się  
Tkwiąc długo wśród szpitalnych ścian  
Nie dokończyłam wcale jej  

Czerwony księżyc daje znak  
Przebija warstwy nocnych mgieł  
Patrz, jak oświetla moją twarz  
Odwrócić wzroku nie waż się  

Żelaznych krat widoku nie zapomnę  
Od pierwszych dni mi ciągle towarzyszy  
By spełnić sny nie zatrzymujmy się  
Aż zadość stanie się sprawiedliwości  
Modlitwy wznoś, by w zamian zyskać siłę  
I wrogów zniszcz nim zginiesz od ich broni  
Nim zbraknie sił dopilnujemy, by  
Karma spotkała ich, kto inny, jak nie my  
Potworze  

Od ciszy w uszach dźwięczy mi  
Kolczasty drut kaleczy pierś  
Z wszystkich melodii dawnych dni  
Nie został ani jeden dźwięk  

Wraz z ciszą nieprzerwany deszcz  
Bezużytecznym czyni wzrok  
Rozmywa wszystko wokół mnie  
Lecz nadal pewny jest mój krok  

Niech czarny deszcz pokryje wszystko smołą  
I schowa to, co przeciw mnie się burzy  
Tych świadków, co dzielili ten sam los  
A potem mnie szaloną nazywali  
Chcę tylko, by wygrała sprawiedliwość  
Więc wesprzyj mnie, bym mogła nieść jej pomoc  
Za każdą z blizn odpłaćmy słono im  
Zostań wspólnikiem mym, kto inny, jak nie ty  
Potworze  

Ach, gdzie ten świat, w którym Bóg strzeże swoich praw, oh  

Żelaznych krat widoku nie zapomnę  
Od pierwszych dni mi ciągle towarzyszy  
By spełnić sny nie zatrzymujmy się  
Aż zadość stanie się sprawiedliwości  
Modlitwy wznoś, by w zamian zyskać siłę  
I wrogów zniszcz nim zginiesz pod ostrzałem  
Nim zbraknie sił dopilnujemy, by  
Karma spotkała ich, chodź ze mną  
Błagam  

Niech czarny deszcz pokryje wszystko smołą  
I schowa to, co przeciw mnie się burzy  
Tych świadków, co dzielili ten sam los  
A potem mnie szaloną nazywali  
Chcę tylko, by wygrała sprawiedliwość  
Więc wesprzyj mnie, bym mogła nieść jej pomoc  
Za każdą z blizn odpłaćmy słono im  
Zostań wspólnikiem mym, kto inny, jak nie ty  
Potworze  

---
id: 4purplekisszombie
title: Purple Kiss - Zombie
sidebar_label: Purple Kiss - Zombie
---
### Info
>Tekst: Daichi, Kocz, Ghostwriter (rap)   
>Korekta: Pchełka  
>Pobierz mp3: https://mp3.nanokarrin.pl/

<iframe width="560" height="315" src="https://www.youtube.com/embed/04OQWioBcN4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Tekst
Zombi bibibi bibibibibibibi	  				
Zombi				  	
					
Skarbie, kiedy czuję twój zapach   					
To chcę, chcę, chcę, chcę, chcę	  				
Coraz bardziej móc się nim upajać  					
W nim topić się			  		
					
Nawet nie myśl o tym, kotku, że ja spuszczę z oka cię					  
Zawsze staram się być przed tobą					  
By w każdej wygrać grze					  
					
Wielki czuję głód			  		
Na ucieczkę za późno			  		
Chętnie mózgi jem jak Lecter		  			
Budzę ufność				  	
Nie odmówisz piękna, potrafię przykuć wzrok  					
Jestem pewna, nie opuścisz mnie już na krok  					
					
I kiedy wreszcie			  		
W ramionach już trzymasz mnie pewnie	  				
Z każdym twoim gestem 			  		
Chcę cię więcej 			  		
					
Do rana					  
Bawmy się tak				  	
By złamać kilka zasad 			  		
Ty i ja					  
Sprawmy, by w noc			  		
Zbliżyły się nasze ciała		  			
Razem wskoczmy w nią			  		
					
Da-Da-Dalej więc, odrzuć wstyd, niech zapłonie pasja					  
Słońce, skończ wreszcie przed tym się wzbraniać					  
Ta-Ta-Ta-Ta-Tak					  
					
Zombi bibibi bibibibibibibi		  			
Zombi bibibi bibibibibibibi		  			
Zombi bibibi bibibibibibibi		  			
					
Odrzuć wstyd, niech zapłonie dziś pasja	  				
Ta-Ta-Ta-Ta-Tak				  	
Zombi					  
					
Posiadam szyk, chodzi mi dziś		  			
Po głowie taka myśl - ja, ty i flirt	  				
Chwyć za rękę mnie, niech cała noc	  				
Będzie tajemnicza jak gęsty mrok	  				
					
Kiedy czuję cię w pobliżu, to rozbudza we mnie głód					  
Dzisiaj pragnę tylko móc grzeszyć 					  
Smakować twoich ust					  
					
Nie dam ci szansy na sen		  			
Liczę, że mój smak też poznasz		  			
Niech poprowadzi dziś cię 		  			
Czysta żądza				  	
					
Do rana					  
Bawmy się tak				  	
By złamać kilka zasad			  		
(DALEJ!!!)				  	
Ty i ja					  
Sprawmy, by noc				  	
Zbliżyły się nasze ciała		  			
Razem wskoczmy w nią			  		
					  
Da-Da-Dalej więc, odrzuć wstyd, niech zapłonie pasja					  
Słońce, skończ wreszcie przed tym się wzbraniać					  
Ta-Ta-Ta-Ta-Tak					  
					
Zombi bibibi bibibibibibibi		  			
Zombi bibibi bibibibibibibi		  			
					  
Czas już przestać grać fair play	  				
Niech instynkt sam poprowadzi mnie 	  				
Teraz w szczęściu rozpłyń się		   			
Szansę masz, więc korzystaj z niej 	  				
					
Zombi bibibi bibibibibibibi		  			
					
Czas już przestać grać fair play	  				
Niech instynkt sam poprowadzi mnie   
					
					
Odrzuć wstyd, niech zapłonie dziś pasja	  				
					
Do rana					  
Bawmy się tak				  	
By złamać kilka zasad 			  		
Ty i ja					  
Sprawmy, by w noc			  		
Zbliżyły się nasze ciała		  			
Razem wskoczmy w nią			  		
					
Da-Da-Dalej więc, odrzuć wstyd, niech zapłonie pasja					  
Słońce, skończ wreszcie przed tym się wzbraniać					  
Ta-Ta-Ta-Ta-Tak				  	
					
Zombi bibibi bibibibibibibi		  			
Zombi bibibi bibibibibibibi  
					
Zombi bibibi bibibibibibibi  
					
Odrzuć wstyd, niech zapłonie dziś pasja					  
Ta-Ta-Ta-Ta-Tak					  
Zombi 		  


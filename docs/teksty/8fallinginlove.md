---
id: 8fallinginlove
title: Landon Pigg - Falling In Love At A Coffee Shop
sidebar_label: Landon Pigg - Falling In Love At A Coffee Shop
---
### Info
>Tekst: Daichi, Liska + korekta Pchełka
>Pobierz mp3: https://mp3.nanokarrin.pl/

<iframe width="560" height="315" src="https://www.youtube.com/embed/n-0PBWUlc_8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Tekst
Przyznam, że widok twój cały przysłania mi świat  
I powód tego znam - miłości poznałam smak  
Gdy nasze oczy spotkały się znów  
To brakło mi tchu  

Miłość mi w duszy gra i uciec nie mam już jak  

Tak dobrze jak ty mnie nie poznał nikt  
Dziś twoja obecność dodaje mi sił  

Słychać twój głos, czuć słodką woń  
Dziś w naszej kawiarni przy tobie chcę siąść  
Gdy mówisz “cześć”  
Znowu rozpływam się  
 
Przyznam, że widok twój cały przysłania mi świat  
I powód tego znam - miłości poznałam smak  
Gdy w twoich oczach dostrzegłam błysk, to  
W ich wpadłam toń  
To musi być znak  
Że miłość mi wciąż w duszy gra  

I nie zaprzeczę, że chciałabym, byś  
Już zawsze przez życie mógł obok mnie iść  

Słychać twój głos, czuć słodką woń  
Dziś w naszej kawiarni przy tobie chcę siąść  
Gdy mówisz “cześć”  
Znowu rozpływam się  
Słychać twój głos, czuć słodką woń  
Dziś w naszej kawiarni przy tobie chcę siąść  
Gdy mówisz “cześć”  
Znowu rozpływam się  

Hej, kocham cię  
Hej, kocham cię  
Skarbie, wiesz?  
---
id: 7plonrzezniku
title: Wiedźmin – Płoń, rzeźniku, płoń
sidebar_label: Wiedźmin – Płoń, rzeźniku, płoń
---
### Info
>Tekst: Michał Skarżyński (część wykorzystana w serialu), Elven (reszta)
>Pobierz mp3: https://mp3.nanokarrin.pl/

<iframe width="560" height="315" src="https://www.youtube.com/embed/r0NGuIhM35s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Tekst
Wiem że w zdrowiu trwasz  
Hmmm... jaka szkoda  
Bez Ciebie i ja  
Dobrze się mam  

Czy mój śpiew nie bawił Cię  
Panie: "HA WSZYSTKO WIEM!"  
Teraz żar  
Spali wspomnień mych  
ślad  

Gdy wyruszysz na  
Nowy szlak  
Będziesz go  
Przemierzał całkiem sam  

Czy w ogóle o to dbasz  
Wszak masz broń i tę durną twarz  
Więc teraz patrz  
Jak ten żar  
pali wspomnień mych  
ślad  

Panie i Panowie byliście dziś wspaniali, pamiętajcie by rzucić grosik A jakby mnie ktoś szukał Będę przy szynkwasie!  

Czego tak żal  
Co za głupia tęsknota  
Tyle przygód, tyle chwil i miejsc  
A Ty mi mówisz precz, więc  
Czego żal  
Płoń rzeźniku płoń  

Choćbym tysiąc wyśpiewał  
Tu strof  
To prawdy nie będzie w nich więcej niż w słowie  
Płoooń  
Płoń rzeźniku płoooń  
Płoń rzeźniku płoń  
Płoń x9  
Spłonie więc, po tym co było ślad  
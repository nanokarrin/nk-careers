---
id: 3cityofstars
title: La La Land - City of Stars
sidebar_label: La La Land - City of Stars
---
### Info
>Tekst: Kat  
>Korekta: Pchełka, Hikari, Mirabelle  
>Pobierz mp3: https://mp3.nanokarrin.pl/

<iframe width="560" height="315" src="https://www.youtube.com/embed/cU8jso_5g-s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Tekst
Stolico gwiazd  
Czy to dla mnie błyszczysz tak?  
Stolico gwiazd  
Tyle jeszcze skrywasz kart  
Kto wie?  
Poczułem to, gdy pierwszy raz objąłem cię  
Że nasze sny  
Wreszcie spełniły się   

Stolico gwiazd  
Za jednym goni każdy z nas  
Idąc przez pub  
I przez kurtynę dymną zatłoczonych knajp  
Nasz cel  
To znaleźć miłość, którą odwzajemni ktoś  
Ten pęd  
Ten wzrok  
Ten gest  
Ten krok  


Kogoś, kto wesprze cię gdy  
Chcesz ziścić swe sny  
Mrok zdusić i stawić światu czoła  
Kto czule szepnie ci, że  
Nie jesteś już sam  

Nie dbam o to, czy wiem  
Gdzie skończę swój bieg  
Bo w moim sercu gra muzyka  
W rytmie ram pam pam pam  

Niech zatrzyma się czas  

Stolico gwiazd  
Czy to dla mnie błyszczysz tak?  
Stolico gwiazd  
Jasno tak lśnisz pierwszy raz  


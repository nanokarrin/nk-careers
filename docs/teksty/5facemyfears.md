---
id: 5facemyfears
title: Kingdom Hearts III - Face My Fears 
sidebar_label: Kingdom Hearts III - Face My Fears 
---
### Info
>Tekst: Krysta
>Pobierz mp3: https://mp3.nanokarrin.pl/

<iframe width="560" height="315" src="https://www.youtube.com/embed/rQdXt5PRzSQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Tekst
Czy  
Wstrzymać dech i biec?  
Czy  
W otchłań rzucić się?  
Znów  
Wszystko idzie źle  
Tak jest  
Tak już jest  

Ref:  
Czas już iść, czas już iść, przestać w miejscu tkwić  
O, czas już iść, czas już iść, przestać w miejscu tkwić  
Pragnę odkryć miejsca, których jeszcze nie zna nikt  
Sprawię, że wstanie świt  

Sprawię, że wstanie świt  

Czy  
Walczyć z całych sił?  
Czy  
Z pola bitwy zejść  
Wiem, nikt  
Nie będzie za mnie żył  
Tak jest  
Tak już jest  

Ref.  
Czas już iść, czas już iść, przestać w miejscu tkwić  
O, czas już iść, czas już iść, przestać w miejscu tkwić  
Pragnę odkryć miejsca, których jeszcze nie zna nikt  
Sprawię, że wstanie świt  

Mengkhayal, mengkhayal, mengkha-mengkhayal, mengkhayal, mengkha-mengkhayal  
Mengkhayal, mengkhayal, mengkha-mengkhayal, mengkhayal, mengkha-mengkhayal  
Mengkhayal, mengkhayal, mengkha-mengkhayal, mengkhayal, mengkha-mengkhayal  

Sprawię, że wstanie świt  

Sprawię, że wstanie świt  

Ref.  
Czas już iść, czas już iść, przestać w miejscu tkwić  
O, czas już iść, czas już iść, przestać w miejscu tkwić  
Pragnę odkryć miejsca, których jeszcze nie zna nikt  
Sprawię, że wstanie świt  

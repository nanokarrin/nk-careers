---
id: regulaminogolny7
title: Zachowanie Członków 
sidebar_label: 6. Zachowanie Członków wobec siebie
---

## 6. Zachowanie członków wobec innych Członków poza serwerem NanoKarrin

### 6.1 Motywacja
NanoKarrin to społeczność, która rządzi się określonymi zasadami. Z racji tego, że NanoKarrin w sensie społecznym istnieje przede wszystkim na serwerze Discord, tam przede wszystkim te zasady są egzekwowane. Oprócz tego chcemy je też egzekwować, kiedy interakcje między Członkami zachodzą poza serwerem, są niezgodne z regulaminem grupy i działają na szkodę Członka, który jest częścią tej interakcji. Chcemy, by Członkowie NanoKarrin wykazywali się pewnym podstawowym poziomem szacunku do siebie. Jeśli tego szacunku brakuje, to mamy prawo kwestionować jakość Członkostwa danej osoby w tej społeczności.

### 6.2 Od kogo chcemy egzekwować
Od Członków społeczności, w praktyce od każdego na serwerze NanoKarrin (nie ograniczamy się do ludzi z rangą NK).
#### Ciągłość członkostwa
Dla uproszczenia przyjmujemy, że człowiek jest Członkiem społeczności od momentu dołączenia do niej do momentu ewentualnego usunięcia przez moderację lub koordynację. Nie ma znaczenia, czy ktoś w międzyczasie wyjdzie z serwera czy zadeklaruje brak przynależności do społeczności. Zresztą w takich przypadkach nie powinno to mieć znaczenia dla osoby łamiącej regulamin, bo wszystkie kary są związane z zawieszeniem lub pozbawieniem tego członkostwa. 

### 6.3 Co chcemy egzekwować
Regulamin, w szczególności te części które dotyczą ogólnych relacji międzyludzkich i które w swojej naturze nie są ograniczone do korzystania z Discorda.

### 6.4 Kiedy chcemy egzekwować
Muszą zaistnieć następujące warunki, żeby moderacja mogła podjąć działania:
* Zaistniała interakcja pomiędzy dwoma Członkami społeczności, która łamie zasady określone w regulaminie grupy.
* Przynajmniej jeden z Członków tej interakcji czuje się tą sytuacją pokrzywdzony i jest gotów osobiście zgłosić ten fakt.
* Istnieje dostateczny materiał dowodowy, który pozwoli moderacji stwierdzić, czy sytuacja miała miejsce i w jakim stopniu łamie regulamin.
* Sytuacja miała miejsce w ciągu miesiąca od zgłoszenia.
* Sytuacja miała miejsce po wprowadzeniu w życie niniejszego dokumentu i miała miejsce, kiedy zarówno osoba poszkodowana, jak i łamiąca regulamin były Członkami społeczności NanoKarrin.

## 6.5 Czego oczy nie widzą, tego sercu nie żal
W przeciwieństwie do wykroczeń z Discorda, które są potencjalnie widoczne dla moderatora i to wystarcza do ukarania Członka, wiele z powyższych zapisów może być egzekwowanych tylko, jeśli ktoś dowie się o ich przekroczeniu. Jeśli Członek będzie dostatecznie dyskretny, by nikt się nie dowiedział o jego wykroczeniach, nie będzie konsekwencji w postaci akcji moderatora. Jeśli jednak jakiekolwiek akcje moderatorskie zostaną podjęte, powinno to być dostatecznym dowodem na to, że dyskrecja nie została zachowana.

### 6.6 Taryfikator kar
Nieprzestrzeganie zasad może skutkować następującymi karami:  
Pouczenie, ostrzeżenie, czasowe usunięcie z serwera, usunięcie z serwera
---
id: regulaminogolny5
title: Wytyczne społeczności
sidebar_label: Wytyczne społeczności
---

1. Szanujmy prywatność: rozpowszechnianie screenów z rozmów niepublicznych powinno odbywać się za zgodą wszystkich uczestników rozmowy.

2. Dbajmy o dobry wizerunek NK:
* Wewnątrz grupy  
Jesteśmy częścią NK, bo chcemy nią być, chcemy tworzyć społeczność, do której warto dołączyć i w której warto pozostać. Niech to, co zachęca do dołączenia ludzi spoza grupy, ma odzwierciedlenie także w środku.
* Poza grupą
NanoKarrin współpracuje z zewnętrznymi podmiotami, a nasze przyszłe współprace zależą także od tego, jak wywiązujemy się w kwestiach PR-owych. Każdy członek NK powinien dbać o wizerunek naszych partnerów, działać z nimi w jednej drużynie. Dotyczy to zarówno online, jak i offline, ze szczególnym wskazaniem na konwenty, gdzie jesteśmy postrzegani jako członkowie grupy od początku do końca wydarzenia, nawet jeśli w danym momencie nie pełnimy dyżuru. W skrócie: nie dopuszczajmy do sytuacji, gdzie nasze opinie, zachowanie czy dostęp do wewnętrzych informacji negatywnie rzutują na wizerunek partnera.

3. Szanujmy innych twórców, wewnątrz grupy i poza nią. Jeśli chcemy krytykować, krytykujmy konstruktywnie.
Przykład, to robić, można znaleźć [na tej stronie](https://www.poradnikzdrowie.pl/psychologia/rozwoj-osobisty/zasady-konstruktywnej-krytyki-czyli-jak-krytykowac-aby-uzyskac-poprawe-aa-T494-YHgs-4mwY.html).
> Konstruktywna krytyka – rodzaj krytyki, która charakteryzuje się tym, że osoba krytykująca przedstawia lub sugeruje jednocześnie sposób lub sposoby rozwiązania problemu poddanego krytyce. Można ten rodzaj krytyki zaliczyć do krytyki wyższego poziomu w przeciwieństwie do zwykłej krytyki pozbawionej tej cechy. (za: wikipedia.pl)

4. Wspierajmy młodszych stażem. Pomagajmy nowym członkom (ranga @Junior) odnaleźć się w grupie i wspierajmy ich rozwój. Okażmy im cierpliwość i zrozumienie. Dawajmy ciastka.
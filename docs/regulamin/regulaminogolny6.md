---
id: regulaminogolny6
title: Zachowanie Członków na konwentach
sidebar_label: 5. Zachowanie Członków na konwentach
---

## 5. Zachowanie członków NanoKarrin poza serwerem Discord na konwentach i innych publicznych wystąpieniach

### 5.1 Motywacja
NanoKarrin chce mieć narzędzia do zarządzania swoimi Członkami poza serwerem Discord w sytuacjach, w których Członkowie publicznie reprezentują grupę i mogą działać na jej szkodę.

### 5.2 Kiedy Członkowie reprezentują grupę:
#### 5.2.1 Na konwentach
* Zawsze w salce/stoisku NK  
* Zawsze w sleepie NK  
* Na terenie conplace, jeśli powołują się na swoje członkostwo w grupie lub noszą coś co wyraźnie identyfikuje ich przynależność - np. koszulkę, bluzę.  
#### 5.2.2 Na streamach, innych serwerach Discord, forach, mediach społecznościowych
* Kiedy powołują się na swoje członkostwo w NK lub kiedy rozmowa toczy się o NK i ich członkostwo wynika z kontekstu
* Kiedy rozmowa odbywa się w miejscu publicznym (np. Twitch) lub semipublicznym (np. serwer Discord, na który łatwo jest wejść i jest na nim wiele osób)

### 5.3 Czego nie wolno robić Członkowi, który reprezentuje grupę:
* Łamać zasad ustalonych przez organizatora eventu, w tym w szczególności:  
x Pić alkoholu i przyjmować innych zakazanych substancji psychoaktywnych  
x Uprawiać seksu  
* Łamać zasad społeczności NanoKarrin określonych w regulaminie.
* Obrażać członków społeczności NanoKarrin, jak i całej grupy jako całość.
* Obrażać organizatorów konwentów i grup dubbingowych.
* Obrażać innych uczestników wydarzenia.
* Wpływać negatywnie na wizerunek organizatora eventu (mp. narzekać, plotkować publicznie lub semi-publicznie).

### 5.4 Specjalne obostrzenia
#### 5.4.1 Twórcy atrakcji
##### 5.4.1.1 Ubiór i higiena
Nawet w tak niezobowiązującym środowisku jak konwent twórca atrakcji powinien zachować minimum schludności. Powinien być w pełni ubrany i w miarę konwentowych możliwości czysty.
##### 5.4.1.2 Zachowanie wobec innych uczestników konwentu
Wszyscy uczestnicy konwentu są zarówno potencjalnymi odbiorcami naszej twórczości, jak i Członkami grupy. Choćby z tego powodu powinniśmy odnosić się do nich z szacunkiem, życzliwością i w miarę możliwości być pomocni i responsywni, gdy zadają pytania, nawet jeśli pytają o te same lub podobne rzeczy.
##### 5.4.1.3 Zachowanie wobec innych grup dubbingowych i ludzi na scenie dubbingowej
Choć napięcie pomiędzy grupami dubbingowymi jest normalne, nikomu nie jest do twarzy z byciem niemiłym dla innych ludzi w branży. W szczególności w sytuacjach publicznych bądźmy uprzejmi, unikajmy otwartej krytyki, a jeśli już potrzebujemy kogoś krytykować, róbmy to konstruktywnie i możliwie przyjaźnie.  
Niezależnie od animozji pomiędzy grupami i Członkami lepiej jest myśleć o nich jako kolegach z tej samej branży pracujących nad innymi projektami, a nie konkurentach.

#### 5.4.2 Organizatorzy z ramienia NK
Organizatorzy NK w szczególności reprezentują grupę, bo pokazują nie tylko, jak wygląda Członek tej grupy, ale także jak ta grupa jest zorganizowana i jakie ma relacje z innymi organizacjami.
##### 5.4.2.1 Organizator powinien
* Traktować organizatorów konwentu, z którym pracuje, jako swoich dorosłych współpracowników, z którymi próbuje osiągnąć jeden cel
* O ludziach, z którymi współpracujemy, mówić dobrze, jeśli jest to tylko możliwe, a krytykować ostrożnie i z taktem
* Traktować uprzejmie wszystkich organizatorów i reprezentantów innych organizacji
* Być frontem dla całego zespołu, który robi program na dany konwent, a, jeśli jest to możliwe, być frontem nawet dla całej grupy
##### 5.4.2.2 Organizator nie powinien
* Tłumaczyć się z niepowodzeń, zwalając winę na organizację konwentu lub Członków swojego zespołu
* Publicznie prać brudów swojego zespołu
* Wyciągać publicznie lub semi-publicznie szczegółów współpracy z konwentami

### 5.5 Czego oczy nie widzą, tego sercu nie żal
W przeciwieństwie do wykroczeń z Discorda, które są potencjalnie widoczne dla moderatora i to wystarcza do ukarania Członka, wiele z powyższych zapisów może być egzekwowanych tylko, jeśli ktoś dowie się o ich przekroczeniu. Jeśli Członek będzie dostatecznie dyskretny, by nikt się nie dowiedział o jego wykroczeniach, nie będzie konsekwencji w postaci akcji moderatora. Jeśli jednak jakiekolwiek akcje moderatorskie zostaną podjęte, powinno to być dostatecznym dowodem na to, że dyskrecja nie została zachowana.

### 5.6 Taryfikator kar
Nieprzestrzeganie zasad może skutkować następującymi karami:  
Pouczenie, ostrzeżenie, czasowe usunięcie z serwera, usunięcie z serwera
---
id: regulaminogolny1
title: Discord i Forum
sidebar_label: 1. Discord i Forum
---

## 1.1 Discord
### 1.1.1 Link do serwera Discord
Serwer Discord NK znajduje się pod adresem https://discord.gg/nanokarrin

### 1.1.2 Nicki na serwerze Discord
**Użytkownicy serwera** muszą mieć pseudonimy, które da się oznaczyć znakami z polskiej klawiatury i nie mogą być pisane innym fontem niż domyślny.  
 - Jeśli login nie spełnia powyższych wymagań, zostanie on zmieniony na podobnie brzmiący nick lub "pszemek", jeśli login jest zupełnie nieczytelny.
 - O zmianę pseudonimu można prosić na kanale #zgloszenia-problemy  

**Każdy członek NanoKarrin** ma obowiązek posiadać na serwerze Discord pseudonim taki sam jak w spisie członków NK (tzw. Spisie Powszechnym).
- Pseudonim może zawierać dopiski na początku nicku, jednak mogą być to jedynie emotki/grafiki UTF8 i muszą być oddzielone od nicku spacją
- Pseudonim może zawierać dopiski na końcu nicku, jednak jeśli nie są emotką, muszą one być oddzielone spacją
- Forma pseudonimu może być minimalnie zmodyfikowana, jednak tak, by pseudonim nadal pozwalał na łatwe wyszukanie po nicku i rozpoznanie danego członka
- Moderacja może zmienić pseudonim, jeśli uzna, że wprowadza w błąd innych członków serwera
- Każdą zmianę nicku należy zgłosić moderacji

### 1.1.3 Regulamin Discorda
1.1.3.1 Należy korzystać z kanałów zgodnie z ich przeznaczeniem - każdy kanał ma swój własny opis. Zakaz prowadzenia dyskusji na kanałach:
- #zgłoszenia_problemy
- #szukam_ekipy
- #castingi_nk   
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie

1.1.3.2 Multimedia, które należy oznaczyć jako “spoiler” i ostrzec przed ich treścią:
- Migające animacje mogące wywołać reakcje epileptyczne
- Multimedia zdecydowanie głośniejsze od przyjętych standardów\* (tzw. ear rape'y)   
\*Za standardową głośność uznajemy -13 LUFS, pliki o głośności wyższej niż -5 LUFS powinny być oznaczone. W praktyce jeśli słuchamy czegoś na serwisie streamingowym typu YouTube czy Spotify i mamy ustawione pod niego komfortową głośność, a chcemy wrzucić coś co zmusza nas do jej przyciszenia to powinniśmy o tym uprzedzić.      
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie, (Czasowe) usunięcie z serwera

1.1.3.3. Niepokojące i drastyczne treści:  
A. Niepokojące treści wymienione poniżej należy oznaczyć jako “spoiler”, ostrzec przed ich treścią i umieszczać wyłącznie na kanałach NSFW (+18).
- soczysta filmowa przemoc
- “nieszczęśliwe wypadki”
- inne szokujące lub niesmaczne treści mogące wywołać przerażenie takie jak insekty lub “jumpy scare’y”   
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie, (Czasowe) usunięcie z serwera

B. **Całkowity zakaz** publikacji wymienionych drastycznych treści:
- realistyczna przemoc
- fekalia
- rozczłonkowane ciała   
- prawdziwe sceny śmierci lub bezpośrednie wyniki tych scen
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie, (Czasowe) usunięcie z serwera

1.1.3.4 Rozmowy na temat narkotyków i seksu należy prowadzić wyłącznie na kanałach NSFW (+18). Zakaz publikowania:
- Nagości i pornografii
- Opisów stosunków płciowych   
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie, (Czasowe) usunięcie z serwera

1.1.3.5 Zakaz publikowania:
- Gróźb karalnych
- Niepublicznych danych osobowych, zdjęć, nagrań głosu i wideo innych użytkowników bez ich zgody
- Multimediów/stron/linków zawierających złośliwe oprogramowanie   
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie, (Czasowe) usunięcie z serwera

1.1.3.6 Zakaz wypowiedzi zawierających elementy wyszydzające, poniżające osobę lub grupy osób ze względu na ich cechy takie, jak np. płeć, kolor skóry, orientację seksualną, wygląd, wyznanie, niepełnosprawność.   
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie, (Czasowe) usunięcie z serwera

1.1.3.7 Zakaz SPAM-u, w tym:
- Wielokrotnego wysyłania wiadomości reklamowych i linków
- Wysyłania zaproszeń discord przez osoby spoza Grupy
- Oznaczania rang NK przez osoby spoza Grupy
- Oznaczania ról przez osoby spoza Grupy w celu autopromocji
- @Wzmieniania ról niezgodnie z ich przeznaczeniem
- Rozsyłania powtarzalnych wiadomości prywatnych do członków serwera spoza swojej listy znajomych   
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie, (Czasowe) usunięcie z serwera

1.1.3.8 Członkom zespołu koordynacyjnego NK zabrania się posiadania opisów, awatarów, nazw i dopisków do nazw zawierających:
* Powiązania z nazizmem lub zbrodniami wojennymi (np.: awatary z Hitlerem, Stalinem)
* Słowa, zwroty lub gesty mogące zostać uznane za wulgarne (np.: środkowy palec)
* Słowa lub zwroty mogące zostać uznane za obraźliwe lub dyskryminujące osoby ze względu na pewną cechę, w tym: wygląd, chorobę, okres rozwojowy, niepełnosprawność, orientację seksualną, płeć, wyznawaną religię, światopogląd, narodowość lub rasę. (np.: "fag", "nigga", "rak")
* Elementy nawiązujące do seksu. W tym: ahegao, emotki kabaczka/banana/kropel/brzoskwini itp., animacje sugerujące orgazm
* Elementy wizerunku i nazw innych fandubberów lub grup dubbingowych (np.: twarze osób z fandubbingu, nazwy grup dubbingowych)
* Narkotyki, alkohol, leki i substancje psychoaktywne (np.: marihuana, strzykawki, tabletki)
* Elementy o zabarwieniu politycznym (w tym twarzy polityków)  
  **Taryfikator kar**: Pouczenie, usunięcie wiadomości i poinformowanie o tym autora, ostrzeżenie, (Czasowe) Odebranie funkcji na serwerze, (Czasowe) usunięcie z serwera

1.1.3.9 Moderator Discord ma prawo czasowo zakazać na kanale używania multimediów i emotek (w tym pochodnych typu: "xD", "iks de", "lol" itp.), by ograniczyć SPAM, trolling lub poziom emocjonalny dyskusji.  
  **Taryfikator kar**: Usunięcie multimediów wysłanych po zakazie, Czasowe odebranie funkcji na serwerze (maks. 1 dzień), Czasowe usunięcie z serwera (maks. 1 dzień)


## 1.2 Kary
### 1.2.1 Nieprzestrzeganie zasad może skutkować następującymi karami:
- Pouczenie
- Usunięcie wiadomości i poinformowanie o tym autora
- Ostrzeżenie
- Wyciszenie
- Usunięcie multimediów
- (Czasowe) Odebranie funkcji na serwerze
- (Czasowe) Usunięcie z serwera  

**Nagminne złe zachowanie może skutkować usunięciem z Grupy.**

### 1.2.2 Ostrzeżenia   
Ostrzeżenia są przyznawane zgodnie z taryfikatorem kar za naruszanie regulaminu i funkcjonują w formie analogicznej do Żółtych Kartek w piłce nożnej. To jest: każdy użytkownik, który otrzyma ostrzeżenie, po przekroczeniu danego limitu ostrzeżeń zostanie ukarany usunięciem z serwera na dwa miesiące. Użytkownicy niebędący członkami Grupy zostaną usunięci z serwera przy otrzymaniu drugiego ostrzeżenia, natomiast członkowie Grupy zostaną usunięci z serwera dopiero przy trzecim ostrzeżeniu.  
**UWAGA:** ostrzeżenia funkcjonują równolegle do innych kar, a moderatorzy mają prawo z miejsca przyznać surowszą karę niż ostrzeżenie, jeśli surowsza kara znajduje się w taryfikatorze kar dla danego przewinienia.

### 1.2.3 Tryb Slow Mode   
W przypadku, gdy moderator lub koordynator uzna, że dyskusja prowadzona na dowolnym kanale tekstowym zaczyna przybierać formę kłótni, może zostać wprowadzony na kanał “Slow Mode”. Po włączeniu tego trybu, każdy użytkownik na kanale jest ograniczony do wysłania jednej wiadomości tekstowej w ciągu ustawionego przedziału czasu. Na przykład: jedna wiadomość na 30 sekund.

### 1.2.4 Tryb Nadzwyczajny   
Regulamin nie przewiduje wszystkich sytuacji, jakie mogą się pojawić. W takim przypadku ostateczną decyzję w trybie nadzwyczajnym podejmuje Koordynator Moderacji, którego obowiązkiem jest później uwzględnić takie sytuacje w kolejnej wersji regulaminu.

### 1.2.5 Ścieżka eskalacji   
Jeśli nie zgadzasz się z decyzją moderatora lub masz uwagi odnośnie jego funkcjonowania, zgłoś to Koordynatorowi Moderacji lub anonimowo w ankiecie.
**Link do ankiety**: https://nanokarrin.pl/ZazaleniaNaModeracje
  
**W przypadku uwag odnośnie Koordynatora Moderacji, zgłoś to Królowi.**

## 1.3 Forum
Archiwalne forum znajduje się pod adresem https://nanokarrin.pl/forum/index.php

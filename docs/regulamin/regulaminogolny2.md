---
id: regulaminogolny2
title: Zasady członkostwa i rekrutacja
sidebar_label: 2. Zasady członkostwa i rekrutacja
---
## 2. Zasady członkostwa i rekrutacja
### 2.1 Regulaminy rekrutacji (2020/05/15)
Regulaminy rekrutacji dostępne są w [sekcji Rekrutacja](/docs/rekruogolne).

### 2.2 Zasady członkostwa w Grupie (2024/03/03)
2.2.1 Posiadanie rangi "Członek NK" na Discordzie jest równoznaczne z członkostwem w NanoKarrin.

2.2.2 Tylko posiadanie rangi "Członek NK" daje dostęp do zamkniętej części Discorda, dostępnej wyłącznie dla członków Grupy. 

2.2.3 Członek NanoKarrin, który opuścił Grupę lub stał się nieaktywny, może w każdym momencie odzyskać zdobyte wcześniej rangi. Wyjątki:   
- członkowie, którzy nie przeszli z wynikiem pozytywnym dwóch etapów ewaluacji, muszą podejść do rekrutacji na nowo.   
- członkowie, którzy opuścili Grupę w związku z naruszeniem regulaminu, muszą otrzymać od Koordynatorów pozwolenie na powrót.   

2.2.4 Członek NanoKarrin nie ma prawa brać wynagrodzenia pieniężnego za produkty dubbingowe tworzone w ramach projektu NanoKarrin. Przyjęcie wynagrodzenia jest równoznaczne z dożywotnim zrzeczeniem się członkostwa w Grupie. Dopuszczalne jest kupowanie produktów dubbingowych od osób niebędących członkami Grupy. Członek, który opuścił Grupę lub stał się nieaktywny (patrz punkt 2.2.2.), po czym przyjął wynagrodzenie za stworzenie produktu dubbingowego do projektu NanoKarrin, traci prawo do powrotu do Grupy.

2.2.5 Lista członków NK znajduje się w Spisie Powszechnym. Link do Spisu Powszechnego znajduje się na kanale discordowym nk-info.

2.2.6 Rangę "Członek NK" można uzyskać przez przejście rekrutacji na rolę projektową (np. aktor) lub funkcyjną (np. przewodnik) (patrz: [Rekrutacja](/docs/rekruogolne).   
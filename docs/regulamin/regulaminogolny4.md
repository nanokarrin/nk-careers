---
id: regulaminogolny4
title: Wydawanie projektów
sidebar_label: 4. Wydawanie projektów
---

## 4. Wydawanie projektów na kanałach dystrybucji NanoKarrin (2023/04/12)
* Przez wydanie projektu rozumie się wpisanie go do kolejki projektów, a następnie opublikowanie na przynajmniej jednym z kanałów dystrybucji NK (np. YouTube, cda.pl, Facebook).  
* Od 1 stycznia 2024 r. wydawanie projektów odbywa się wyłącznie trybem producenckim.
* Do wydania projektu niezbędne są dwie okejki otrzymane podczas Spotkania Producentów i potwierdzone pisemnie przez prowadzącego spotkanie w wątku danego projektu z oznaczeniem Biura Projektowego.     

### 4.1 Kolejka projektów (2023/01/06)
4.1.1 Kolejka znajduje się na Discordzie, na kanale nk-projekty-info i jest jawna dla każdego członka NanoKarrin.
4.1.2 Projekty w kolejce muszą spełniać wymagania zawarte w dokumencie “Czeklisty publikowania filmów” (link dostępny na kanale z kolejką).   
4.1.3 Wydawany jest jeden projekt w tygodniu, w sobotę, chyba że opiekun kolejki zarządzi inaczej.   
4.1.4 Opiekun kolejki może zmieniać kolejkę zgodnie z interesem Grupy lub własnym widzimisię.   

### 4.2 Rozpowszechnianie projektów (2023/01/06)
##### 4.2.1 Prawa do produktów składających się na projekt
Każda osoba uczestnicząca w projekcie NanoKarrin zachowuje prawo do rozporządzania stworzonymi przez siebie produktami dubbingowymi przekazanymi NanoKarrin na potrzeby realizacji projektu, o ile nie narusza to praw własności produktów pozostałych członków ekipy projektu.
>Przykład: Wokalista śpiewający tekst musi uszanować prawo tekściarza do wyboru sposobu i czasu pierwszej publikacji tekstu.

##### 4.2.2 Przekazanie produktu NanoKarrin 	
Przekazanie swojego produktu dubbingowego osobie prowadzącej projekt NanoKarrin jest jednoznaczne z udzieleniem Grupie niezbywalnej zgody na użytkowanie i dystrybucję tego produktu w celu realizacji projektu. Zgoda i prawa do realizacji projektu z użyciem produktów należą do NanoKarrin, a nie do osoby prowadzącej opiekującej się projektem.
>Przykład: Aktor, wysyłając swoje nagranie, udziela NanoKarrin niezbywalnej zgody na wykorzystanie nagrania w ramach projektu, nawet po zmianie osoby prowadzącej.

##### 4.2.3 Redystrybucja projektów (2018/08/01)
Dopuszcza się, aby wydany projekt NanoKarrin został zamieszczony w niezmienionej formie na kanale osób trzecich pod dwoma warunkami:
* Opis będzie zawierał pełny skład ekipy biorącej udział w projekcie.
* Opis będzie zawierał link prowadzący do tego projektu na kanale dystrybucji NanoKarrin (np. do YT, fb).

##### 4.2.4 Redystrybucja produktów składowych projektów
Informacje o możliwości i warunkach redystrybucji konkretnych produktów dubbingowych projektu NanoKarrin należy pozyskać u autora danego produktu, chyba że w opisie projektu zawarte jest jednoznaczne przyzwolenie. Autorzy są zawsze wymieniani w opisie opublikowanego projektu.

##### 4.2.5 Reupload projektów zewnętrznych 
Projekt zewnętrzny może być opublikowany na kanałach dystrybucji NanoKarrin, jeżeli:
- prowadzącym projekt jest Członek NK
- zawiera w dowolnym miejscu logo NanoKarrin lub napis "we współpracy z NanoKarrin"
- cała ekipa projektu wyraziła zgodę na reupload
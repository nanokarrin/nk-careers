---
id: konwenty
title: Konwenty
sidebar_label: Konwenty
---

## Koordynator Kawalerii Konwentowej
- Zajmuje się organizacją zespołu kawalerii konwentowej, przede wszystkim Organizatorów i Wsparcia.
- Razem z organizatorami podejmuje decyzje, na które konwenty NK pojedzie.
- Utrzymuje wiedzę zespołu kawalerii na właściwym poziomie. Organizatorzy powinni wiedzieć, nad jakimi konwentami aktualnie zespół pracuje i o jakich myśli. 
- Sprawdza status organizacji poszczególnych konwentów, czy wszystko idzie zgodnie z planem, czy nikt nie potrzebuje pomocy.

## Organizator Kawalerii 
- Zarządza całym procesem uczestnictwa NK na konwencie po tym, jak to uczestnictwo zostało potwierdzone przez niego wspólnie z koordynatorem. Każdy konwent, w którym NK bierze udział, powinien mieć przypisanego jednego lub więcej Organizatorów.
- Może delegować zadania do dowolnej liczby ludzi, ale jest jednostkowo odpowiedzialny za organizację danego konwentu. Powinien być w stanie sam zauważać i rozwiązywać problemy - nie musi tego robić samotnie, ale powinien potrafić brać za nie odpowiedzialność. 
- Może, ale nie musi wykazywać inicjatywę w znajdowaniu konwentów, na które NK może pojechać. Ostatecznie udział w konwencie zawsze musi zostać omówiony z Koordynatorem i innymi Organizatorami.
- Informuje Koordynatora oraz innych Organizatorów o stanie organizacji danego konwentu.

## Wsparcie Kawalerii
- Wspomaga pracę zespołu przed i pomiędzy konwentami. Może to być kwestia tworzenia grafik, robienia reasearchu, szukania kontaktów czy nawet po prostu aktywnego brania udziału w rozmowach, burzach mózgów etc. Cokolwiek, co pomaga zespołowi, ale nie jest związane z konkretnym konwentem i jego organizacją.

## Kawalerzysta
- Jedzie na konwent i robi na nim program. Może to polegać na prowadzeniu własnych atrakcji, wspieraniu atrakcji zdefiniowanych przez Organizatora, śpiewaniu na koncercie albo wspieraniu uczestnictwa NK na konwencie w jakiś inny sposób, jeśli taka potrzeba została zdefiniowana przez Organizatora.

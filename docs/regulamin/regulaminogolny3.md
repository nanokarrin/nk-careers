---
id: regulaminogolny3
title: Tworzenie projektów
sidebar_label: 3. Tworzenie projektów
---

## 3. Tworzenie projektów NanoKarrin
### 3.1 Kto może prowadzić projekt (2024/02/13)
Projekt może prowadzić każdy członek NK. Jeśli podejmie się prowadzenia projektu, otrzymuje wtedy rangę "Producent". Każdy projekt producencki powinien mieć post na discordowym forum nk-projekty.   
Uwaga, ranga "Producent" nie jest zastrzeżona dla prowadzącego projekt. Każda osoba zainteresowana procesem wydawniczym w grupie może nadać sobie tę rangę, by otrzymywać powiadomienia.    

### 3.2 Kto może uczestniczyć w projekcie (2016/11/23)
Osoba prowadząca projekt ma prawo przyjąć do pracy nad projektem dowolną osobę – również bez rangi i spoza Grupy.

### 3.3 Spotkanie Producentów (2024/10/4)
3.3.1 Spotkanie Producentów to spotkanie ogólnogrupowe, odbywające się na kanale głosowym pokoj-nk. Ogłaszane jest jako wydarzenie na serwerze NK.   
3.3.2 Każde spotkanie powinno mieć wyznaczonego prowadzącego, czyli osobę, która moderuje kolejność wypowiedzi.   
3.3.3 Na spotkanie może przyjść każdy członek NK, który ma na to ochotę, niezależnie od tego, czy jest Producentem czy nie.   
3.3.4 Prowadzący spotkanie może wyrzucić ze spotkania uczestnika, który zakłóca jego przebieg i/lub działa w złej intencji, wbrew dobremu interesowi grupy.     
3.3.5 Spotkania Producentów może zwołać każdy członek NK z minimalnie trzydniowym wyprzedzeniem. Zaleca się, by przed spotkaniem na kanale nk-projekty-czat został założony wątek do zamieszczania plików do weryfikacji. Te pliki powinny zostać sprawdzone w pierwszej kolejności podczas spotkania.     
3.3.6 Na Spotkaniu Producentów uczestnicy mogą zgłaszać uwagi do prezentowanego projektu lub jego składowej. Te uwagi powinny być rozpatrzone, ale ich poprawienie leży w gestii prowadzącego projektu. Wyjątkiem są krytyczne uwagi od opiekunów roli dotyczące składowej, za którą są odpowiedzialni, te muszą zostać uwzględnione: patrz punkt niżej.   
3.3.7 W dowolnym momencie trwania projektu, w tym również podczas Spotkania Producentów, Opiekunowie Ról mogą zawetować składową projektu, za którą są odpowiedzialni (Opiekun Tekściarzy - tekst, Opiekun Realizatorów - miks etc). Opiekunowie Ról biorą wtedy na siebie przygotowanie odpowiedniego feedbacku dla producenta prowadzącego projekt oraz twórcy produktu.          
3.3.8 Jeśli podczas Spotkania Producentów projekt otrzyma dwie okejki, czyli jest zaakceptowany do publikacji, prowadzący spotkanie pisze o tym w wątku tego projektu z oznaczeniem @Biura Projektowego.

### 3.4 Przejmowanie porzuconych projektów (2016/11/23)
Projekty, których prowadzący stał się nieaktywny, mogą być za zgodą lidera Grupy przejęte przez innego prowadzącego.

### 3.5 Prowadzenie projektu, który jest wersją alternatywną już istniejącego (2024/02/26)
Jeśli Producent chce poprowadzć projekt scenki lub piosenki, która już została wydana na kanale YT NK lub jest w produkcji (ma założony wątek w dziale projektowym), musi publicznie, na kanale nk-projekty-czat, poprosić o zgodę całą ekipę pierwszej wersji projektu (na ile oznaczenie wszystkich członków ekipy jest możliwe) i tę zgodę uzyskać.

### 3.6 Status projektu strategicznego, CODE RED (2024/10/05)
Jest to specjalny status nadawany projektom strategicznym, ważnym dla całej społeczności NK. Taki projekt powinien możliwie szybko zostać zrealizowany i wydany.   

- Projekt jest prowadzony przez producenta na serwerze głównym NanoKarrin, w dziale CODE RED
- Projekt ma na start określany deadline: 2 tygodnie na każde 5 minut trwania
- Na każdą rolę projektową powinny być obsadzone dwie osoby, osoba główna i jej dubler; jeśli wystąpią opóźnienia w dostarczeniu produktu, osobę główną zastępuje się dublerem
- Projekt jest wydawany poza Kolejką projektów (przed innymi projektami i jak tylko materiały będą gotowe, nie tylko w sobotę)
- Członkowie projektu dostają specjalną rolę CODE RED i są przypięci na górze listy
- Jak w każdym projekcie nad producentem stoi Lider Grupy; jeśli producent nie może lub nie chce podjąć decyzji w krytycznym momencie projektu, podejmuje ją Lider Grupy
- Teksty / dialogi może pisać wielu tłumaczy naraz. Fragmenty tłumaczeń mogą być łączone, by osiągnąć jak najlepszy efekt. Proces ten powinni przeprowadzić tłumacze, ale w razie niezgody może on być eskalowany do producenta, a w dalszym kroku do Lidera Grupy.
- Projekt otrzyma większe wsparcie medialne
- Projekt w celu wydania musi przejść zwykły proces otrzymania dwóch okejek podczas Spotkania Producentów; podczas trwania CODE RED terminy spotkań producentów powinny być zaplanowane z góry tak, by zwiększyć ekspozycję projektu
- W razie problemów NanoKarrin udziela swoich kontaktów i wsparcia, by przyspieszyć prace
- O przyznaniu CODE RED decyduje Król, a w drugiej kolejności Lider Grupy; zawnioskować o ten status dla dowolnego projektu (zaczętego po 5 października 2024) może każdy na kanale nk-projekty, oznaczając i Nanohę, i Pchełkę

### 3.7 Współpraca z zewnętrznymi podmiotami (2019/06/10)
W razie współpracy nad projektami NanoKarrin z zewnętrznymi podmiotami logo lub intro Grupy musi się pojawić w projekcie jako pierwsze – przed logiem/intrem/nazwą zewnętrznego podmiotu. Wyjątkiem są projekty publikowane z ramienia Bitew Dubbingowych NanoKarrin.

---
id: definicje
title: Definicje
sidebar_label: Definicje
---
**Rola** – pełniona na skalę projektu; określa zadania wykonywane w trakcie jego realizacji 

**Funkcja** – pełniona na skalę grupy; funkcja niesie za sobą obowiązki i uprawnienia

**Zespół Koordynacyjny** – wszystkie osoby pełniące w grupie funkcje, które mają dostęp do serwera koordynacyjnego Discord.

**Ranga** – nadawane przez rekrutera zaświadczenie, że osoba jest w stanie dobrze wykonywać daną rolę. Posiadanie rangi jest jednoznaczne z członkostwem w grupie NanoKarrin.

**Grupa zawodowa** – wszyscy członkowie posiadający daną rangę w grupie NanoKarrin.

**Grupa / NK** – grupa fandubbingowa NanoKarrin

**Portal NanoKarrin** – strona www.nanokarrin.pl

**Produkt dubbingowy** – dowolny produkt tworzony na potrzeby projektu dubbingowego: nagranie wokalne, nagranie aktorskie, tekst piosenki, ścieżka dźwiękowa, ilustracja, miks itp.

**Projekt NanoKarrin / projekt NK** – wewnętrzny projekt Grupy realizowany w celu udostępnienia go przez media społecznościowe NanoKarrin, posiadający temat na forum

**Projekt zewnętrzny** – projekt realizowany przy udziale i pod patronatem Grupy, ale przeznaczony do publikacji w mediach nieadministrowanych przez NK lub w mediach społecznościowych Grupy na zasadach ustalonych przez Koordynację indywidualnie dla projektu

**Projekt** – projekt NK lub projekt zewnętrzny

**Wydanie projektu** – oficjalna publikacja projektu w mediach społecznościowych grupy



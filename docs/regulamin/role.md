---
id: role
title: Role w grupie
sidebar_label: Role w grupie
---

## Król (Nanoha)
Nadzoruje pracę Koordynatorów. Jest głównym administratorem witryny i mediów społecznościowych grupy i posiada władzę absolutną.

## Koordynacja NK

#### Koordynator rekrutacji (Pchełka)
Jest przełożonym opiekunów rekrutacji i rekruterów i jako taki posiada uprawnienia każdego z nich. Odpowiada za regulamin rekrutacji. Ma ostateczne słowo w kwestii przyjmowania do grupy.

#### Koordynator mediów (Nanoha)
Opiekuje się mediami społecznościowymi grupy takimi jak kanał YouTube, Dailymotion, fanpage Facebook oraz dyskami i witryną grupy.   
Jest przełożonym moderatorów mediów społecznościowych.

#### Koordynator moderacji (Seric)
Odpowiada za porządek i przestrzeganie regulaminu na Discordzie NK oraz forum NK.   
Jest przełożonym Zespołu Moderacyjnego.

#### Koordynator wsparcia IT (Seraskus)
Posiada ostateczne słowo w kwestiach wyglądu, kształtu oraz uprawnień na Forum, serwerze Discord oraz portalu grupy. Określa rangi oraz ich uprawnienia. Jest odpowiedzialny za przepływ informacji wewnątrz grupy oraz utrzymanie jej systemów informatycznych.   
Jest przełożonym Zespołu Wsparcia IT.

#### Koordynator kawalerii konwentowej (Orzecho)
Odpowiada za obecność NK na eventach zewnętrznych, np. konwentach, festiwalach. Jest przełożonym kawalerzystów, czyli osób zajmujących się działalnością NK na eventach.

## Role zespołów koordynacyjnych

#### Moderatorzy
Czuwają nad porządkiem na Forum i serwerze Discord. Mają uprawnienia do przenoszenia/edycji postów, nadawania rang i uprawnień oraz nakładania kar na toksycznych użytkowników. Moderatorzy odpowiadają przed Koordynatorem Moderacji i są przez niego osobiście wybierani i odwoływani.

#### Rekruterzy
Podejmują decyzję o nadawaniu rang w swojej dziedzinie (przykład: rekruter tekściarzy może nadać rangę tekściarza). Jako ekspert w swojej dziedzinie, każdy rekruter może dać Producentowi veto na wydanie projektu i nakazać wykonanie zmian w obrębie swojej dziedziny (przykład: rekruter tekściarzy może nakazać wykonanie poprawek tekstu).    
Rekruterzy odpowiadają przed Koordynatorem Rekrutacji i są przez niego osobiście wybierani i odwoływani.   
Opiekun roli to główny rekruter, stojący w hierarchii wyżej niż inni rekruterzy. Każda z ról projektowych może mieć wielu rekruterów, ale tylko jednego opiekuna danej roli.   

#### Senpai
Jest to osoba odgórnie przydzielona każdej nowej osobie w grupie na czas ewaluacji. Pomaga wkręcić się w grupę, wspiera radą i pomaga wejść w towarzystwo. Jeden senpai może mieć maksymalnie trzech podopiecznych.   
Senpaie odpowiadają przed Koordynatorem Rekrutacji i są przez niego osobiście wybierani i odwoływani.   

#### Przewodnicy
Członkowie NK, którzy służą rekrutom za źródło wiedzy i drogowskaz, tłumaczą zasady działania grupy, kierują ku odpowiednim osobom. To oni prowadzą Cię na początku rekrutacji, przydzielają Ci senpaia i sprawdzają od czasu do czasu, jak odnajdujesz się w grupie.   
Przewodnicy odpowiadają przed Koordynatorem Rekrutacji i są przez niego osobiście wybierani i odwoływani.   

#### Wsparcie IT
Technicy posiadający uprawnienia administracyjne w systemach grupy. Jeśli masz problem z uprawnieniami lub działaniem serwera, pomogą Ci go rozwiązać.
Technicy odpowiadają przed Koordynatorem Wsparcia IT  i są przez niego osobiście wybierani i odwoływani.   

## Rangi i role w ekipie projektowej
Rangi są nadawane przez rekruterów i potwierdzają umiejętności ich posiadacza w pełnieniu danej roli w projekcie.   

#### Ranga typu “młodszy...” oraz "Reżyser" (2024/02/13)
Te dwie role nie obowiązują od roku 2024.
* Dawniej role dzieliliśmy na "młodsze" i "pełne", gdzie "młodszy" oznaczał osobę, która wykazuje predyspozycje do danej roli, ale musi jeszcze podszlifować umiejętności. Ten podział został ostatecznie zniesiony i ranga "młodszy" nie obowiązuje.   
* W poprzednim systemie wydawniczym można było prowadzić projekt jako "reżyser". Obecnie ta ranga istnieje na serwerze jedynie jako ranga legacy - pusta etykietka niedająca żadnych uprawnień.   

#### Role wspólne dla projektów piosenkowych i aktorskich:
Ilustrator - tworzy rysunki do filmu.   
Animator - tworzy animację i napisy do filmu.

#### Role w projekcie wokalnym:
Wokalista - śpiewa w piosence.
Realizator dźwięku - dokonuje masteringu i obróbki dźwięku.
Tekściarz - tłumaczy tekst piosenki.

#### Role w projekcie aktorskim:
Aktor - gra głosowo w scence.
Dźwiękowiec - znajduje i podkłada dźwięki i muzykę do scenki, montuje głosy aktorów.
Dialogista - pisze dialogi scenki.

## Pozostałe role

#### Junior
Członek NK, który w grupie jest około rok. Ranga Junior przyznawana jest w momencie dołączenia do grupy, a zdejmowana 2 czerwca roku następnego.   


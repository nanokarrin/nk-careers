---
id: dzwieksciezkapodstawy
title: Podstawy tworzenia ścieżki dźwiękowej
sidebar_label: Podstawy ścieżki
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/DMwb4rAG4BY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Tworzenie, rozbudowywanie i porządkowanie własnej bazy efektów dźwiękowych.
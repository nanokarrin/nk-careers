---
id: dialogigdrive
title: Udostępnianie scenariusza
sidebar_label: Udostępnianie
---

# Udostępnianie scenariusza w Google Drive

Udostępnianie scenariusza poprzez Google Drive to według mnie najwygodniejszy sposób. Pozwala dzielić się różnymi materiałami i w zależności od potrzeb odpowiednio ustawiać uprawnienia. Przy odpowiednio skonfigurowanych uprawnieniach nie jest wymagane logowanie do konta Google, aby zobaczyć dokument, ani nanieść w nim komentarze.

Udostępnianie na podstawie linku z włączoną możliwością komentowania jest o tyle przydatna, że nie wymaga podawania każdego z osobna, kto powinien mieć dostęp do scenariusza (wyobraź sobie scenkę z 10 aktorami, każdy przecież potrzebuje dostępu). Z kolei komentowanie przydaje się, jeśli aktor jednak nie jest w stanie wypowiedzieć którejś kwestii. Może wtedy zaznaczyć odpowiedni fragment i napisać o tym, ewentualnie od razu zaproponować swoją wersję kwestii do aprobaty. Mail z komentarzem trafia do właściciela dokumentu, także nie powinien przegapić takich uwag.

Od razu zaznaczę, że uprawnienie do komentarzy nie oznacza możliwości edycji dokumentu. Także nie musicie się obawiać, że jakiś losowy człowiek pozmienia czy wykasuje wam cały scenariusz. Zresztą, ufajmy sobie trochę, jak jesteśmy razem w grupie :)

Komentowanie scenariusza jest przydatne także podczas rekrutacji. Rekruter może bezpośrednio odnosić się do odpowiednich kwestii, ich fragmentów a nawet poszczególnych słów. To dużo wygodniejsze niż kopiowanie odpowiedniego fragmentu na forum i opisywanie, co z nim nie tak. Do tego rekrut i tak musi potem znaleźć ten fragment w swoim scenariuszu, żeby samemu zobaczyć kontekst wypowiedzi (chyba, że już wszystko zna na pamięć :) ).

Zaznaczamy dokument prawym przyciskiem myszy i wybieramy Udostępnij.../Share...

![Menu dokumentu](http://s2.ifotos.pl/img/krok1png_sshersp.png)

Następnie klikamy na Uzyskaj link do udostępniania/Get link for sharing.

![Opcje udostępniania](http://s5.ifotos.pl/img/krok2apng_ssherwa.png)

Potem z listy rozwijanej wybieramy Każda osoba z linkiem może komentować/Anyone with a link can comment

![Umożliwienie komentowania](http://s2.ifotos.pl/img/krok3apng_ssherwq.png)

I voilà! Osoba wchodząca z linka może dodawać komentarze.

![Efekt końcowy](http://s10.ifotos.pl/img/krok4png_sshersh.png)

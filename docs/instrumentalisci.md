---
id: instrumentalisci
title: Instrumentaliści
sidebar_label: Instrumentaliści
---
Opiekun listy: Yudeko#4356


|      Nick     |     Discord ID    |                                                                             Próbka                                                                            |
|:-------------:|:-----------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|     benk86    |    benk86#2422    |                                                          https://www.youtube.com/watch?v=qFQOgIVizB8                                                          |
|     eXway     |     eXway#8885    |                                                  https://soundcloud.com/exway-yt/sets/my-instrumentals-shorts                                                 |
| JoyBassScream | JoyBasScream#1641 |                                                   https://youtu.be/7MYUJs-v4BE https://youtu.be/Flr9Jk2EXQQ                                                   |
|    KatQrika   |      Kat#2281     |                                       https://drive.google.com/file/d/1hVhL60hqie7cY3aZ7apAS6RhkS25UMhS/view?usp=sharing                                      |
|     Krysta    |    Krysta#7355    |                                      https://drive.google.com/file/d/1W0QkcjHC-HlpxE6yX5ez_9HuBomcrFE2/view?usp=drivesdk                                      |
|      LiON     |     LiON#2340     |                                                          https://www.youtube.com/watch?v=3A3a2SzMBJg                                                          |
|     Niccup    |    Niccup#6918    |                                       https://drive.google.com/file/d/173b_k08M7q_0s83zJwf0zGYxkcXRxHIl/view?usp=sharing                                      |
|    Notlive    |    Notlive#3617   |                                                           https://soundcloud.com/notlive/the-vault-2                                                          |
|     Owarin    |    owarin#3899    | https://soundcloud.com/owarinn/idzie-zima-instrumental-org-w-opisie/s-wHTVQ9MSjGO https://soundcloud.com/owarinn/departures-instru-org-w-opisie/s-KKaXZt1FjOC |
|    Ravey16    | 1if30f45hit_#6072 |                                                        https://soundcloud.com/1if30f45hit/golden-clouds                                                       |
|    Reszort    |    Reszort#5715   |                            https://soundcloud.com/reszort38/upload-tylko-dla-nanokarrin-odcinki-serdecznie-zapraszamy/s-2XiJoavwcsD                           |

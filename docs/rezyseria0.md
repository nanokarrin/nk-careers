---
id: rezyseria0
title: Wstęp
sidebar_label: Wstęp
---

Szkółka reżyserii piosenek jest dla osób z wizją którzy łączą pracę artystów o różnych talentach w jednolity audio-wizualny produkt.

## Spis treści:
♬ [4 sposoby na to, aby twój realizator dźwięku był szczęśliwy](rezyseria1)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Zrozum różnicę pomiędzy produkcją a miksowaniem](https://rekrutacja.nanokarrin.pl/docs/rezyseria1#1-zrozum-różnicę-pomiędzy-produkcją-a-miksowaniem)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Ustal dokładnie co będziesz wysyłał](https://rekrutacja.nanokarrin.pl/docs/rezyseria1#2-ustal-dokładnie-co-będziesz-wysyłał)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Wyślij kawałek referencyjny](https://rekrutacja.nanokarrin.pl/docs/rezyseria1#3-wyślij-kawałek-referencyjny)  
&nbsp;&nbsp;&nbsp;&nbsp; ♬ [Wyślij wszystko w zorganizowany sposób](https://rekrutacja.nanokarrin.pl/docs/rezyseria1#4-wyślij-wszystko-w-zorganizowany-sposób)  
♬ [Check-lista przed zaakceptowaniem nagrań od wokalisty](rezyseria2)  
♬ [Check-lista przed zaakceptowaniem miksu do realizatora](rezyseria3)  

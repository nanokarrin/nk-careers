---
id: rekruanimator
title: Rekrutacja na animatora
---

### Osoby decyzyjne
Obecnie rekrutację sprawdza Kara (Discord: @karakopiara) i to ona podejmuje decyzję o nadaniu rangi.

### Przebieg rekrutacji
Warunkiem otrzymania rangi animatora jest stworzenie wideo, które może posłużyć jako obraz-animacja do coveru fandubbingowego. Wideo powinno pokazywać Twoje umiejętności w programach do obróbki wideo i efektów wizualnych.   

Uwagi dla animatorów   

- Należy założyć post rekrutacyjny
- W poście należy zamieścić linki do przynajmniej dwóch filmów (swojego autorstwa; filmy nie muszą być stworzone specjalnie na potrzeby rekrutacji)
- Każdy filmik musi trwać min. 1,5 minuty, a maks. 5 minut
- Piosenki, do których tworzone są filmy, muszą być w języku polskim
- Na filmach powinny znaleźć się napisy (tekst piosenki, ekipa projektu) w języku **polskim**
- Ocenie poddane zostaną ogólna estetyka filmu, timing oraz poprawność napisów (dbałość o polskie znaki i interpunkcję)   
Post rekrutacyjny załóż na naszym serwerze Discord: kategoria "Rekrutacja NK". Tam możesz też podejrzeć, jak robili to inni :)   

Wynik rekrutacji pojawi się w formie wiadomości w założonym przez Ciebie wątku. Jeśli nie odpiszemy po dwóch tygodniach, przypomnij się! Też jesteśmy ludźmi, mamy mnóstwo spraw na głowie, możemy po prostu zapomnieć.   

### Jaki może być wynik rekrutacji?
### Pozytywny
Otrzymujesz rangę Animator. Jest to ranga przydzielana rekrutom, którzy wykazali odpowiednie umiejętności w edytowaniu wideo i tworzeniu animacji.

##### Negatywny
Nie udało Ci się pomyślnie przejść rekrutacji, możesz podejść do niej ponownie po miesiącu. Wykorzystaj ten czas na doskonalenie swoich umiejętności, ponieważ Twoje postępy będą brane pod uwagę. Prosimy również, aby w takim przypadku (wraz z założeniem nowego wątku rekrutacyjnego) wysłać nam link do swojej poprzedniej rekrutacji, ponieważ to ułatwi nam pracę i wystawienie ponownej oceny.
Do rekrutacji można podejść tyle razy ile potrzeba, ponieważ każdy ma szansę na rozwój oraz na dołączenie do grupy. Wróć do nas w momencie, kiedy naprawdę będziemy mogli ocenić Twoje postępy.

### Dwa słowa od opiekuna
Filmy powinny być tworzone wyłącznie przez Ciebie, co oznacza, że po wskazaniu błędów powinieneś poprawić je sam, a nie prosić o to kogoś innego. Jeżeli masz jakiś problem, nie wahaj się zadawać pytań na Discordzie, na kanale #animatorzy w kategorii "Fandubbing".

Filmik musi być pomysłem własnym pod względem montażu. Nie może być on skopiowany z żadnego źródła (np. z kanału NanoKarrin lub innego twórcy, który nie wyraża na to zgody) i w jakikolwiek sposób przerobiony. Dla wyjaśnienia: nie dotyczy to filmów takich jak np. amv/pv/openingi/endingi/teledyski itp oraz audio - czyli można robić filmiki z piosenkami NK). W razie jakichkolwiek wątpliwości pytaj w wiadomościach prywatnych.

Pamiętaj, że napisy muszą pojawiać się na filmiku chwilę przed tymi, które słychać w piosence - napisy w NK działają na zasadzie karaoke, a chwilkę to trwa, zanim obraz zobaczony przez człowieka zostaje przetworzony przez jego mózg ;)

Oceniane są Twoje umiejętności obsługi programu, styl i estetyka filmiku.

Dla lepszego efektu (jeśli chodzi o openingi oraz endingi z anime), polecamy szukanie ich w wersji 'creditless', by zbędne napisy nie przeszkadzały w estetyce oraz odbiorze filmu.

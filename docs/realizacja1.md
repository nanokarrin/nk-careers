---
id: realizacja1
title: Słowniczek realizatora
sidebar_label: Słowniczek realizatora
---

![](/assets/procesnagrywania.png)
<center><b>Rysunek 1</b>: Przegląd procesu nagrywania do komputera</center>

## Przegląd procesu nagrywania do komputera  
**(A)** Dźwięk jest zarejestrowany przez mikrofon  
**(B)** Sygnał analogowy przechodzi przez kabel XLR do audio interfejsu  
**(C)** W tym wypadku, przedwzmacniacz i ‘Analog to Digital A/D’ konwerter są razem w środku audio interfejsu  
**(D)** Sygnał cyfrowy jest wysłany z audio interfejsu do komputera<sup>1:exclamation:</sup>  
**(E)** Sygnał jest przeniesiony do softwaru na komputerze  
**(F)** Software (DAW) manipuluje sygnał  
**(G)** Suma wszystkich sygnałów jest kierowana poza DAW z powrotem przez komputer<sup>1:exclamation:</sup>  
**(H)** Sygnał kieruje się z powrotem do zewnętrznej karty dźwiękowej  
**(I)** Sygnał cyfrowy jest konwertowany na sygnał analogowy (Digital to Analog D/A) i wysłany do głośników/słuchawek  

_1:exclamation:To miejsca gdzie większość początkujących ma problemy. Musisz wybrać Inputs (wejścia) i Outputs (wyjścia) w odpowiednich miejscach na komputerze żeby móc nagrywać i słyszeć dźwięk z głośników/słuchawek._

## Audio Interface  
Audio Interface (pol. audio interfejs) to urządzenie, które pozwala na przesłanie sygnału z nagrywanym dźwiękiem do karty dźwiękowej twojego komputera. Zazwyczaj nie nazywamy audio interfejsami kart dźwiękowych, które były dołączone do naszych komputerów po ich zakupie.

Audio Interfejs musi mieć niską latencję. Sygnał z twojego mikrofonu musi być w stanie bardzo szybko przedostać się do przedwzmacniacza w audio interfejsie a potem do komputer i twojego DAW. Potem znowu tą samą drogą sygnał musi też dostać się do twoich słuchawek. Większość wbudowanych kart dźwiękowych w laptopach i komputerach stacjonarnych nie radzi sobie z tym procesem bo są za wolne. Jeżeli zamierzasz coś nagrywać i chcesz słyszeć jak to brzmi w czasie rzeczywistym to wysoko-latencyjna karta dźwiękowa sobie z tym nie poradzi.

Zewnętrzna karta dźwiękowa jest polecana do nagrywania w domu aby pozbyć się problemów z latencją. Niższej jakości alternatywą (zamiast zewnętrznej karty dźwiękowej) są mikrofony USB.

## AUX Send  
Auxiliary Send (pol. wyjście pomocnicze) to część drogi sygnału (routing) na konsolecie albo wirtualnym mikserze w DAWie. Aux Send wysyła sygnał w dodatkowe miejsce żeby nałożyć na niego efekty bez zmieniania oryginalnej drogi sygnału. Najpopularniej Auxy używane są do dodawania pogłosu albo delay przy obróbce nagrań wokalnych.

Każdy sygnał w mikserze może być wysłany do kanału Aux. Częstym sposobem miksowania jest stworzenie 2 kanałów Aux z 2 różnymi typami pogłosu i wysyłanie tych pogłosów do poszczególnych ścieżek dźwiękowych w projekcie. W ten sposób każda ścieżka może mieć ustawiony poziom pogłosu z tych dwóch działających efektów. Łatwiej jest mieć tylko 2 pogłosy na np. 15 ścieżek niż 15 różnych pogłosów na każdą ścieżkę.

## Bleed
Bleed (pol. krwawienie) to te dodatkowe dźwięki, które dostają się do mikrofonu. Na przykład mówimy o krwawieniu kiedy wokalista/ka nagrywa siebie a dźwięk instrumentalu ze słuchawek przenika do nagrania. Bleed może być dobry i zły zależnie od sytuacji. W podanym przykładzie bleed ze słuchawek jest szkodliwy bo może wprowadzić słyszalne błędy podczas miksowania (myśl - głośniejszy wokal = głośniejszy bleed ze słuchawek).

## Boost
Boost (pol. podbicie) to termin używany przy korekcie częstotliwości (equalizacji). Kiedy mówimy ‘podbij 2k o 3dB’ to oznacza, żeby ‘podgłośnić 2,000 Hz o 3 decybele’ na parametrycznym korektorze częstotliwości.

## Bounce
Bounce (pol. eksport) albo ‘bouncing’ to nic innego jak eksportowanie sumy wyznaczonych ścieżek w programie jak na przykład Audacity. Bouncing może być też użyteczny do łączenia paru nagrań razem jeżeli maksymalna ilość ścieżek w DAWie jest problem.

## Bus
Bus albo grupa to terminy używane kiedy mówimy o routingu sygnałów. Bus to jakiekolwiek miejsce na mikserze albo programie do nagrywania gdzie sygnały się łączą. Bardzo podobna sytuacja jak z normalnym autobusem. Sygnały (pasażerowie) przemieszczają się razem gdzieś przez jakiś czas a potem wydostają się gdzie indziej. Na przykład możemy wysłać 5 nagrań różnych wokalistek do jednej grupy. Korzyści z takiego rozwiązania są ogromne. Możemy nałożyć korektę częstotliwości (EQ) na wszystkie 5 nagrań. Mamy wtedy jeden suwak do kontroli głośności wszystkich 5 nagrań.
Często używa się paru grup przy miksowaniu. W przypadku grupówek często rozdzielam różne partie głosów na poszczególne grupy. W ten sposób ułatwiam sobie pracę, ale wciąż mogę nakładać efekty na indywidualne ścieżki.

## Chórki
Chórki — nazywane wokalem wspierającym — to partie śpiewu harmonicznego wykonane przez jednego lub więcej wokalistów/ek wraz z głównym wykonawcą.

## Clipping
Kiedy wzmacniacz jest ‘popchnięty’ do momentu ‘clippingu’ to sygnał audio jest przesterowany. Jeżeli próbujemy ‘przepchnąć’ zbyt dużo sygnału (np. sinusoidę) przez wzmacniacz to czubki tej fali zostaną obcięte. W ten sposób czubki staną się płaskimi daszkami zamieniając perfekcyjny sinusoidalny sygnał w przesterowaną falę prostokątną.

Przesterowanie wzmacniacza gitarowego jest często jak miód dla ucha lecz przester w domenie cyfrowej prawie zawsze tworzy wyczuwalny, nieprzyjemny efekt.

## Cut
Cut (pol. tłumienie/obniżanie) to termin używany przy equalizacji. Kiedy mówimy ‘przytnij 2k o 3dB’ to oznacza, ‘zredukuj 2,000 Hz o 3 decybele’ na parametrycznym korektorze częstotliwości.

## DAW
DAW oznacza Digital Audio Workstation. To ekstrawaganckie określenie ‘komputera używanego do nagrywania’. Popularne programy komputerowe DAW to: Audacity, Premiere Pro, n-Track, FL Studio, Reaper, Pro Tools, Ableton, Cubase, Logic.

## Dry
Dry (pol. suche) czyli suche nagrania bez dodatkowych efektów takich jak pogłos czy delay. Jeżeli wokal brzmi ‘sucho’ to znaczy, że brzmi jakby wokalista śpiewał/a prosto do twojego ucha bez jakiejkolwiek przestrzeni dookoła.

## Eksport Audio
(Patrz [Bounce](#bounce))

## EQ
(Patrz [Korektor Parametryczny](#korektor-parametryczny))

## Fade in/out
Jeden z podstawowych aspektów edycji audio to manipulacja głośności. Fade in i out nawiązują do stopniowej zmiany głośności dźwięku.

## Grupówka
Grupówka to projekt grupowy składający się z wielu wokalistów.

## Headroom
Headroom (pol. zapas) może być powiązany z każdym etapem nagrywania gdzie sygnał może być przeciążony. Każdy układ elektroniczny ma maksymalną wysokość sygnału wejściowego (napięcie elektryczne) z jaką może sobie poradzić. Jeżeli headroom się zwiększa to maksymalna wysokość sygnału wejściowego też się zwiększa.
W przypadku DAW headroom oznacza ilość wolnego miejsca dla sygnału przed przesterowaniem.

## Korektor Parametryczny
Korektory parametryczny (equalizer) złożony jest z zespołów filtrów, które służą do podbijania (boost) lub tłumienia (cut) określonego zakresu częstotliwości. Wiele osób widziała korektory graficzne na których można narysować uśmiechnięte buźki dostępnymi suwakami. Korektory graficzne operują na niezmiennych częstotliwościach. Korektory parametryczne różnią się w ten sposób, że można na nich znaleźć i zmienić dowolną częstotliwość. Dodatkowo, korektory parametryczne zawsze pozwalają na zmienienie szerokości zakresu częstotliwości, który chcemy zmienić. Ten koncept jest znany jako Q-factor (pol. Dobroć Q - jeżeli ktoś jest zainteresowany drążeniem tematu).

## Mastering
Mastering to proces zmieniania kolekcji piosenek w album (albo w singiel, playlistę, podcast...) i łączenia ich w finałowy master przed publikacją. Mastering ma na celu sprawienie, że miks jakiejś piosenki brzmi najlepiej jak tylko może przed publikacją.

## Mikrofon
Mikrofon to przyrząd który konwertuje fale dźwiękowe na prąd elektryczny. Kiedy instrument muzyczny (np. struny głosowe) wydaje jakiś dźwięk to stworzona fala dźwiękowa ‘leci’ przez całe pomieszczenie. Jeżeli przyłożysz swoją dłoń do ust i coś zaśpiewasz to rzeczywiście poczujesz wibracje. Te wibracje są kombinacją częstotliwości. Mikrofon może konwertować drgania fal dźwiękowych w prąd przemienny, którego rezultaty możecie zobaczyć na ekranie waszego programu do nagrywania dźwięku.

## Mixing
Miksowanie to łączenie różnych nagrań dźwiękowych w jedną ścieżkę stereo. Łączenie tych nagrań to rzeczywiście sztuka, która wymaga dużej ilości ćwiczenia (to samo tyczy się masteringu). Miksowanie wymaga podejmowania ciężkich decyzji, które bezpośrednio oddziałują na wizję końcową produkcji.

## MP3
Mp3 to skompresowany cyfrowy format audio. Mp3 jest bardzo popularne w internecie bo brzmi okey i zajmuję bardzo mało miejsca, więc można je szybko ściągać. NIE CHCESZ pracować z plikami tego formatu podczas miksowania. Zawsze lepiej założyć, że piosenka nad którą pracujesz jest najlepszej możliwej jakość i dopiero po publikacji jest degradowana do gorszej jakości. Jeżeli potrzebujesz mp3 do uploadowania na Youtubie polecam zawsze konwertować plik z formatu .wav do mp3 320kbps.

## Panorama
Panorama (ang. pan/panning) odnosi się do pozycji danego instrumentu w spektrum stereo np. lewo i prawo. Kiedy ktoś mówi, że przesunął dźwięk wokalu na prawą stronę w panoramie to znaczy, że usłyszymy ten wokal z prawego głośnika lub prawego ucha naszych słuchawek. Zwykle używana skala panoramy w programach DAW to L100 (lewo) przez 0 (centrum) aż do R100 (prawo).

## Pasmo Częstotliwościowe
Pasmo częstotliwościowe (ang. bandwidth, pol. dobroć Q) to termin określający zakres częstotliwości, który jest używany w korektorach parametrycznych. Powiedzmy, że podbijasz 1kHz o 6dB w korektorze parametrycznym. Jeżeli użyjesz wąskiego pasma częstotliwości to afektowane pasmo częstotliwości będzie wąskie. Jeżeli użyjesz szerokiego Q to podbijesz o wiele więcej częstotliwości dookoła 1kHz.

## Plugin
Audio plugin (pol. wtyczka audio) to software, który pozwala na dodanie funkcjonalności do programu obróbki dźwięku. Istnieją różnorodne wtyczki audio a główne rodzaje to: EQ, kompresor, limiter, reverb, delay, chorus, phaser, distortion.

## Pop Filter
Pop filtr to rajstopy na wieszaku. Służą do eliminacji spółgłosek wybuchowych takich jak p, t, k podczas nagrywania wokalu.

## Preset  
Preset (pol. ustawienie) w kontekście audio odnosi się do czyjegoś szablonu ustawionych parametrów w pluginie. Używaj presetów, ale pamiętaj, że wiedza o poszczególnych parametrach da Ci więcej swobody podczas miksowania.

## Produkcja
Produkcja (ang. production) odnosi się do procesu tworzenia jakiejś piosenki. Słowo produkcja zazwyczaj używamy jako określenie procesu nagrywania instrumentów (np. głosu). A post-produkcja to wszystkie procesy odnoszące się do miksowania tych nagrań i ich masteringu.

## Projekt Grupowy
(Patrz [Grupówka](#grupowka))

## Proximity Effect
Efekt bliskości opisuje sposób w jaki niskie częstotliwości zwiększają się w momencie kiedy zbliżasz się do mikrofonu. Ten efekt przejawia się w mikrofonach o charakterystyce kardioidalnej i ósemkowej<sup>2:exclamation:</sup>.

Ważne jest pamiętanie o tym efekcie podczas nagrywania wokalu. Umiejętność jego używania ma duży wpływ na brzmienie twoich nagrań. Sprawdź sam jak zmienia się brzmienie twojego głosu kiedy śpiewasz 20cm od mikrofonu w porównaniu do 5cm.

_2:exclamation: Jeżeli jesteś zainteresowany/a czym są charakterystyki kierunkowości mikrofonów to możesz zacząć swoje badania w [szkółce szprzętowej](hard1)_

## Przester
(Patrz [Clipping](#clipping))

## Reverb
Reverb (pol. pogłos, rewerberacja) to otaczające brzmienie, które rozpływa się po pomieszczeniu. Zacznijmy od pojedynczego echa (delay). Odbicie dźwięku od ściany jest bardzo oczywiste. HEJ! . . . hej. TY! . . . ty. Teraz dodaj tysiąc gazylionów takich poszczególnych ech, które obijają się po każdej powierzchni akustycznej znajdującej się pokoju. W momencie kiedy odbicia (delays) stają się zupełnie nie rozróżnialne wchodzimy w świat rewerberacji. Reverb może kojarzyć nam się ze śpiewaniem pod prysznicem albo krzyczeniem w sali gimnastycznej. Reverb może być naturalny lub symulowany elektronicznie i komputerowo (pluginy).


## Reżyser Piosenek
Reżyser piosenek pełni rolę producenta (ang. producer) projektów fandubbingowych. Do jego zadań między innymi należy praca z wokalistami podczas produkcji, praca z reżyserem dźwięku (dźwiękowcem) nad wizją piosenki i ostateczna decyzja wydania produktu końcowego.

## RMS Loudness
Słowa _RMS_ i _peak_ są używane całkiem często w prawie wszystkim co ma do czynienia z elektroniką. W świecie audio te słowa pokazują się kiedy mówimy o głośności sygnału audio.

_Peak_ to wartość najgłośniejszej części sygnału. Jeżeli popatrzysz na swój miks to szybko zobaczysz, że czubki sygnału składają się z głośniejszych części waszych nagrań. W muzyce peaks reprezentowane są często akcentami werbla lub stopy, które przebiją się ponad resztę dźwięków.

_RMS loudness_ to średnia głośność sygnału liczona w czasie. Pewnie słyszałeś/aś o terminie ‘Loundess War’ (Wojny Głośności), której problem leży w wysokiej wartości RMS nowoczesnych produkcji w porównaniu do piosenek wydanych paręnaście lat temu. Wiele dzisiejszych piosenek jest niszczone przez maksymalizowanie głośności zatracając w ten sposób jakąkolwiek dynamikę brzmienia.

## Sibilance
Sibilance (pol. spółgłoska szczelinowa) to głośne i nieprzyjemnie brzmienie ‘sss’. Polskie sybilanty to: s, z, c, dz, sz, ż/rz, cz, dż, ć, dź lub ang. s, z, sh, zh, ch, j. Sybilanty zazwyczaj są niemile widziane w nagraniach wokalnych. Do ich redukcji używamy korektorów częstotliwości lub de-eser’ów (nie mówię tutaj o deserach tylko o takim rodzaju kompresorów, który działa na wyznaczone pasmo częstotliwości).

## Wav
.wav to nieskompresowany format audio używany na płytach CD.

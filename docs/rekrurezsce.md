---
id: rekrurezsce
title: Rekrutacja na reżysera scenek
---
:::warning
Rekrutacja na reżysera dostępna jest jedynie dla członków NK, którzy są w grupie od minimum miesiąca.
:::
:::tip
Ostatnia zmiana: 27 kwietnia 2020 r.  
:::

### Osoba decyzyjna
Obecnie rekrutację prowadzi Nanoha (Discord: Nanoha#9476), to z nią należy się kontaktować, jeśli chcesz podjąć się tej rekrutacji.

### Przebieg rekrutacji
1. Znajdź scenkę, którą chciałbyś usłyszeć w polskiej wersji. Sprawdź, czy twórcy udostępnili soundtrack, ewentualnie czy jesteś na tyle pomysłowy, aby go czymś zastąpić i kto mógłby stworzyć ścieżkę dźwiękową.
2. Zastanów się, jaki głos w jakiej roli byś widział.
3. Złap Nanohę na Discordzie i poinformuj ją, że chcesz rozpocząć rekrutację. Nano wypyta Cię o kilka organizacyjnych aspektów i jeśli wszystko będzie ok, możesz kontynuować.
4. Załóż temat w dziale "Dubbingowo -> Scenki" z adnotacją [No.1]. Mimo że to Twój pierwszy projekt, jest to oficjalne przedsięwzięcie, które po odpowiednich instrukcjach i podpowiedziach Nanohy wleci na kanał! :)
5. Od tego momentu tworzysz projekt. Co tydzień zdajesz Nano raport z postępów. Nawet jeśli nic się nie dzieje, powiedz jej o tym.
6. Nanoha ocenia, czy projet jest gotowy do publikacji i to ona przydziela rangę w dniu premiery projektu.

> Do nagrań projektowych najlepiej stwórz sobie jakiś folder na Gdrivie lub serwer Discord, tak, abyś wszystko miał w jednym miejscu, bez posiadania 1000 różnych linków, które giną w akcji. Poinstruuj swoich aktorów, jak mają podpisywać nagrania, aby było to czytelne później zarówno dla Ciebie, jak i dźwiękowca.

### Jaki może być wynik rekrutacji?
#### Pozytywny
Otrzymasz rangę Reżysera Scenek NK, a Twoj pierwszy projekt jest opublikowany jako debiut reżyserski. Możesz tworzyć projekty w ramach grupy i zgodnie z Regulaminem NK.

#### Negatywny
Nie otrzymasz rangi. Z Nano ustalasz, w jakim zakresie musisz się jeszcze podszkolić.

---
id: aktorspistresci
title: Spis treści
sidebar_label: Spis treści
---

> Opracowała: Alisun

## Lekcje i ćwiczenia:
* Lekcja 1 – [Rozgrzewka](aktorlekcja1)
* Lekcja 2 – [Dykcja](aktorlekcja2)
* Lekcja 3 – [Emisja głosu](aktorlekcja3)

## Porady:
* Rada 1 – [Mikrofon](aktorrada1)
* Rada 2 – [Popfiltr](aktorrada2)
* Rada 3 – [Wyciszanie pomieszczenia](aktorrada3)
* Rada 4 – [Nagrywanie](aktorrada4)
* Rada 5 – [Udostępnianie nagrań](aktorrada5)

## Często zadawane pytania i odpowiedzi:
* [FAQ](aktorfaq)

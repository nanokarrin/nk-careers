---
id: tekst6
title: W poszukiwaniu rymu
sidebar_label: W poszukiwaniu rymu
---

W poszukiwaniach rymu do konkretnego jednosylabowego słowa może nam pomóc kilka sposobów:  
a) wymienianie po kolei słów brzmiących podobnie do naszego słowa X poczynając od litery "a", a kończąc na "z" (zwykle coś wpada do głowy)  
b) przeglądanie stron typu rymer.org, które - choć bardzo przydatne - zawierają też masę śmiecia, który mimo wszystko trzeba przejrzeć i odfiltrować  
c) męczenie znajomych ("Hej, daj mi rym do >>kot<<!")

Ja wam natomiast proponuję sposób nieco prostszy, choć wymagający poświęcenia odrobiny czasu: stworzenie zbioru rymów, bazując na tekstach istniejących piosenek.  
Siedzicie w domu i słuchacie muzyki z YT. Jedziecie samochodem i słuchacie radia. Ktoś w waszej okolicy puszcza muzykę z
telefonu. Wytężcie uszy, szukajcie ciekawych rymowych rozwiązań i je zapisujcie! A nuż takie albo inne wyrażenie wykorzystacie później w piosence! :)

Przykładem takiego zbioru jest [Zjednoczenie Smutnych Tekściarzy] (https://docs.google.com/document/d/1Qe0wV7023O20mkNPRNPZ_EcQgnpygweOLruLgskkhQQ/edit?usp=sharing).

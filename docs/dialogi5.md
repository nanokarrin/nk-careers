---
id: dialogi5
title: O wysławianiu się
sidebar_label: Język
---

## Parafraza

Coś, z czym będziesz często mieć styczność podczas całego procesu tworzenia scenariusza. Krótko mówiąc, tłumaczenie polegające na zachowaniu jedynie sensu oryginału, które może znacząco różnić się od tłumaczenia dosłownego, przekazanie tego samego, ale innymi słowami.

Kłapy i inne czynniki same w sobie wprowadzają spore ograniczenia, które w zasadzie niwelują możliwość tłumaczenia dosłownego. Trzeba zatem znajdować sposoby, by dane sformułowania oddawać innymi słowami.

Jak to robić? Wątpię, by istniały jakieś automatyczne słowniki czy metody. Trzeba po prostu mieć spory zapas różnych zwrotów, wyrażeń, idiomów czy nawet przysłów, trochę pomyślunku i intuicji językowej.

>I am Professor! (oryginał jest nieco podniośle)

>Jestem profesorem! (zwyczajnie, sylaba za dużo)

>Jam jest profesor! (archaicznie)

Jak ją nabyć? Czytając. Dużo czytając. Mam nadzieję, że skoro zdecydowałeś/łaś się podążyć ścieżką dialogisty, to nie muszę Cię do tego zachęcać :) Najlepiej czytać różne rzeczy, by zdobyć jak najszerszy zakres słownictwa, wyrażeń i stylów. Choć w praktyce w grupie dominują raczej scenki zorientowane na fantastykę czy przygodę, także można się skupić na takich. Albo po prostu na tym, co Cię interesuje.

## Wyrażenia

A tutaj nie tyle reguła, co raczej wskazówka. Nasz rodzimy język obfituje w rozmaite idiomy, powiedzenia, czy przysłowia. Czasem, jeśli coś pasuje, zamiast tłumaczyć zwyczajnie, warto skorzystać z utartego wyrażenia. Od razu wprowadzi to taki swojski akcent :)

>I knew that this would happen. Don't say I didn't warn you.

>To się musiało tak skończyć. Przecież cię ostrzegałem.

>Nosił wilk razy kilka, ponieśli i wilka.

I mała rada odnośnie angielskiego. Być może to oczywiste, a być może nie, więc wolę wspomnieć. Ten język również dysponuje sporą liczbą tzw. phrasali. Jeśli coś wydaje się nie mieć sensu, sprawdź, czy przypadkiem nie stanowi wyrażenia. Czasem dodanie małego in, out, off czy innych potrafi zupełnie zmienić znaczenie czasownika.

>Carry out the mission!

>Wynieś misję na zewnątrz!

>Wykonaj misję!

## Zamiana kolejności

Mała sztuczka, którą można czasem zastosować. Bywa tak, że w oryginale postać wypowiada dwa zdania. Po przetłumaczeniu okazuje się, że jedno jest trochę za długie, a drugie trochę za krótkie i ciężko jest zmienić ich długość. Być może, jeśli zamieni się je kolejnością, to będzie łatwiej je dopasować, albo nawet od razu będą pasować.

>Don't imagine too much! It's not like I like you or anything, baka!

>Tylko nie wyobrażaj sobie nie wiadomo czego! Wcale cię nie lubię!

>Wcale cię nie lubię! Nie wyobrażaj sobie nie wiadomo czego!

Przykład trochę na wyrost, ale ilustruje to, co chciałem pokazać. Ta sztuczka dosyć często jest wykorzystywana w angielskim dubbingu anime (o ile napisy mówią prawdę, nie znam japońskiego :P )
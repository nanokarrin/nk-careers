---
id: dzwiekmuzykapodstawy
title: Podstawy pracy z muzyką
sidebar_label: Podstawy pracy z muzyką
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/KUlhPKbtZrY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Podstawowe informacje o pracy z muzyką - wyszukiwanie, wstawianie, głośność, prosty montaż bazujący na oryginale.
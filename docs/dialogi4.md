---
id: dialogi4
title: Korzystanie z okazji
sidebar_label: Okazje
---

## Oszustwa

No dobrze, podałem już szereg zasad, obostrzeń i ograniczeń, których trzeba przestrzegać przy pisaniu dialogów. Czy oznacza to, że przez cały czas trzeba niewolniczo sie ich trzymać i nad każdą kwestią spędzać długie godziny, zanim znajdzie się tę jedyną? Otóż nie. Są sytuacje, w których można podejść do tematu nieco swobodniej i nie przejmować się częścią ograniczeń. Nie zawsze, ale jednak.

Przy okazji chciałbym zaznaczyć, że napisanie dobrego scenariusza jest procesem dosyć czasochłonnym. Na szczęście większość scenek realizowanych w grupie (z różnych względów) jest raczej krótka - kilkominutowa. Scenariusz do takich scenek da się zrealizować w skończonym czasie, nawet licząc się z nauką, studiami, pracą, itp. Trzeba się tylko trochę zorganizować. Katastrofy i wydarzenia losowe to inna para kaloszy i zostawmy je na boku - starajmy się być raczej optymistami :)

## Widoczność ust

Kwestia jest dosyć prosta - jeśli usta są widoczne - piszemy pod kłapy. Jeśli nie - wystarczy, że zmieścimy się w czasie. Nie musi to nawet być czas trwania oryginalnej kwestii. Jeśli nie widać ust, możemy ją zacząć, gdy tylko poprzednia postać skończy mówić, a skończyć, gdy zacznie mówić kolejna. Trzeba tylko pilnować, by nie przekroczyć granicy sceny, jeśli dana kwestia jest na jej granicy. W ten sposób możemy w kwestii zmieścić więcej, niż normalnie bylibyśmy w stanie.

Przypadki w których nie widać ust mogą być różne. Postać może być odwrócona tyłem do "kamery", może być złowrogo skadrowana, tak że widać tylko jej przepełnione szaleństwem oczy, może być ciemno albo postać jest na tyle oddalona, że mimo faktu, że twarz i usta są widoczne, to ciężko dostrzec, kiedy mówi. W każdym z takich przypadków możemy naginać kwestie do naszych potrzeb. Ciekawym przypadkiem są postacie, które mają usta, ale nimi nie ruszają, gdyż posługują się telepatią bądź czymś podobnym. Również wtedy nie trzeba się przejmować kłapami, ale warto zwrócić uwagę na mimikę. Warto, by np. fragment kwestii "... doprawdy?" był wypowiedziany w momencie, w którym postać marszczy brwi.

Może się też zdarzyć, że ujęcie zmieni się w połowie kwestii, np. początek pokazuje osobę mówiącą, a końcówka reakcję rozmówcy. W takim przypadku początek musi pasować w kłapy, reszta nie. Podobnie z każdym innym wariantem, łącznie z takim, że twarz mówiącego jest widoczna tylko przez krótki urywek kwestii. O ile w trakcie nie ma jakiejś przerwy, zwykle nie trzeba się takim fragmentem przejmować, bo i tak jakaś część kwestii tam się znajdzie.

## Narracja

Najwięcej swobody daje oczywiście fragment, w którym wykonywana jest narracja. Jedna z postaci może np. opowiadać swoje wspomnienia i zamiast jej twarzy widoczne są owe wspomnienia. Inna postać może rozpocząć opowieść o skarbie za siedmioma górami i siedmioma lasami, a obraz będzie przedstawiał starą księgę baśni, ilustrującą bajanie.

Zazwyczaj przy narracji przez dłuższy czas mówi jedna osoba, a pomiędzy zdaniami są krótsze lub dłuższe odstępy. Zapewnia to dużą dowolność, a jedyne, co należy dopasować, to ewentualną zgodność z tematem przewodnim obrazu. Przykładowo kwestia o zionącym ogniem smoku powinna nastąpić z grubsza wtedy, kiedy wyżej wymieniona księga jest otwarta na stronie przedstawiającą tę potężną prastarą istotę, a nie na stronie przedstawiającej podziemne królestwo krasnoludów. Zwykle narracje nie nastręczają zbyt wiele problemów.

## Zbliżenia

Kłapy dość zgrabnie opisują to, co w większości animacji dzieje się z ustami postaci. Kłapią. Kłap-kłap. Otwierają się i zamykają. Dlatego głównie ważna jest długość i sylaby (z grubsza odpowiadające pojedynczemu otwarciu i zamknięciu ust).

Im dalsze i mniejsze na ekranie są usta postaci, tym mniej dokładnie trzeba się trzymać kłapów. Widz po prostu będzie zwracał na nie mniejszą uwagę i przegapi ewentualne niezgodności.

Analogicznie, przy dużych zbliżeniach usta są bardziej widoczne i wymagają więcej uwagi. Szczególnie przy emocjonalnych scenach, gdzie twarz postaci może wypełniać niemal cały obraz. Może się też okazać, że zamiast prostego cyklu otwórz-zamknij, usta będą zanimowane bardziej realistycznie, różnie poruszając się dla różnych głosek.

W takim przypadku idealnie by było mniej więcej dopasować samogłoski, gdyż to one głównie odpowiadają za kształt otwartych ust (np. dzióbek dla "u", nieco rozszerzone wargi dla "e", itd.). To jednak wymaga dużo pracy i wprowadza spore ograniczenia. Uznałbym to raczej za wyzwanie dla zaawansowanych niż coś, co wykonuje się za każdym razem.
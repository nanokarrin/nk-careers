---
id: szkolka0
title: Słowo wstępu
sidebar_label: Słowo wstępu
---

W tym miejscu zebraliśmy część naszej wiedzy dotyczącej dubbingu. Całość podzielona jest na kategorie zależne od roli, którą chciałbyś pełnić w grupie, od tego też zależy charakter szkółki: ta dla dialogistów jest pełnoprawnym kursem warsztatu, natomiast szkółka wokalu to raczej zbiór rad i ćwiczeń.  
Warto zajrzeć wszędzie, a nuż znajdziesz coś interesującego. Jeśli coś Cię ciekawi, ale nie możesz tu tego znaleźć, napisz do nas na Discordzie!
Powodzenia!

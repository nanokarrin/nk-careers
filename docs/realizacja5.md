---
id: realizacja5
title: Materiały treningowe
sidebar_label: Materiały treningowe
---

Możecie znaleźć się w takiej sytuacji, że będziecie chcieli sobie na czymś poćwiczyć. Choćby i miały te ćwiczenia trafić do szuflady - zawsze to ćwiczenia. Dlatego poniżej udostępniam materiały do paru piosenek już wydanych przez NK do samodzielnego montażu. Instrukcji obrazkowej niestety brak.

Poniżej są "odmiksowane" projekty, wszystkie nagrania są mniej lub bardziej ustawione tak jak powinny być. Tylko brać i miksować.

1. Aldnoah.Zero ED 2 - aLIEz: https://drive.google.com/file/d/1o_-eI6PC0m26A14EXOraZQRErrawMYiS/view?usp=sharing
2. Bleach OP1 – Asterisk: https://drive.google.com/file/d/1uiI9oFwhSxK3cV7MKvo5Sz53o4p0gjIB/view?usp=sharing
3. Itazura na Kiss – Jikan yo Tomare: https://drive.google.com/file/d/1taB3g7T_pButVH3TygOavsBTRH9fICnJ/view?usp=sharing

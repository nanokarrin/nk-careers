---
id: realizacja4
title: Czyszczenie sybilantów i innych brudów
sidebar_label: Czyszczenie sybilantów i innych brudów
---

Czyszczenie sybilantów i _brudów_ w nagraniach wokalistów jest codziennym zajęciem dla realizatorów dźwięku. Poniżej opisuje przykładowy proces obróbki nagrań pod kątem ich 1) od'sybilantowania i 2) czyszczenia brudów.

```Polecam użyć słuchawek do odsłuchy poniższych przykładów audio.```

## Czyszczenie sybliantów

**Sibilance (pol. spółgłoska szczelinowa) to głośne i nieprzyjemnie brzmienie ‘sss’. Polskie sybilanty to: s, z, c, dz, sz, ż/rz, cz, dż, ć, dź lub ang. s, z, sh, zh, ch, j.**

Sybilanty można poskramiać na różne sposoby:
- Pozycją mikrofonu względem wokalisty
- Automatyką filtrów we wtyczce EQ
- Wtyczką de-essera

Tutaj skupię się na ostatniej opcji. Opiszę proces od'sybilantowania nagrań przy użyciu de-essera czyli dynamicznego filtra zakresu częstotliwości gdzie znajdują się sybilanty. Te zakresy mogą być różne dla mężczyzn i kobiet, ale zazwyczaj znajdują się gdzieś na paśmie pomiędzy 6kHz - 14kHz.

Nie będę pisał dokładnie jak używać de-essera bo sam efekt jest stworzony w taki sposób, że zachęca użytkowników do przypadkowego kręcenia pokrętłami i naciskania guzików, które bardzo szybko pozwalają usłyszeć co dzieje się z naszym nagraniem.
Poniżej przedstawiam jak wygląda proces od'sybilantowania i co dzieje się z edytowanymi nagraniami.

1. Najpierw mamy oryginalne nagranie wokalne:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuZ3RQZTAwTnQwdGc/preview?usp=sharing">
</iframe>

2. Po nałożeniu wtyczki z de-esserem:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuZjhLc3BpN2JTUEE/preview?usp=sharing">
</iframe>

3. Tutaj jest przykład odizolowanych sybilanów, które są przyciszane przez wtyczkę de-essera:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuV0M0RXllZ2U2TjA/preview?usp=sharing">
</iframe>


4. Aby upewnić się, że najbardziej problematyczne sybilanty są odpowiednio przyciszone dodaję automatykę czułości de-essera w wybranych miejscach:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuWENwRXhYZWh5OUU/preview?usp=sharing">
</iframe>


W moim DAWie automatyka tego fragmentu nagrania wygląda tak:

<iframe
   frameborder="0"
   width="800"
   height="200"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYub3VXMl9GU2h5MkU/preview?usp=sharing">
</iframe>


5. Teraz nagranie z de-esserem + automatyką + pogłosem:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuT2JGVUZIOXgzYmM/preview?usp=sharing">
</iframe>


6. Obrobione nagranie wokalne połączone z podkładem brzmi tak:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuWWVPc2M0aGRVNjA/preview?usp=sharing">
</iframe>


## Czyszczenie brudów
**Czyli inaczej... anihilacja sonicznego świata poniżej 100 Hertzów**.

Przy pracy z nagraniami fandubbinowymi cały czas borykamy się z zawartością nagrań wokalistów, aktorów lub muzyków, która zawiera nisko-częstotliwościowe błoto. Bez zagłębiania się w szczegóły te brudy rejestrowane przez mikrofony mogą być powodowane przez:
- Rezonans stołu/podłogi na którym stoi stojak na mikrofon
- Prace drogowe za oknem nagrywającego
- Pozycje mikrofonu względem wokalisty

Zazwyczaj na ten problem składa się parę rzeczy, ale na szczęście istnieje bardzo proste rozwiązanie. A mianowicie filtr górnoprzepustowy (nazywany również _high-pass_ lub low-cut) w naszym _EQ_ (korektorze częstotliwości).

Filtr typu high-pass (HP) niszczy (czyt. mocno przycisza) wszystko poniżej naszego wybranego progu. Tym progiem przy każdej produkcji fandubbingowej powinno być 100Hz.

Napiszę to jeszcze raz. Każde nagranie, które obrabiacie powinno mieć high-pass usuwający wszystko poniżej przynajmniej 100Hz.
[**Ten jeden prosty trik spowoduje, że wasze miksy będą brzmiały 1000 razy lepiej (clickbait)**](https://youtu.be/dQw4w9WgXcQ).

Każdy realizator powinien wiedzieć jak ustawić HP na EQ w swoim DAWie. W razie problemów w internecie znajduje się wiele demonstracji jak nałożyć HP na nagrania w DAWie twojego wyboru (n.p., Adobe Audition).

Poniżej przedstawiam drastyczny przykład gdzie potrzeba było ustawić próg high-pass'u wyżej niż 100Hz.

1) Oryginalne nagranie bez nałożonych efektów:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYueURZNU5rWkt1SVk/preview?usp=sharing">
</iframe>

2) Nagranie z nałożonym filtrem górnoprzepustowym (tutaj wybrałem próg filtra na 250Hz bo poniżej tego nic nie pomagało wokalistce w jej brzmieniu):

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuYzNkUUgzYjloS2s/preview?usp=sharing">
</iframe>
<figcaption>Taaaaaak! O wiele lepiej!</figcaption>

3) Dla porównania dodałem automatykę EQ żeby pokazać jak zmienia się brzmienie wokalu podczas zwiększania progu (do 250Hz) filtra:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuWlNhclVNcDZ3M3c/preview?usp=sharing">
</iframe>
<figcaption>Pierwszy wers zaczyna brudno i kończy czysto a drugi wers zaczyna czysto i kończy brudno</figcaption>

4) Tutaj miks bez EQ i de-essera:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYueFJQZXIybnp6ZFU/preview?usp=sharing">
</iframe>

5) A tutaj jest miks z filtrem górnoprzepustowym na 250Hz i paroma korektami EQ + de-esser:

<iframe
   frameborder="0"
   width="450"
   height="66"
   src="https://drive.google.com/file/d/0B7PJyfNgwVYuY1pGSTdHTXRaVDg/preview?usp=sharing">
</iframe>
<figcaption>Taaaaaak! O wiele lepiej!</figcaption>



Jeżeli zwócisz uwagę to usłyszysz że nagranie nabrało życia. Wysokie częstotliwości zostały automatycznie wyolbrzymione przez brak błota i równocześnie polepszyła się zrozumiałość słów w piosence co często może być problemem kiedy słuchamy muzyki na kiepskich słuchawkach dousznych.

~[Razjel](https://maciek-tomczak.github.io/) 👋🏻

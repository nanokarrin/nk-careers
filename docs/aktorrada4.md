---
id: aktorrada4
title: Nagrywanie
sidebar_label: Nagrywanie
---

Jeśli mamy już omówione sprawy sprzętu i swojego miejsca pracy, to czas przejść do tego, jak nagrywać.

## Programy do nagrywania
Jest wiele programów do nagrywania – płatnych, ale i darmowych. Na przykład:
* Płatne z darmową wersją próbną – [Adobe Audition](https://www.adobe.com/pl/products/audition.html)
* Darmowe z opcją podglądu wideo – [Reaper](https://www.reaper.fm/)
* Darmowe bez opcji podglądu – [Audacity](https://www.audacityteam.org/)

Najlepiej wypróbować kilka i zobaczyć, który najlepiej Tobie pasuje, bo mimo wszystko spędzisz w nim trochę czasu, więc lepiej zaprzyjaźnić się z takim programem niż wojować przy każdej próbie nagrywania w nim.

## Zalecenia
* Nagrywaj na minimalnej czułości mikrofonu oraz programu nagrywającego, by uniknąć przesterów.
* Nie jedz słodyczy, ani nie pij słodkich napojów w trakcie nagrywania. Od słodkiego wytwarza się gęsta ślina, która przeszkadza w wyraźnym mówieniu.
* Bądź ostrożny podczas rozgrzewania aparatu mowy (patrz [Lekcja 1](aktorlekcja1)). Jeśli będziesz wykonywać zbyt gwałtowne i/lub obszerne ruchy, to może Tobie np. wypaść szczęka, a tego chyba nikt nie chce.
* Postaraj się wycinać rzeczy niezwiązane ze scenariuszem, nagrywając kwestie. Rozmowa z mamą, komentarze do nagrań, zbędne kaszlnięcia, urwane teksty, kwestie z pomyłkami itp. nie są pomocne w pracy nad projektem albo oceną Twojego podejścia na rekrutację. Jako reżyserzy lub rekruterzy chcemy dostać nagranie kwestii, a przesłuchiwanie dodatkowych rzeczy tylko pochłania czas. Uważaj jednak, żeby podczas pozbywania się ich, nie uciąć przypadkowo fragmentów nagrań (np. początku czy końcówki), które faktycznie mają być wysłane. Najlepiej zostawić moment ciszy przed i po każdej próbie.
* Nagrywając kilka razy tę samą kwestię, uważaj, by nie wpaść w ciąg powtarzania jej identycznie wiele razy pod rząd. Postaraj się, by każdy dubel brzmiał inaczej.
* Bardziej obszerne wyjaśnienie powyższych zaleceń, możesz zobaczyć i posłuchać na naszym kanale: [LIVE Warsztaty aktorskie oraz Q&A](https://youtu.be/PcrRKcWJ5cQ?t=118) (od 1:58)

## Uwagi
* Nie odszumiaj swoich nagrań, ani nie nakładaj na nie żadnych efektów, filtrów i tym podobnych, jeśli wysyłasz komuś swoje nagrania do projektu. Wyjątkiem jest jedynie sytuacja, jeśli zostaniesz o to bezpośrednio poproszony. Najczęściej są wyznaczone inne osoby, które zajmują się obróbką audio, więc Ty nie musisz zaprzątać sobie tym głowy. W przypadku rekrutacji jest oceniana jakość Twojego sprzętu, więc rekruterzy chcą usłyszeć jego oryginalną jakość, a nie tę uzyskaną po nałożeniu efektów.

## Przykłady
Poniżej są filmiki z przykładami, jak nagrywają niektórzy członkowie NanoKarrin – Cearme, konji i nouwak
- [Cearme](https://youtu.be/PcrRKcWJ5cQ?t=2282) (od 38:02)
- konji – [część 1](https://youtu.be/bv3q_KbDyA8), [część 2](https://youtu.be/tJZMUGq3UVA)
- [nouwak](https://youtu.be/HIJyY_20yfM?t=1693)
- Pawlik (przykład jego stanowiska do nagrywania poniżej)

![Stanowisko Pawlika](https://i.imgur.com/iOZhtrd.jpg)

* Otwarta szafa
* Przypięty czterema spinaczami grubszy kocyk
* Mikrofonik na statywie z mini reflection filterem
* Pod mikrofonem i nagrywającym rozłożony kolejny kocyk
* Pozycja nagrywania: na siedząco
* Opcjonalnie zarzucić kocyk na górną część otwartych drzwi szafy
---
id: rekruogolne
title: Witamy w rekrutacji!
sidebar_label: Zasady ogólne
---

# Hej! Fajnie, że jesteś! :)

:::important
UWAGA! Nowe zasady rekrutacji!
:::

### Jak wygląda rekrutacja?
Jeśli interesuje Cię dołączenie do NK, napisz na naszym Discordzie do którejkolwiek osoby o randze "Przewodnik". Przewodnik zdzwoni się z Tobą, by opowiedzieć Ci o grupie, procesie rekrutacji, wydawaniu projektów etc. Taka rozmowa zajmie maksimum kwadrans. Chcemy mieć pewność, że wiesz, na co się piszesz i że będziesz mieć punkt zaczepienia już od pierwszych chwil bycia w grupie!   
(Taka rozmowa z Przewodnikiem może odbyć się też na konwencie).    

Obecnie Przewodnikami są: maseil, Ruthie(Marime), SusieFiedler, funfel, Qki.   

Gdy Przewodnik da Ci zielone światło, otrzymasz rangę "Rekrut". Załóż wątek rekrutacyjny na wybraną przez Ciebie rolę na naszym Discordzie w dziale "Rekrutacja NK". W przypiętym poście znajdziesz wzory rekrutacji, nie zapomnij też dodać odpowiedniego taga. W razie jakichkolwiek wątpliwości nie wahaj się pytać Opiekunów Rekrutacji lub Przewodników – chętnie pomogą, od tego są!   
Link do Discorda: https://discord.gg/nanokarrin  

### Co po rekrutacji?
Po przejściu rekrutacji otrzymasz rangę Członka NK, odpowiednią rolę i dostęp do zamkniętej części Discorda NK. Zostanie też Ci przydzielony senpai, członek grupy, który pomoże Ci się zaaklimatyzować.   
Zaplanujemy też dwie rozmowy, ewaluacje, miesiąc po dołączeniu oraz cztery miesiące po dołączeniu do grupy – chcemy sprawdzić, czy odnajdujesz się w grupie, czy masz dostęp do wszystkich potrzebnych Ci informacji, czy potrzebujesz pomocy, by się rozkręcić. Na czas ewaluacji w ramach przypomnienia o konieczności odbycia rozmowy z Przewodnikiem chwilowo ograniczone zostaną Twoje uprawnienia do pisania na serwerze. Im wcześniej skończysz obie ewaluacje, tym szybciej zostaniesz pełnoprawnym członkiem, nie odkładaj tego! ^^    

### Role projektowe i role funkcyjne
Role projektowe (związane z umiejętnościami potrzebnymi do wydawania projektów, np. Aktor, Tekściarz, Animator) są przydzielane przez rekruterów danych ról. Role funkcyjne (związane z wykonywanymi obowiązkami w grupie, np. Przewodnik) są przydzielane przez Koordynatorów. Zdobycie którejkolwiek z tych ról wiąże się z otrzymaniem roli Członek NK. Każda zdobyta rola musi być odnotowana w Spisie Powszechnym (patrz: Regulamin NK).

### Role projektowe - scenki i piosenki
NK dzieli się na dwa główne nurty: scenki i piosenki. 
* Rangi dostępne w projektach wokalnych: wokalista, realizator dźwięku, tekściarz.
* Rangi dostępne w projektach aktorskich: aktor, dźwiękowiec, dialogista. 
* Rangi wspólne: animator, ilustrator.

### Role
Dokładniejszy opis ról w NK: [klik](/docs/regulamin/role)

### Regulamin NK
Regulamin NK obowiązuje każdego członka grupy. **Dołączając do grupy, akceptujesz regulamin**. [Zobacz regulamin](/regulamin)

### Poprawienie rekrutacji
Kolejną rekrutację będziesz mógł złożyć dopiero po upływie **miesiąca od negatywnej decyzji rekrutera**, chyba że rekruter zarządzi inaczej. Po odrzuceniu rekrutacji ranga "rekrut" zostanie Ci odebrana. Nie bój żaby - kiedy będziesz gotowy, napisz do swojego Przewodnika, by nadał ją ponownie.   

### Aktywny członek NanoKarrin
Jeśli należysz do NanoKarrin i chcesz otrzymać koleją rolę, poproś Przewodnika o rangę "Rekrut", by uzyskać dostęp do działu Rekrutacja na Discordzie. Ta ranga zostanie Ci nadana od ręki. Tam załóż wątek rekrutacyjny na daną rolę. Jeśli wynik będzie pozytywny, otrzymujesz nową rangę. Nie musisz przechodzić ewaluacji, nie zostaje Ci przydzielony Senpai, nie otrzymujesz rangi "Junior".

### Nieaktywny członek NanoKarrin
Jeśli kiedyś należałeś do NanoKarrin, ale odszedłeś lub stałeś się nieaktywny, napisz do Przewodnika. Przewodnik wyjaśni, jak obecnie działamy w grupie i przywróci rangi zgodnie z zapisem w Spisie Powszechnym. Jeśli chcesz, możemy przydzielić Ci senpaia i/lub nadać rangę "Junior".
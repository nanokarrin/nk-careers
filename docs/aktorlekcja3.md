---
id: aktorlekcja3
title: Emisja głosu
sidebar_label: Emisja głosu
---

Emisja głosu to poprawne operowanie głosem w mowie lub śpiewie. Do tego celu wykorzystuje się znane z akustyki zjawisko rezonansu. Polega to na tym, aby strumień drgającego powietrza umiejętnie skierować na rezonatory – jamę gardłową, nosową i ustną. Wtedy otrzymasz największą amplitudę drgań, a w konsekwencji odpowiednio silny, czysty i dźwięczny głos.

Od poprawności emisji głosu uzależnione są efekty artykulacji, frazowania i akcentowania, a nawet wymogi kultury żywego słowa. W dobrej emisji głosu chodzi o to, aby przy minimalnym wysiłku, przy jak najmniejszym obciążeniu wiązadeł głosowych uzyskać jak najpełniejsze brzmienie i jak najlepszą nośność głosu.

Celem ćwiczeń głosowych jest:
* ustalenie średniego położenia głosu, to znaczy charakterystycznej dla danej osoby wysokości głosu
* wyrobienia miękkiego nastawienia głosu – drgania fałdów głosowych pojawiają się wcześniej niż ich zwarcie
* umiejętność kierowania głosu na maskę, czyli na sklepienie podniebienia twardego
* wyrabianie umiejętności modulowania siły i wysokości głosu.

## Ćwiczenia fonacyjne

### Zestaw 1
1. Długa fonacja głoski aaaaaa i innych samogłosek
2. Wybrzmiewanie połączonych samogłosek aouaou. Słupek samogłoskowy:
 * a e y i o u
 * e y i o u a
 * y i o u a e
 * i o u a e y
 * o u a e y i
3. Fonacja głoski aaa z opuszczaniem ramion podniesionych wcześniej do boku
4. Jak wyżej, tylko połączone samogłoski aou
5. Wydłużanie głoski „m”
6. Wydłużanie głosek mmmaaammmaaa, mmamma mia, mmmammma mia
7. Wymawianie jednym cięgiem, na przykład, dni tygodnia, liczenie wronich ogonów (patrz podpunkt 11. w Zestawie 2)
8. Wymawianie coraz dłuższych zdań na jednym wydechu
9. Wyczuwanie wibracji nosa lub kości czaszki przy wymawianiu „m”, „n”
10. Wymawianie głoski „a” nisko i wysoko – rezonatory piersiowe i głowowe

### Zestaw 2
1. W pozycji leżącej połóż wnętrze otwartej dłoń na twarzy. Nie uciskając nosa i warg, wymawiaj głoskę „mmm” płynnie, a potem „m… m… m…” w sposób przerywany. Staraj się zaobserwować wibracje i drżenia, jakie wywołuje głoska „m”. Przy czym zmieniaj położenia dłoni i szukaj miejsca, w którym dźwięk rezonuje najwyraźniej
2. To samo ćwiczenie wykonaj z użyciem głoski nnn oraz „n… n… n...”
3. W pozycji leżącej wymawiaj głoskę m, utrzymując dźwięk na jednym tonie na przemian „mmm” i „m… m… m…”
4. Dodawaj do spółgłoski „m” samogłoski, pamiętając o utrzymaniu dźwięku na jednym tonie: 
 * mmmaaa, mmmooo, mmmuuu
5. Wdech i na wydechu mormorando (mrucząc) na głosce „mmmmmm” zwieramy i otwieramy usta
6. Mormorando z lekko wysuniętym językiem
7. Wdech i na wydechu wypowiadaj „ma me mi mo mu” na jednym tonie
8. Na wydechu śpiewaj głoskę „a” z wyciągniętym językiem na jednym tonie 
9. Wdech i na wydechu wypowiadaj:
 * mamam, memem, mimim, momom, mumum
 * zamm, zemm, zimm, zomm, zumm
 * wamm, wemm, wimm, womm, wumm
 * wamam, wemem, wimim, womom, wumum
 * wija, wije, wijo, wiju
 * Jola, Wiola
 * na, ne, ni, no, nu
 * nija, nije, nijo,niju
 * da, de, di, do, du
 * pa, pe, pi, po, pu
 * appa, eppe, oppo, uppu
 * tpa, tpe, tpi, tpo, tpu
 * pta, pte, pti, pto, ptu
 * dram, drem, drym, drom, drum
 * dwam, dwem, dwym, dwom, dwum
 * bam, bem, bim, bom, bum
 * babam, bebem, bibim, bobom, bubum
 * bram, brem, brym, brom, brum
 * babba, babbe, babbi, babbo, babbu
 * ga, ge, gi, go, gu
 * fa, fe, fi, fo, fu
 * af, ef, if, of, uf
 * afa, efe, ifi, ofo, ufu
10. Powtarzaj wyrazy, wydłużając głoskę „m”, na przykład:
 * mmmoc, mmmost, mmmorze, mmmokry, mmmetrumm, mmmetoda, mmmelodia, mmmundur, mmmuzyka, mmmuskuły
 * pommmoce, kommmpot, kammmera, hammmulec, sammmolot, wymmmowa, brammma, tammma, lammma, dammma
 * tammm, powiemmm, mmmammm, dammm, dommm, albummm, poziommm, dymmm, sammm, łommm, grommm, złommm, ogrommm
11. Na jednym wydechu mów znane wyliczanki lub, na przykład, licz wrony:
 * jedna wrona bez ogona, druga wrona bez ogona, trzecia wrona bez ogona itd.
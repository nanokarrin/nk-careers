---
id: hard3
title: Mikrofony
sidebar_label: Mikrofony
---
Opiekun listy: Lirveni#1394 


|      NICK      |              MIKROFON              |                               INTERFEJS                              |
|:--------------:|:----------------------------------:|:--------------------------------------------------------------------:|
|       Aga      |            Behringer C1U           |                                   -                                  |
|      Akay      |          LD Systems D1013C         |                                   -                                  |
|      Aloes     |          Reloop sPodcaster         |                                   -                                  |
|       Ari      | Samson C01U Pro \ LD Systems 1012C |                                   -                                  |
|      Aris      |           Samson C01U Pro          |                                   -                                  |
|      baquu     |           Samson C01U Pro          |                                   -                                  |
|      Colty     |          Behringer XM8500          |                            Behringer UMC22                           |
|      Dani      |           Samson G-Track           |                                   -                                  |
|      Denvi     |               MXL 770              |                            Audiobox USB 96                           |
|     Elthien    |          Behringer XM8500          |                       Behringer U-PHORIA UMC202                      |
|      Elven     |             Rode Nt1-A             |                            Steinberg UR12                            |
|   Falloutman   |              Rockband              |                                   -                                  |
| Filipus Magnus |     MXL 770 \| Behringer BA 85A    |                       Behringer U-PHORIA UMC202                      |
|     Hikari     |           Samson C01U Pro          |                                   -                                  |
|     Hotaru     |          Reloop sPodcaster         |                                   -                                  |
|      Jerry     |           Samson C01U Pro          |                                   -                                  |
|     KanadeQ    |             Rode NT1-A             |                       Behringer U-PHORIA UMC202                      |
|   KaraKopiara  |           Samson G-Track           |                                   -                                  |
|      Kefir     |           Samson C01U Pro          |                                   -                                  |
|      Klara     |             Rode NT1-A             |                        Focusrite Scarlett 2i2                        |
|    Krecioch    |           Samson C01U Pro          |                                   -                                  |
|   Lady Cherry  |           Samson C01U Pro          |                                   -                                  |
|     Lirveni    |              Blue Yeti             |                                   -                                  |
|      Mian      |           Samson C01U Pro          |                                   -                                  |
|      Mira      |           Samson C01U Pro          |                                   -                                  |
|      Miyo      |              Blue Yeti             |                                   -                                  |
|      Miyu      |             Rode NT1-A             |                                   -                                  |
|     Nanoha     |         AKG Perception 120         |     Mikser Behringer Xenyx 302USB \| Power Supply Behringer PS400    |
|      Natsu     |              Blue Yeti             |                                   -                                  |
|     nouwak     |              AKG C214              |                    Focusrite Scarlett 2i2 2nd GEN                    |
|     Orzecho    |             Shure SM7b             | Personus Audiobox USB \| Scarlett Solo \| Preamp TritonAudio FetHead |
|     Pawlik     |          Reloop sPodcaster         |                                   -                                  |
|     Pchełka    |             Shure SM7b             |  Presonus Audiobox USb \| Scarlet Solo \| Preamp TritonAudio FetHead |
|      Roro      |             Novox NC-1             |                                   -                                  |
|      Rose      |           Samson C01U Pro          |                                   -                                  |
|      Ruiri     |        Audio-Technica AT200        |                               Behringer                              |
|     Rzulia     |           Samson C01U Pro          |                                   -                                  |
|    Seraskus    |        Audio-Technica At2035       |                       Behringer U-PHORIA UMC204                      |
|    Shiguroya   |        LD System D1013C USB        |                                   -                                  |
|     Skryty     |             Novox NC-1             |                                   -                                  |
|     Smiley     |        M-Audio Vocal Studio        |                                   -                                  |
|      Snow      |              Blue Yeti             |                                   -                                  |
|     Spidek     |           Samson C01U Pro          |                                   -                                  |
|      Taola     |             Novox NC-1             |                                   -                                  |
|      Ytna      |           Samson C01U Pro          |                                   -                                  |

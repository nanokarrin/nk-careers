---
id: dzwiekwarsztaty
title: Warsztaty dźwiękowe
sidebar_label: Warsztaty
---

## 11.12.2020 - Filipus Magnus

Zapis z warsztatów przeprowadzonych przez Filipusa dla juniorów.

<iframe width="560" height="315" src="https://www.youtube.com/embed/iuqpnKvz9pg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Materiały
[Paczka z projektem Reapera i użytymi plikami dźwiękowymi](https://www.mediafire.com/file/gwfrvgk1ak8rk50/gorzejnapewnojuzbycniemoze.zip/file)

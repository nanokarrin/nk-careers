---
id: aktorfaq
title: FAQ
sidebar_label: FAQ
---

## Czym są modulacje?
Modulacje to wszelkie zmiany tonu i wysokości głosu jak również w pewnym stopniu zmiany barwy, które przeinaczają Twój naturalny głos, którym posługujesz się na co dzień, w inny. By wyćwiczyć modulacje, możesz sam poeksperymentować wymyślając różne głosy i próbując je odtworzyć na głos. Możesz też spróbować naśladować głos z bajki, gry, filmu lub sławnych osób, który wydaje Ci się ciekawy. Jednak bywa tak, że nawet po miesiącach porządnych i systematycznych ćwiczeń nie będzie wychodzić Ci żadna modulacja. Jeśli tak się stanie, to nie martw się tym zbytnio, tylko poświęć większą uwagę na ćwiczenia Twojej naturalnej barwy, by mieć nad nią jak największe panowanie. Bo nie liczy się ilość, tylko jakość, prawda?

## Czym są przestery?
Przestery występują, gdy poziom nagrania tego, co mówimy, jest tak wysoki, że próbuje przekroczyć 0 dBFS. Z reguły towarzyszy temu, na przykład, zapalenie się czerwonej lampki na mikrofonie albo zmiana koloru paska pokazującego głośność nagrania w programie – z zielonego bądź żółtego na czerwony. To czerwone oznaczenie ostrzega o próbie przekroczenia tej granicy. Odsłuchując nagranie, gdzie wystąpił przester, mogą się pojawić nieprzyjemne trzaski.

## Czym są kłapy?
Aktorzy głosowi nie mogą grać na czas według swojego upodobania. Jeśli tak się zdarza, to jest to bardzo rzadkie zjawisko. Zwykle muszą dostosować się do czasu, w którym mówi dana postać. Zarówno w animacjach jak i w produkcjach z żywymi aktorami muszą obserwować, kiedy ich bohater otwiera, a kiedy zamyka usta i wpasować się w to czasowo. Takie otwarcia ust widoczne na ekranie nazywa się w (fan)dubbingu kłapami. Aktorzy głosowi powinni pilnować, żeby mieścić się w kłapy, ponieważ jeśli trzeba byłoby znacząco zmienić tempo ich nagrań, czyli przyspieszyć lub spowolnić daną kwestię, to brzmiałaby ona sztucznie i od razu  byłoby wiadome, że nie jest to w pełni naturalny głos. Jednak czasem w projekcie fandubbingowym może zdarzyć się tak, że osoba odpowiedzialna za dialogi jest jeszcze niedoświadczona i napisze tekst, który jest dłuższy lub krótszy od widocznych kłap i mimo usilnych prób nie jesteś w stanie przez to nagrać się odpowiednio w czasie. Przedyskutuj wtedy tę sprawę z dialogistą i/lub reżyserem scenki. Oni powinni wtedy podpowiedzieć Ci, jak zagrać, aby dany tekst został wypowiedziany odpowiednio w kłapy lub zaproponują zmianę danego tekstu na dłuższy lub krótszy.

# Chcesz wiedzieć więcej?
Jakiś czasem temu członek NanoKarrin, Cearme, zrobił live, na którym prowadził warsztaty z aktorstwa i odpowiadał na pytania, więc jeśli chcesz dowiedzieć się czegoś jeszcze lub szukasz dodatkowych wyjaśnień, to udaj się na nasz kanał:
- [LIVE Warsztaty aktorskie oraz Q&A](https://youtu.be/PcrRKcWJ5cQ)
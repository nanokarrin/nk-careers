---
id: dzwiekproces
title: Ogólnie o procesie
sidebar_label: Proces
---

## Etapy procesu

Możemy podzielić cały proces tworzenia dźwięku na kilka etapów. Nie jest to ścisły podział, często można robić kilka naraz, albo wręcz przeciwnie - podzielić się pracą na kilka osób, w której każdy będzie wykonywał bardzo wyspecjalizowany i wąski zakres czynności (np. tylko odszumiał dialogi). Taki podział pozwoli jednak pokazać różne aspekty pracy dźwiękowca, bo jego rola nie sprowadza się tylko do "wytnij originalne głosy i wstaw zamiast nich polskie".

### Muzyka
Generalnie muzykę staramy się brać z OST (Original Soundtrack) jeśli są dostępne. Na słuch identyfikujemy, które piosenki i w których miejscach są użyte w oryginale i umieszczamy je w tych samych miejscach. Często trzeba przycinać czy przemontowywać muzykę, bo nie zawsze jest używany cały utwór, a np. sam początek albo okolice punktu kulminacyjnego.

### Efekty dźwiękowe
#### Znalezienie
Przeważnie dźwiękowcy mimo posiadania różnych paczek dźwięków (przykładowo spisanych [tutaj](dzwiekwstep)) w tworzonych projektach muszą użyć specyficznych dźwięków, których nie mają w swoim repertuarze. Trzeba wtedy szukać w internecie, najlepiej z użyciem angielskich nazw, bo to daje większe szanse na znalezienie szukanego dźwięku.

#### Montaż
Dźwięk musi zgrywać się z obrazem - konkretne odgłosy muszą być umieszczone w odpowiednim miejscu na linii czasu. Często do montażu wykorzystywane są edytory wideo, ze względu na możliwość pracy z obrazem i podglądu na żywo.

#### Obróbka
Generalnie obróbka wygląda bardzo podobnie zarówno przy odgłosach jak i dialogach - trzeba odpowiednio ustawić głośność, panoramę i nałożyć efekty w zależności od potrzeb.


### Dialogi
#### Przetwarzanie wstępne
Przed przystąpieniem do montażu, otrzymane nagrania można przetworzyć, np. odszumić, poprawić ich klarowność equalizerem, wyrównać głośność kompresorem czy od razu nałożyć potencjalnie zasobożerne efekty, które mogłyby wpływać na wydajność, jeśli byłyby nakładane w DAW-ie.

#### Montaż
Podobnie jak odgłosy, dialogi również muszą zgrywać się z obrazem. Tutaj trzeba pamiętać o tym, że zarówno początek jak i koniec kwestii powinny się z grubsza zgrywać z ruchem ust postaci. Podobnie przerwy w dialogach powinny być wtedy, gdy usta są zamknięte. Dialogi można lekko spowalniać albo przyspieszać, by lepiej pasowały do obrazu, ale zbyt duża manipulacja powoduje powstanie słyszalnych artefaktów.

#### Obróbka
Efekty również muszą mieć odpowiednią głośność i panoramę, a także efekty w zależności od potrzeb. W odróżnieniu od SFX-ów, na dialogi nakładamy przeważnie specyficzne efekty, które mają konkretne cele - przykładowo pogłosy zazwyczaj mają oddawać akustykę otoczenia, albo sygnalizować myśli bohaterów.

### Tworzenie finalnego wideo
W przypadku scenek zazwyczaj nie jest zaangażowany animator, bo jedyna zmiana w wideo to dodanie loga na początku, a napisów końcowych na końcu. Dlatego niezależnie od tego, w czym powstawało udźwiękowienie filmu, finalnie dźwiękowiec musi skorzystać z programu do montażu wideo, by finalne wideo wraz z dźwiękiem wyeksportować w dobrej jakości. Naszą platformą docelową jest YouTube, dlatego jeśli chodzi o format i ustawienia, najlepiej stosować się do jego
[zaleceń](https://support.google.com/youtube/answer/1722171?hl=pl).


## Kulisy produkcji

Poniższy materiał przedstawia różne aspekty tworzenia dubbingu. Druga połowa skupia się na aspektach dźwiękowych. Film nie jest przesadnie techniczny, zrobiony jest raczej w stylu "popularnonaukowym" i może dać ogólne pojęcie o tym, czym zajmuje się dźwiękowiec i jak wygląda cały proces pracy nad audio w fandubbingu.

<iframe width="560" height="315" src="https://www.youtube.com/embed/HIJyY_20yfM?start=1277" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
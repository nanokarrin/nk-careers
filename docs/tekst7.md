---
id: tekst7
title: Brzmienie
sidebar_label: Brzmienie
---

### Eufonia
To, czy piosenka się spodoba i czy będzie "płynąć", zależy w dużej mierze od jej brzmienia, a konkretnie brzmienia "pięknego", czyli eufonii.
Eufonia, cytując wikipedię, to *nazwa zbiorcza dla wszystkich typów organizacji nadanej tekstowi w jego warstwie brzmieniowej, które w jakimś okresie uważane są za estetycznie dodatnie*.   
Na eufonię składa się wiele czynników, które można podsumować w czterech punktach:
1. Śpiewalność
2. Rymy
3. Motywy
4. Tempo

#### 1. Śpiewalność
Co nam po tekście piosenki, którego nie da się wymówić nawet przy wolnym tempie, czytając? Polski opiera się w dużej mierze na spółgłoskach, które - w przeciwieństwie do samogłosek - trudno wymówić. Należy więc unikać trudnych zbitek spółgłoskowych czy podwojeń liter, które w najlepszym razie spowodują zaplucie się wokalisty i nienaturalną emisję, a w najgorszym sprawią, że wokalista ułatwi sobie zadanie przez zjedzenie co trudniejszych fragmentów. Przykładem morderczego tekstu jest "gwiazdorstwo w świetle reflektorów" (fragment piosenki z Im@sa, szybkie tempo).  
Napisawszy tekst, przeczytaj go na głos. Nie żartuję: na głos. Czytanie w myślach nic nie da, tam zawsze mamy idealną dykcję i trzymamy tempo. Jeśli ty - jako tekściarz - napotkasz jakieś trudności w wymowie, zastanów się dobrze nad danym fragmentem, bo prawdopodobnie wokalista też będzie się trudził.  
Po poprawieniu co trudniejszych miejsc zaśpiewaj piosenkę i też sprawdź, czy się da. Tutaj nie ma co jęczeć, że nie umiesz śpiewać - śpiewanie u tekściarzy jest ZAWSZE poglądowe (patrz: demo), nikt intonacji nie będzie oceniał.  
Jeśli masz taką możliwość, skontaktuj się z wokalistą, który będzie śpiewał twój tekst. Porozmawiaj z nim, jakie głoski sprawiają mu trudność i na jakiej wysokości. Pomóżcie sobie wzajemnie, by piosenka była jeszcze lepsza. Jest też kilka elementów uniwersalnych, przykładowo w wyższych dźwiękach powinno się używać raczej samogłosek otwartych (a, o) niż zamkniętych (i, y), co ułatwia emisję.

#### 2. Rymy
Musisz sobie uświadomić, że:
* rymy niosą się na samogłoskach i półsamogłoskach 
* spółgłoski się nie rymują
* w przypadku słów wielosylabowych (2+) samogłoską rymującą się jest ta, na którą pada akcent, a nie ostatnia

Kilka przykładów słów z wypisanymi obok samogłoskami:
kot -> **o**  
mama -> **a** a  
motyka -> o **y** a  

Takie postrzeganie słów pomoże ci oderwać się od spółgłosek i znaleźć ciekawe rymy.

Gdy poczujesz się pewniej w temacie rymów, możesz poćwiczyć wplatanie w tekst piosenki asonansów - czyli powtarzanie tych samych głosek w bliskim sąsiedztwie. Być może eufonia wynikająca z zastosowania takiego zabiegu nie będzie oczywista w zapisie tekstu, ale zapewniam cię, że usłyszy się ją w piosence. Uwaga: półsamogłoski mogą zaburzać rymy asonansowe.

#### 3. Motywy
Motywy to melodie, które powtarzają się w piosence. Może się zdarzyć (z różnych względów, np. śpiewalność), że zmienicie jeden motyw - coś skrócicie, coś wydłużycie, dodacie sylabę w przedtakcie. W takim wypadku należy zmienić też pozostałe, odpowiadające mu motywy, żeby zachować powtarzalność.

#### 4. Tempo
Zmiana tempa emisji głosu w piosenkach japońskich jest częstym zjawiskiem. Pewnie nieraz zauważyliście, jak tempo piosenki na moment wzrasta niemożebnie (nagle pojawia się cholernie długi, przeładowany sylabami fragment, który daje wrażenie przyspieszenia, podniesienia tempa ;)).  
Na takie miejsca trzeba zwracać szczególną uwagę i podwoić przy nich czujność. Dlaczego? Otóż, jak pisałam wcześniej, język polski to język o dużej ilości spółgłosek - język szumiący, szeleszczący. O ile japoński - język samogłoskowy - może pozwolić sobie na zwiększenie tempa bez utraty jakości dykcji, tak polski mocno nam to ogranicza.  
Zwykle w takich sytuacjach sylaby są usuwane, a dźwięki przedłużane tak, aby jak najmniej zmienić motyw, ale jednocześnie ograniczyć sylaby do takiej ich liczby, którą da się wymówić.  
Dodatkowo takie zmiany tempa, przełożone na język polski w stosunku 1:1 sprawiają wrażenie, jakby tekściarz na siłę chciał wepchnąć do melodii jak najwięcej słów. Ergo, nienaturalnie.

### Wnioski
Osiągnięcie eufonii w tekście piosenki to nie jest coś, co można osiągnąć, pisząc jeden tekst piosenki. To umiejętność, którą trzeba wyćwiczyć, a nawet wtedy raz wychodzi gorzej, raz lepiej. Ważne jest to, aby się nie poddawać i pisać, pisać, pisać, mając gdzieś z tyłu głowy śpiewalność, rymy, motywy i tempo :).

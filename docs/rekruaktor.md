---
id: rekruaktor
title: Rekrutacja na aktora
---

## Osoby decyzyjne
Obecnie rekrutację sprawdzają:
* Kiara (Discord: @kiarkiar)
* Xi (Discord: @xitty)
* Nanoha (Discord: @nanosia)
* SusieFiedler (Discord: @susiefiedler)

To oni podejmują decyzję o nadaniu rangi.  
Jeśli przed podejściem do rekrutacji masz jakieś pytania odnośnie do gry aktorskiej, modulacji, emisji głosu itp., a nie jest to uwzględnione ani tutaj, ani w Szkółce NanoKarrin, napisz bezpośrednio do któregoś z nich na Discordzie.

## Przebieg rekrutacji
1. Nagraj wiersz Jana Brzechwy "Krasnoludki". Staraj się zastosować jak najwięcej emocji oraz zmian głosu! Przeczytaj wcześniej uważnie tekst, żeby nie było później pomyłek.
<p className="indent"><i>Krasnoludki z wszystkich miast<br/>
Urządziły w lesie zjazd.<br/>
Program zjazdu był taki:<br/>
Po pierwsze –<br/>
Gdzie zimują raki?<br/>
Po drugie –<br/>
Czy brody są dosyć długie?<br/>
Po trzecie –<br/>
Czy zima może być w lecie?<br/>
Po czwarte –<br/>
Co robić, żeby dzieci nie były uparte?<br/>
Po piąte –<br/>
Skąd wiadomo, że zawsze po czwartku jest piątek?<br/>
Po szóste –<br/>
Dlaczego niektóre orzechy są puste?<br/>
Pierwszy mówić miał najstarszy,<br/>
Ale tylko czoło zmarszczył;<br/>
Drugi mówić miał najmłodszy,<br/>
Więc powiedział coś trzy-po-trzy;<br/>
Potem głuchy streścił szeptem<br/>
Wszystko to, co słyszał przedtem;<br/>
Ślepy mówił o kolorach,<br/>
Lecz przeoczył coś, nieborak;<br/>
Zaś niemowa opowiedział<br/>
O tym, czego sam nie wiedział.<br/>
Mańkut milcząc spojrzał wokół<br/>
I napisał tak protokół:<br/>
„Krasnoludki z wszystkich miast<br/>
Urządziły w lesie zjazd.<br/>
O czym tam się mówiło przez dwanaście godzin,<br/>
To pana, proszę pana, zupełnie, ale to zupełnie, nie obchodzi!”</i></p>

2. Przygotuj **jedną** (1) scenkę dubbingową. Nie więcej. Nie przesyłaj więc linków do kilku filmików ze scenkami, a wybierz jeden z nich.  
Jak to zrobić?
* Znajdź postać z anime, filmu animowanego, gry bądź innej serii, którą chcesz odegrać i wybierz scenę. Nie wybieraj postaci, które prowadzą narrację, są bohaterami audiobooków, słuchowisk itp. Krasnoludki mają na celu sprawdzić Twój zakres możliwości, a scenka odegranie emocji bohatera wynikających z jego mimiki twarzy, mowy ciała lub dziejących się wokół wydarzeń. Odpada więc również dubbingowanie komiksów, mang, voice/demo reele, trailery filmów/seriali itp. Scenka powinna zawierać jakąś wybuchową emocję: rozpacz, euforię, gniew etc.
* Dubbingujemy po polsku, więc chcemy sprawdzić wymowę języka polskiego. Jeśli oryginalna scenka, którą wybrałeś, jest w innym języku, to stwórz proste tłumaczenie. Nie musi to być nic skomplikowanego. Nie szkodzi, jak nie będzie idealnie w ruch ust postaci. Jeśli korzystasz z gotowej polskiej wersji językowej, to spisz sobie z niej dialogi.
* Scenka musi zawierać minimum 60 słów, ale nie być dłuższa niż 2-3 minuty. Zrezygnuj z fragmentów z długimi momentami ciszy.
* Nagraj swój głos mikrofonem w programie do nagrywania głosu. Najlepiej użyj takiego, który umożliwia także podgląd obrazu z filmu, np. [Reaper](https://www.reaper.fm/). Tutorial "Pierwsze kroki w Reaperze": [LINK](https://youtu.be/K6a-c2ckMlA?t=106)
* Jeśli korzystałeś z Reapera (lub innego programu współpracującego z obrazem), wyeksportuj obraz wraz z dźwiękiem. Jeśli natomiast masz samo nagranie głosu, w programie do montowania (np. Windows Movie Maker) umieść wideo i dodaj do niego swoje nagranie. Cały plik zapisz jako wideo. Nie ma potrzeby tworzyć ścieżki dźwiękowej. Wystarczy nam tylko Twój głos i obraz. Jeśli wideo z oryginalnej scence ma dźwięk, to usuń lub wycisz go, żeby nie zagłuszał Twojego głosu.
* Scenka powinna zostać stworzona na potrzebę rekrutacji, tak aby przedstawiać Twój obecny, najbardziej aktualny poziom.
3. Scenkę wrzuć na serwis, który pozwala oglądać filmy bez pobierania. To samo zrób z „Krasnoludkami”. Do obu plików polecamy Google Drive. Udostępnij pliki każdemu, kto posiada do nich link. W [Szkółce aktorskiej](aktorspistresci) jest poświęcony na to temat: [Udostępnianie nagrań](aktorrada5) .
4. Załóż nowy post rekrutacyjny na Discordzie NK. Możesz podejrzeć, jak robili to inni :)

Wynik rekrutacji pojawi się w formie wiadomości w założonym przez Ciebie wątku. Jeśli nie odpiszemy po dwóch tygodniach, przypomnij się. Też jesteśmy ludźmi, mamy mnóstwo spraw na głowie, możemy po prostu zapomnieć (choć staramy się nie).

## Jaki może być wynik rekrutacji?
### Pozytywny
Otrzymasz rangę Aktora NK. Oznacza to, że jesteś zdolną bestią z dobrym mikrofonem, która bez problemu może brać udział w projektach dubbingowych NanoKarrin.

### Negatywny
Nie otrzymasz rangi Aktora NK. Rekrutacja może być odrzucona przez dowolną kombinację poniższych powodów:
<ul>
<li>Brak umiejętności aktorskich</li>
<li>Brak dobrego mikrofonu, tzn. jakość nagrań uniemożliwiająca branie udziału w projekcie</li>
<li>Brak jednego z dwóch materiałów rekrutacyjnych. Przesłanie tylko jednego z obowiązujących równe jest z odrzuceniem rekrutacji i możliwością ponownego podejścia dopiero za miesiąc</li>
</ul>
Jednak niczym się nie martw! Specjalnie na taką okazję przygotowaliśmy Szkółkę aktorską, w której znajdziesz niezbędne materiały do nauki, aby podejść ponownie do rekrutacji po miesiącu i ją przejść pozytywnie!

## Uwaga!
* Nie odszumiaj nagrań i nie nakładaj na nie żadnych efektów, filtrów itp. Rekrutacja ma na celu sprawdzenie oryginalnej jakości audio Twojego mikrofonu, a nie takiej, jaka jest po obrobieniu nagrania
* Nagrania z telefonu, z mikrofonu dołączonego do słuchawek itp. zwykle mają zbyt słabą jakość
* Przed rekrutacją możesz napisać do rekrutera na priv, czy wybrana scenka będzie się nadawać (=pokazujesz oryginalny fragment). Nie możesz natomiast prosić rekrutera, by na privie ocenił Twoje nagrania, by sprawdzić "czy jesteś dość dobry na rekrutację" (=pokazujesz swój dubbing). Do tego celu polecamy kanał "aktorzy" na naszym Discordzie - możesz go znaleźć w kategorii "Grupy Twórców". 

## Słowo od Nanohy
Pamiętaj, że tylko i wyłącznie od swojego zaangażowania, samodyscypliny i chęci zabawy zależy, czy staniesz się członkiem naszej grupy. Cel, który niesie ze sobą rekrutacja, a następnie wejście na wyższy poziom rangi, jest bardzo prosty. Czytaj, próbuj, podpytuj, trenuj, naucz się i zacznij tworzyć fanduby.  
Jeśli odrzucimy Twoją rekrutację, kolejną będziesz mógł zamieścić dopiero po upływie minimum jednego miesiąca. Czas ten poświęć na podstawę rozwijania siebie oraz własnego talentu, czyli codzienny TRENING. Wiadomo, że początki zawsze są trudne, wszystko wydaje się bardzo skomplikowane, ale wierz mi, że czytając, prosząc o pomoc doświadczone osoby, czy samemu próbując coś zrobić, szybko załapiesz, jak to działa i jak dużą frajdę można z tego mieć.

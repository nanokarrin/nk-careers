---
id: tekst2
title: Warsztat tłumacza
sidebar_label: Warsztat tłumacza
---

##### Jaki język?
Idealnie by było, gdyby tłumacz-tekściarz, biorąc się do roboty, perfekcyjnie znał język oryginalny piosenki. Wiem jednak, że w większości przypadków tak nie jest. Część z was pewnie uczy się jakiegoś wschodniego języka albo chociaż zna kilka słów, powiedzonek, potrafi liczyć, ale nie jest to poziom wystarczający, żeby cokolwiek przetłumaczyć.

Wielu ludzi przychodzi do NK pisać teksty i dziwi się, że jako materiały referencyjne dajemy artykuły w języku angielskim. Jak to, przecież nie jesteście tak biegli, żeby to przeczytać! Cóż. Jeśli nie jesteście, to musicie się stać. Bo waszym drugim źródłem tekstu, zaraz po oryginale, powinny być tłumaczenia angielskie.  
Tłumaczenia angielskie zwykle robione są przez ludzi, którzy japoński znają dość dobrze - na tyle dobrze, by - powiedzmy - bez problemu oglądać anime w oryginale. Oczywiście zawsze istnieje margines błędu - dlatego sięgamy po tłumaczenia, nie tłumaczenie. Im więcej tłumaczeń, tym większa możliwość sprawdzenia, które fragmenty są pewne (bo wszędzie są takie same), a które mają nie do końca jasne znaczenie (bo zostały przetłumaczone różnie). To da nam lepszy obraz sytuacji i poszerzy naszą interpretację. Być może w drugim albo trzecim tłumaczeniu pojawi się fraza, dzięki której pojmiecie sens piosenki i będziecie w stanie napisać coś, co ma ręce i nogi.

##### A polskie tłumaczenia?
Do polskich tłumaczeń polecam zaglądać w ostateczności, zwłaszcza tyczy się to portali typu tekstowo, gdzie większość tłumaczeń wygląda jak przerzucona przez google translator. Polacy też najprawdopodobniej tłumaczyli ten tekst z angielskiego, co nam daje możliwość podwojonego błędu: jeśli tłumacz z japońskiego się machnął i coś źle zrozumiał, a potem taki tekst dostał się w ręce tłumacza z angielskiego, który błąd powielił albo pogorszył, będziecie najprawdopodobniej pracować na czymś, co ma wiele "tajemniczych" i "metaforycznych" sformułowań i pomyślicie sobie: "Kurczę, ale ten japoński poetycki!".  
Do tłumaczeń polskich zaglądałabym tylko po to, żeby sprawdzić, jak można ubrać w słowa myśl - sens, który macie w głowie,
który już wypracowaliście. Nie patrzcie na nie, by zrozumieć.
Oczywiście nawet doskonała znajomość angielskiego (czy jakiegokolwiek innego języka, z którego będziecie tłumaczyć) nic wam nie da, jeżeli nie będziecie potrafili dobrze napisać tego po polsku.

#### Podstawowym warsztatem tłumacza jest jego język ojczysty
Co z tego, że rozumiecie "he's all ears", jeśli przełożycie to jako "zamieniam się w ucho"? To trochę podkoloryzowany przykład, ale całe rzesze początkujących tekściarzy zadowalają się tekstem, w którym np. podmiot zmienia się co wers albo są błędy gramatyczne (np. "będąc małym dzieckiem, mama kupiła mu rower").  
Dlatego tak dużą wagę przykładamy nie tylko do sensu piosenki, ale i sposobu jego przedstawienia - naturalności, gramatyki, ba, nawet interpunkcji i ortografii. Jeśli dla kogoś nie jest oczywiste, gdzie postawić przecinek w zdaniu "Kupię bułkę chyba że będą tylko czerstwe", to wiadomo, że trzeba nad tym popracować.

Nie bójcie się własnego języka. Żeby ułatwić wam naukę, w przyklejonym poście "Witaj w szkółce tekściarstwa" udostępniliśmy wam:
1. Słownik gramatyczny języka polskiego
Program, który zawiera m. in. odmiany rzeczowników i czasowników, w tym niektórych nazwisk. Jeśli mylą wam się celownik z biernikiem, nie jesteście pewni, czy woda wrze czy wre - tam łatwo to sprawdzić.
2. Skrócony poradnik poprawnej polszczyzny: na początku zagadnienia gramatyczne, potem ortograficzne i interpunkcyjne, a na końcu stylistyczne. Każda lekcja omawia jedno zagadnienie. Lektura idealna do poduszki.

My możemy jedynie dać wam narzędzia. W waszej gestii leży, jak je wykorzystacie.

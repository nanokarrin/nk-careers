---
id: tekst5
title: Struktura rymowa
sidebar_label: Struktura rymowa
---

### Wychowaliśmy się na rymach
Weźcie którąkolwiek polską piosenkę, którykolwiek polski wiersz. Wszystkie będą się rymować. Ba, nawet reklamy się rymują, zwłaszcza te dla dzieci. Dlaczego? Rymowanki łatwo zapadają w pamięć, szybciej jest je sobie przypomnieć, stanowią swego rodzaju mnemotechnikę do ułatwienia sobie dostępu do innych rzeczy.  
Rymy działają jednak najlepiej, kiedy są ułożone w schematy, zwłaszcza takie, które się powtarzają. To samo tyczy się melodii - lubimy pewne piosenki, bo mają ciekawe, powtarzalne schematy. Melodia bez takich schematów nie brzmi dla nas jak muzyka, irytuje, drażni [patrz tu] (https://www.youtube.com/watch?v=RENk9PK06AQ).

To dlatego stworzenie struktury rymowej w tłumaczeniu piosenki jest tak ważne. Sprawi, że piosenka wpadnie w ucho, będzie melodyczna, słowa łatwiej zapadną w pamięć - a dzięki temu zwiększycie szansę na to, że ktoś przesłucha utworu jeszcze raz. A do tego przecież dążymy ;).  
Strukturą rymową nazywamy układ rymów w piosence: który wers rymuje się z którym, nie tylko w obrębie jednej zwrotki czy refrenu, ale całej piosenki. Przyjmuje się, że zwrotki w jednej piosence powinny mieć taką samą strukturę rymową. Oczywiście i tu bywają wyjątki, ale nie na wyjątkach się teraz skupimy.

### Jak stworzyć strukturę?
#### Piosenki zachodnie
W przypadku piosenek zachodnich sprawa jest zwykle prosta. Zwykle słyszymy, gdzie są rymy, a jeśli nie słyszymy, wspieramy się formą słów. Przeanalizowawszy taki utwór, możemy zdecydować, czy chcemy zachować strukturę oryginału czy też zmienić ją - wszystko zależnie od naszych potrzeb i ograniczeń języka polskiego.  
Przykładowo język angielski może poszczycić się całym mnóstwem słów z akcentem oksytonicznym, i to nie tylko słów jednosylabowych. Już samo to stanowi problem w j. polskim, który repertuar słów jednosylabowych ma mocno ograniczony.
Przy zmianie najważniejsze jest jednak zachowanie sensowności tego, co robimy. Tak, abyście na każde "dlaczego?" umieli odpowiedzieć "dlatego" i żeby nie była to odpowiedź typu "bo tak" ;).

#### Piosenki wschodnie
Jak natomiast sprawa ma się z piosenkami wschodnimi? Poezja wschodnia opiera się głównie na rytmie (kojarzycie ot choćby haiku?). Niewiele osób zdaje sobie sprawę, że rymy to wymysł zachodu. Piosenki chińskie, japońskie, koreańskie etc. w 99% przypadków się nie rymują.

Ale, ale! - ktoś mi powie. - Przecież to wszystko tak dobrze brzmi i założę się o własną babcię, że tam jest rym!

Spójrzmy na język japoński, bo z takimi piosenkami pracujemy najczęściej. Japoński ma pięć samogłosek: a, e, i, o u oraz jedną półsamogłoskę: n. Poza tym składa się z sylab. Zerknijcie [na ten schemat] (https://i.imgur.com/O0PwybI.png). Każda sylaba kończy się samogłoską. Samogłoski tworzą rymy. Oznacza to, że możliwość przypadkowego zrymowania się jakichś słów jest niezwykle wysoka. I tak zwykle bywa.

Co w takim wypadku możemy zrobić z piosenką, która w oryginale nie ma struktury rymowej?
Cóż, stworzyć ją. Nie zawsze jest to łatwe. Przy pełnych piosenkach zwykle jesteśmy w stanie określić, które fragmenty to zwrotki, które refreny, które przejścia, możemy policzyć liczbę wersów na zwrotkę i zastosować pasujące nam rymy (np. abba, abab, aabb, aax-bbx, aac- bbc). Przy openingach i endingach, które zwykle są wycinkiem pełnej piosenki, może się zdarzyć, że takich powtórzonych motywów będzie znacznie mniej.
Naszym zadaniem jest wyłapać wszystkie, które się powtarzają i nadać im podobną strukturę, a następnie sprawdzić, czy z resztę piosenki da się zrobić nawet jeśli nie to samo, to coś podobnego. Im więcej powtarzających się struktur, tym lepiej dla nas, tekściarzy.

### Ćwiczenia
W ramach ćwiczeń proponuję przeanalizować struktury rymowe piosenki:
a) polskiej
b) zagranicznej zachodniej
c) zagranicznej zachodniej, która ma również polską wersję (jest tego multum, ale jeśli nic nie przyjdzie wam do głowy, to zawsze można zerknąć na piosenki z bajek)
d) wschodniej z fanowskim tłumaczeniem melicznym (tutaj możecie sprawdzić, jakie struktury wybrał tłumacz i zastanowić się dlaczego)[/justuj]

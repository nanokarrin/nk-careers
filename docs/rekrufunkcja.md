---
id: rekrufunkcja
title: Rekrutacja na osobę funkcyjną
---

:::tip
Można być członkiem NK z rolą funkcyjną i bez roli projektowej.
:::


### Kawalerzysta (Kawaleria Konwentowa)
Osobą decyzyjną jest Orzecho (DiscordID: orzecho). Jeśli jesteś zainteresowany, odezwij się bezpośrednio do Orzecha.

### Przewodnik, senpai (Rekrutacja)
Osobą decyzyjną jest Pchełka (DiscordID: pchelka). Uwaga, to rola wyłącznie dla członków NK. Jeśli jesteś zainteresowany, odezwij się bezpośrednio do Pchełki.  

*Zadaniem Przewodnika* jest przeprowadzać rozmowy wstępne z osobami, które chcą dołączyć do grupy oraz ewaluacje (rozmowy głosowe), by sprawdzić, czy nasz nowy członek ma wszystko, czego potrzebuje, by zacząć działać i czy udziela się w grupie. Przewodnikiem powinna być osoba komunikatywna, która w grupie spędziła minimum rok.  

*Zadaniem Senpaia* jest wsparcie nowej osoby jako kumpel, wciągnięcie nowego członka do istniejących już podgrupek społecznościowych, by zwiększyć integrację, a także służenie za pierwsze źródło informacji w razie wątpliwości. Senpai nie musi wiedzieć wszystkiego o grupie, ale powinien wiedzieć, gdzie takich informacji szukać dalej i pomóc nowemu członkowi je odnaleźć. Senpaiem może być każdy pełnoprawny członek (nie kouhai).


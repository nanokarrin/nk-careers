---
id: dialogi2
title: Długość kwestii
sidebar_label: Długość
---

## Skracanie

Prawdopodobnie czynność, z którą będziesz mieć do czynienia bardzo często. Kwestia została przetłumaczona perfekcyjnie, pięknie brzmi, pasuje do postaci i ogólnie jest doskonała. Tylko jest dwa razy dłuższa od oryginału i trzeba ją skrócić, bo aktor albo nie da rady jej wypowiedzieć, albo będzie brzmiał jak chomik na kofeinie.

Jeśli kwestia jest dużo dłuższa niż powinna, najlepiej podejść do jej napisania jeszcze raz, od nowa. Podchodząc do niej z zupełnie innej strony. Być może da się to samo powiedzieć w inny sposób, użyć jakiegoś zmyślnego związku frazeologicznego, idiomu albo czegokolwiek innego? Być może właśnie trzeba usunąć jakieś wyrafinowane wyrażenie, które jest dosyć długie i zastąpić je prostszym.

> Choćbym nie wiem, jak się starał, nie jestem w stanie przenieść tego kamienia.

Skrócone:

> Mimo starań, nie mogę przenieść tego kamienia.

Każda kwestia jest nieco inna i trudno znaleźć uniwersalne reguły, ale jest kilka rzeczy na które możesz zwrócić uwagę. Przede wszystkim, postaraj się wycisnąć esencję kwestii - to, co dana postać chce przekazać, bez całej otoczki. I pomyśl w jaki inny sposób możesz to przekazać. Nie musisz robić tego, w ten sam sposób, co oryginał. Każdy język ma swoją specyfikę. Pomyśl, jak ty byś to powiedział/powiedziała.

I kilka rad/sztuczek dla kwestii, które w sumie sa tylko trochę dłuższe i wolelibyśmy już nie wymyślać nowych. Postarajmy się zidentyfikować wypełniacze - słowa lub wyrażenia, które są jak najbardziej na miejscu, ale zdanie mogłoby się bez nich obyć, nie tracąc na zrozumiałości. Często będą to przymiotniki i przyimki, a także wyrażenia "dodające kolorytu". Ich usunięcie w dosyć prosty sposób skróci kwestię.

> Wówczas ruszył wprost na smoka i swą lśniącą kopią przeszył go na wylot, podczas gdy jego giermek ordynarnie się obijał.

Po skróceniu:

> Ruszył na smoka i przeszył go kopią, a jego giermek się obijał.

To prawda, że po skróceniu zdanie nie brzmi już tak dobrze jak wcześniej. Dlatego trzeba poszukiwać złotego środka pomiędzy tym, co brzmi dobrze, a tym co mieści się w kłapach.

## Wydłużanie

Jeśli skracanie masz już opanowane, to wydłużanie będzie bardzo proste. Po prostu rób to, co opisałem powyżej, tylko na odwrót :) Brzmi to jak żart, ale w zasadzie opisuje sedno. Jeśli kwestia jest za krótka, możesz ująć ją w inny sposób, np. zamieniając proste wyrażenie na dłuższy idiom, dodając wypychacze pokroju "wówczas", "natomiast", dodatkowe elementy opisu czy inne specyficzne wyrażenia. Uważaj tylko, by zbytnio nie rozdmuchać kwestii, by nie okazało się, że będzie z kolei zbyt długa, albo zbyt zawiła, przez co zatrze się jej "esencja". Powyższe przykłady czytane od dołu mogą służyć jako przykłady wydłużania kwestii.

## Na koniec

Z moich obserwacji wynika, że mniej oznacza więcej :) Krótsze kwestie pozwalają aktorom na więcej swobody w ich odgrywaniu. Jeśli masz dwa pomysły na kwestię, jeden odrobinę za długi, drugi odrobinkę za krótki, wybieraj raczej ten ostatni. Aktor mając nieco przestrzeni, może coś podziałać głosem, np. przedłużyć bądź zaakcentować jakiś fragment, skutecznie kompensując długość kwestii. Jeśli natomiast musi przyspieszyć, by zmieścić się w kłapach, będzie się skupiał na tym i ciężej mu będzie skoncentrować się na grze aktorskiej.

Oczywiście mowa o drobnych różnicach, akceptowalnych w stosunku do długości oryginalnej scenki. Zbyt krótkie również nie są mile widziane, bo wówczas aktor będzie musiał cały czas mówić wolno, niczym wioskowy głupek mający problem ze składaniem słów w zdanie.
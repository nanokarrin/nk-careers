---
id: rekrurealizator
title: Rekrutacja na realizatora dźwięku
---

## Osoby decyzyjne
Opiekunami rekrutacji, czyli osobami sprawującymi pieczę nad jej przebiegiem i regulaminem, są Razjel (Discord: @.razjel) i Orzecho (Discord: @orzecho). 

## Przebieg rekrutacji
Rekrutacja na Realizatora Dźwięku polega na zmiksowaniu jednej piosenki i zaprezentowaniu jej w specjalnie do tego założonym poście. Post rekrutacyjny załóż na Discordzie NK na forum "Rekrutacja". Tam też możesz podejrzeć, jak robili to inni lub poprosić o pomoc Przewodnika.

Piosenka może być dowolna, powinna jednak dawać Ci pole do popisu i prezentacji Twoich umiejętności. Poza gotowym miksem powinieneś umieścić także materiały wejściowe, żeby można było posłuchać, z czym musiałeś pracować. Możesz też wybrać jeden ze starych projektów NK, do których materiały udostępniamy poniżej.

1. Aldnoah.Zero ED 2 - aLIEz: https://drive.google.com/file/d/1o_-eI6PC0m26A14EXOraZQRErrawMYiS/view?usp=sharing
2. Bleach OP1 – Asterisk: https://drive.google.com/file/d/1uiI9oFwhSxK3cV7MKvo5Sz53o4p0gjIB/view?usp=sharing
3. Itazura na Kiss – Jikan yo Tomare: https://drive.google.com/file/d/1taB3g7T_pButVH3TygOavsBTRH9fICnJ/view?usp=sharing

## Jaki może być wynik rekrutacji?
### Pozytywny
Otrzymujesz rangę "Realizator Dźwięku". 

:::tip
Ludzi zajmujących się wszelkim dźwiękiem zapraszamy na serwer [Nanokarrin of Sound (NoS)] (https://discord.gg/A58hjBb). Tutaj możecie wrzucać miks do oceny i zasięgnąć rady.
:::

### Negatywny
Nie otrzymujesz rangi. Musisz jeszcze popracować nad warsztatem.
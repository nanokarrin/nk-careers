---
id: realizacja2
title: Kurs Realizatorów Fandubbingowych
sidebar_label: Kurs Realizatorów Fandubbingowych (KRF)
---

## Kurs Realizatorów Fandubbingowych
**Kurs pokrywa wstęp do miksowania i masteringu projektów wokalnych w kontekście piosenek fandubbingowych.**  

Kurs przeznaczony jest głównie dla realizatorów dźwięku, dźwiękowców i reżyserów piosenek. Osoby aspirujące do wdrożenia się w świat post-produkcji znajdą dobry wstęp do podstawowych technik miksowania i pracy z piosenkami fandubbingowymi. Doświadczeni realizatorzy dźwięku też mogą znaleźć coś co ich zainteresuje. Oczywiście zachęcam do przejrzenia kursu wszystkich zainteresowanych śpiewaniem, nagrywaniem i post-produkcją nagrań wokalnych.

### Spis treści:
♬ Prezentacja 1 - [Co robić przed rozpoczęciem miksowania?](/docs/realizacja2#co-robić-przed-rozpoczęciem-miksowania)  
♬ Prezentacja 2 - [Miksowanie cz. 1](/docs/realizacja2#miksowanie-cz-1)  
♬ Prezentacja 3 - [Miksowanie cz. 2](/docs/realizacja2#miksowanie-cz-2)  
♬ Prezentacja 4 - [Mastering](/docs/realizacja2#mastering)  

Dodatkowe materiały budujące na wcześniej zaprezentowanych pojęciach i technikach procesu post-produkcji znajdują się w prezentacjach [5](/docs/realizacja2#post-produkcja---miks-cz-1) i [6](/docs/realizacja2#post-produkcja---miks-cz-2).

♬ Prezentacja 5 - [Post-produkcja – miks cz. 1](/docs/realizacja2#post-produkcja---miks-cz-1)  
♬ Prezentacja 6 - [Post-produkcja – miks cz. 2](/docs/realizacja2#post-produkcja---miks-cz-2)  
♬ Prezentacja 7 - [Post-produkcja – master](/docs/realizacja2#post-produkcja---master)  
♬ Prezentacja 8 - [Demo miksowania piosenki](/docs/realizacja2#demo-miksowania-piosenki)  
♬ Prezentacja 9 - [Przygotowanie balansu głośności](/docs/realizacja2#przygotowanie-balansu-głośności)  
♬ Prezentacja 10 - [Edycja nagrań przed miksem](/docs/realizacja2#edycja-nagrań-przed-miksem)  
♬ Prezentacja 11 - [Proces miksowania cz. 1](/docs/realizacja2#proces-miksowania-cz-1)  
♬ Prezentacja 12 - [Proces miksowania cz. 2](/docs/realizacja2#proces-miksowania-cz-2)  
♬ Prezentacja 13 - [Odsłuch końcowego miksu](/docs/realizacja2#odsłuch-końcowego-miksu)  

Wszystkie slajdy kursu [PDF](https://www.slideshare.net/MaciekTomczak/kurs-realizatorw-fandubbingowych-2016)  
## Co robić przed rozpoczęciem miksowania?

Ta prezentacja przedstawia różne pytania, które można zadać przed podjęciem się projektu wokalnego i różne zadania dla realizatora przed rozpoczęciem procesu miksowania.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1leZXEvi2zc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Slajdy dostępne na [https://www.slideshare.net/Razjel/1-jak-zaczac-102889350](https://www.slideshare.net/Razjel/1-jak-zaczac-102889350)  

## Miksowanie cz. 1

Część pierwsza wprowadzenia do procesu miksowania.

<iframe width="560" height="315" src="https://www.youtube.com/embed/IYFqGaLBzRU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Slajdy dostępne na [https://www.slideshare.net/Razjel/2-miksowanie1-102889351](https://www.slideshare.net/Razjel/2-miksowanie1-102889351)  

## Miksowanie cz. 2

Część druga wprowadzenia do procesu miksowania.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1gkmEikw7U0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Slajdy dostępne na [https://www.slideshare.net/Razjel/3-miksowanie2-102889352](https://www.slideshare.net/Razjel/3-miksowanie2-102889352)  

## Mastering

Ta prezentacja przedstawia wstęp do procesu masteringu w kontekście projektów fandubbingowych. Warto pamiętać, że wszystkie zmiany nakładane na połączone nagrania wokalne z wcześniej zmiksowanym podkładem muzycznym (instrumentalem) powinny być delikatne. Nakładając różne efekty na zmiksowaną ścieżkę dźwiękową można bardzo łatwo zniszczyć jej jakość. Jeżeli słyszysz o masteringu po raz pierwszy to polecam zawęzić edycje miksu w tej części do zmian głośności tak, aby wasze miksy były porównywalne do innych dobrych produkcji fandubbingowych.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0vjwdK7CbSE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Slajdy dostępne na [https://www.slideshare.net/Razjel/4-mastering-102889354](https://www.slideshare.net/Razjel/4-mastering-102889354)  

## Post-produkcja - miks cz. 1

Ta prezentacja buduje na wcześniej poruszonych aspektach procesu miksowania piosenek.

<iframe width="560" height="315" src="https://www.youtube.com/embed/N80-lICiyTE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Slajdy dostępne na [https://www.slideshare.net/Razjel/5-postprod1-102889356](https://www.slideshare.net/Razjel/5-postprod1-102889356)  

## Post-produkcja - miks cz. 2

Ta prezentacja buduje na wcześniej poruszonych aspektach procesu miksowania piosenek.

<iframe width="560" height="315" src="https://www.youtube.com/embed/qVhMT9m3kx4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Slajdy dostępne na [https://www.slideshare.net/Razjel/6-postprod2-102889357](https://www.slideshare.net/Razjel/6-postprod2-102889357)  

## Post-produkcja - master

Ta prezentacja jest krótkim dodatkiem do wcześniejszej części o procesie masteringu piosenek.

<iframe width="560" height="315" src="https://www.youtube.com/embed/T0pDYazBWfY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Slajdy dostępne na [https://www.slideshare.net/Razjel/7-postprod3-102889358](https://www.slideshare.net/Razjel/7-postprod3-102889358)  

## Demo miksowania piosenki

Filmiki przedstawiają miksowanie wersji piosenki "No pain, No game" śpiewanej przez [Ayo](https://www.youtube.com/user/DarkAyO666). Ten projekt jest dobrym przykładem paru częstych problemów, które pojawiają się podczas miksowania piosenek fandubbingowych. Są w nim też chórki, więc jest wiele możliwości na demonstracje różnych technik przydatnych podczas post-produkcji. Użyty DAW to Logic 9.

W linku są ścieżki wokalne, instrumental, oryginalna piosenka i przygotowany miks:
https://mega.nz/#!iBMDiQoJ!cIqtdZFUF9cjxyb9ORlZZVvIA-Z_xSEraeM7PrMixgI

Dołączam również wszystkie nagrania użyte w miksie, więc jeżeli będziecie chcieli się sprawdzić w miksowaniu tej piosenki to macie taką możliwość. Dobre wyzwanie jeżeli nigdy wcześniej nie pracowaliście z tak tłocznym miksem i paroma ścieżkami wokalnymi naraz.

Ze względu na brak dużej ilości wolnego czasu starałem się przygotować miks dość szybko, dlatego miksowałem od razu do finałowej wersji tego samego dnia. Filmiki nie były edytowane, więc pokazują cały proces bez zatrzymania.

### Przygotowanie balansu głośności
Pierwsza część pokazuje bardzo szybki balans głośności, żeby zapoznać się z piosenką.
<iframe width="560" height="315" src="https://www.youtube.com/embed/tBipOZIqN-4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Edycja nagrań przed miksem
Tutaj przedstawiony jest proces edytowania nagrań przed miksowaniem.
<iframe width="560" height="315" src="https://www.youtube.com/embed/ESS3eNXUcGg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Proces miksowania cz. 1
Ta część przedstawia mój pierwszy szybki miks. Przed nagrywaniem uporządkowałem nagrania w grupy w moim DAW. Pierwsze 20 sekund pokazuje uporządkowane nagrania.
<iframe width="560" height="315" src="https://www.youtube.com/embed/S4WB-bqFk3Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Proces miksowania cz. 2
Po przerwie na odświeżenie uszu zanotowałem poprawki i zabrałem się do ostatnich zmian w miksie.
<iframe width="560" height="315" src="https://www.youtube.com/embed/lICPHSQiCSo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Odsłuch końcowego miksu
Ta część przedstawia porównanie wersji przed i po miksowaniu.
<iframe width="560" height="315" src="https://www.youtube.com/embed/GXbMSTm5Lic" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



~[Razjel](https://maciek-tomczak.github.io/) 👋🏻

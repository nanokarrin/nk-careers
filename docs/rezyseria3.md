---
id: rezyseria3
title: Check-lista przed zaakceptowaniem miksu do realizatora
sidebar_label: Check-lista przed zaakceptowaniem miksu do realizatora
---

- [ ] Czy przesłuchałem miks przynajmniej 2 razy (notatki)?
- [ ] Czy miks mi się podoba? Tak, dlaczego? Nie, dlaczego?
- [ ] Czy finałowy miks jest w STEREO?
- [ ] Czy finałowy miks jest MP3 320kbps?
- [ ] Czy głośność partii wokalnych jest zbalansowana?
- [ ] Czy pogłos wokalu jest odpowiedni do kontekstu piosenki?
- [ ] Czy jakość miksu jest zbliżona do innych projektów NK z ostatniego roku/miesięcy?
- [ ] Czy miks jest odpowiednio wysterowany, nie jest za cichy?
- [ ] Czy przesłuchałem miks na drugi dzień na świeże uszy?

---
id: tekst4
title: Akcenty
sidebar_label: Akcenty
---

### Co to ten akcent?
Akcent to wyróżnienie sylaby w słowie, słowa w zdaniu bądź nuty w takcie (o taktach dowiesz się z kolejnych lekcji).

Każdy język ma swój charakterystyczny styl akcentowania. Dla przykładu w języku polskim akcentujemy z reguły drugą sylabę od końca wyrazu, a w języku japońskim akcenty w ogóle nie występują (można dowolnie akcentować wyrazy). Akcenty narzucane przez brzmienie języka będziemy nazywać akcentami werbalnymi.

Każda piosenka ma również swój charakterystyczny styl akcentowania - melodia, podobnie jak mowa, ma swoje "sylaby" oraz "słowa", które tworzą coś na kształt zdań i akapitów - mowa o nutach i taktach. Zasady akcentowania melodii określa jej metrum. Akcenty narzucane przez melodię nazywamy akcentami muzycznymi.

### Rodzaje akcentów
Spójrzmy teraz jakie na jakie rodzaje akcentów muzycznych i werbalnych możemy się natknąć.

##### Akcenty muzyczne (narzucane przez melodię):
* Akcent metryczny – narzucany przez metrum (takt) piosenki; bardzo mocny
* Akcent grupowy – na pierwszą nutę grup powstałych z rozdrobnienia dźwięków; bardzo słaby
* Akcent dynamiczny – mocniej, głośniej
* Akcent meliczny – wyżej
* Akcent rytmiczny – dłużej

##### Akcenty werbalne (narzucane przez gramatykę i logikę):
* Akcent gramatyczny wyrazowy) – leży na sylabach w słowie; odpowiada za naturalne brzmienie słów
* Akcent logiczny zdaniowy – leży na słowach w zdaniu; odpowiada za uwydatnienie sensu wypowiedzi

### Magiczne połączenie
Pisząc tekst piosenki, musimy być świadomi tego, że stykają się w nim dwie płaszczyzny - werbalna oraz muzyczna. Obecność warstwy muzycznej sprawia, że musimy układać wyrazy i zdania w tekście tak, by na akcenty narzucane przez muzykę padały odpowiednie sylaby i słowa. Jeśli nam się to nie uda, nastąpi tzw. przeakcentowanie słowa i będzie ono brzmiało nienaturalnie. Przeakcentowanie można porównać do błędu ortograficznego w tekście pisanym - jasno pokazuje braki w elementarnym wykształceniu autora.

### Więcej o akcentach gramatycznych (wyrazowych)
##### Podstawowe zasady akcentowania w języku polskim
1. Akcent w języku polskim w większości przypadków kładziemy na drugiej sylabie od końca. Jest to tak zwany akcent główny. Na przykład:  
**ma**ma, pa**ra**sol
2. Jeśli słowo posiada więcej niż 3 sylaby, na co drugą sylabę od akcentu głównego padają tak zwane akcenty pomocnicze (poboczne), lżejsze od akcentu głównego. Akcent poboczny nie jest akcentem wyrazowym a zestrojowym - zdarza się, że pada na proklitykę. Na przykład:  
 *ma*mu**sień**ka, *za*ła**ma**nie po**go**dy, *nie* po**lu**bił

3. Końcówki przypuszczeń -bym, -byście, -byśmy etc.) oraz końcówki czasowników czasu przeszłego (-śmy  i -ście) są traktowane tak, jakby ich nie było. Akcent kładziemy w takich wyrazach na drugą sylabę od tych końcówek. Na przykład:   
zro**bi**libyście
4. Zbitki nie + jednosylabowy czasownik są akcentowane jak jeden wyraz – akcent pada w nim na drugą sylabę od końca, czyli partykułę nie. Na przykład:  
**Nie** rób tego!


##### Zaawansowane zasady akcentowania w języku polskim - atony
:::warning
Uwaga! Pomiń ten temat, jeśli dopiero zaczynasz przygodę z tekściarstwem. Wróć do niego jako młodszy tekściarz!
:::

###### Atony
Istnieją wyrazy, która nie posiadają własnego akcentu - tak zwane atony (gr. atonos = nieakcentowany). W języku polskim atonami są drobne, powszechnie występujące słówka i przyrostki nazywane, które tworzą zestroje akcentowe z sąsiadującymi wyrazami. Nazywamy te słówka klitykami, a dokładniej - enklitykami, jeśli stoją przed wyrazem, z którym tworzą zestrój akcentowy i proklitykami jeśli stoją za nim. Na klityki nie kładziemy akcentu, a słowa należące do  ich zestroju akcentowego akcentujemy normalnie.

###### Enklityka
Enklityka to wyraz nieposiadający własnego akcentu, lecz tworzący zestrój akcentowy z wyrazem ją poprzedzającym. W języku polskim enklitykami są  jednosylabowe formy fleksyjne zaimków osobowych (mnie, ją, cię, nas, etc.) oraz zaimek zwrotny (się).
Enklityki występują w tekstach naprawdę często.  
Weźmy choćby krótki przykład z piosenki o miłości:
> [„Proszę **cię**, nie odwracaj **się**”] (https://youtu.be/AlvkVChv4dY?t=17s) - Kimi ni Todoke opening 1

Ciekawostka: Końcówki wspomniane punkcie 3. zasad podstawowych również są enklitykami.

###### Proklityka
Proklityka to wyraz nieposiadający własnego akcentu, lecz tworzący zestrój akcentowy z wyrazem następującym po nim. Ogólnie proklitykami są jednosylabowe: przyimki (na, pod, bez, u, do etc.), spójniki (i, więc, lub etc.) oraz partykuła "nie".  
Na przykład:

> Nie znalazł nas i poszedł na taras.

Jak widzisz partykuła "nie", spójnik "i" oraz przyimek "na" nie posiadają własnych akcentów, ponieważ są proklitykami, a zaimek osobowy "nas" nie ma akcentu, bo jest enklityką.

Występuje jednak pewien wyjątek: kiedy wyraz następujący po domniemanej proklityce to jednosylabowy zaimek lub czasownik, wtedy wyrazu tego nie traktujemy jak proklityki, a zamiast tego oba wyrazy traktujemy jak jedno słowo – akcentując drugą sylabę od końca:
> Nie ma u mnie imprezy!

"Nie ma" akcentujemy jak jeden wyraz, podobnie "u mnie". Swoją drogą "mnie" jest zaimkiem osobowym, czyli enklityką, więc i tak nie może mieć własnego akcentu!
Ciekawostka: Wspomniana w punkcie 4. zasada dotyczy właśnie wyjątku proklityki.

###### Zapamiętaj!
Proklityki i enklityki to wyrazy jednosylabowe.
Oboczność proklityki również występuje tylko w parze z wyrazami jednosylabowymi

### Ćwiczenia

Z akcentami jest jak z muzyką w radiu - niby nie słuchasz, nie uczysz się, a jednak potrafisz śpiewać z wokalistą jak przyjdzie co do czego. Zwykle nie mamy problemu, by poprawnie akcentować wyrazy w ojczystym języku. Sprawa komplikuje się, kiedy przychodzi do akcentów niepoprawnych - rzadko je słyszymy, są więc jak zamaskowane pułapki, w które wpadamy. Świadome osłuchanie się ze złym akcentowaniem i przyklejenie mu łatki "NIE" ułatwi nam zakodowanie struktury akcentowej języka polskiego :).

##### Ćwiczenie 1
Weź do ręki swoją ulubioną książkę (ostatecznie książkę, którą akurat masz pod ręką, niech stracę) i znajdź opis na około pół strony. Usiądź wygodnie, włącz nagrywanie głosu (na dyktafonie w komórce, w programie na komputerze - obojętnie) i przeczytaj opis na głos. Następnie przeczytaj dokładnie ten sam fragment, ale tym razem akcentuj wszystkie wyrazy na ostatnią sylabę. I wreszcie przeczytaj fragment, akcentując pierwszą sylabę w każdym słowie.

Nie staraj się gnać. Jeśli uważasz, że coś źle przeczytałeś albo akcent wskoczył nie tam, gdzie go chciałeś, cofnij się, powiedz to jeszcze raz. Smakuj akcent w ustach, słuchaj siebie, jak wypowiadasz słowa.

Po zakończeniu czytania przesłuchaj plik. Powinieneś zauważyć "nienaturalność" przy drugim i trzecim czytaniu, przy czym drugie czytanie pewnie będzie brzmiało jak podróbka francuskiego :). Jeśli udało ci się utrzymać w miarę jednostajne tempo czytania, możesz spróbować puścić trzy nagrania równocześnie i słuchać różnic.

##### Ćwiczenie 2
Włącz piosenkę, która ostatnio wpadła Ci w ucho. Koniecznie taką, której tekstu nie rozumiesz (coś sobie śpiewają, fajnie brzmi, ale o czym?  kij wie). Wybierz jakiś fragment, wers lub dwa, i spróbuj wstawić w linijkę polskie słowa, których akcentowanie pasuje do muzyki.
Słowa nie muszą się łączyć w spójną całość, ćwiczenie ma na celu jedynie przypasowanie odpowiednich akcentów werbalnych do akcentów muzycznych (coś jak układanie puzzli :)). Dlatego ważne jest, żeby nie brać kilku wersów z rzędu - mózg od razu zacznie szukać połączeń i będziecie skupiać się na utrzymaniu ciągłości myśli zamiast na akcentowaniu.

##### Ćwiczenie 3
Wybierz jakąś dobrze znaną polską piosenkę (polecam tutaj wszelkie Pixary i Disneye) i poproś znajomego, żeby usunął z co drugiego wersu jakieś słowo - najlepiej czasownik, przymiotnik, rzeczownik lub przysłówek. Następnie spróbuj uzupełnić luki słowami innymi niż w oryginale, a jednak posiadającymi takie samo akcentowanie i sens w kontekście. To dobry moment, by puścić wodze fantazji i stworzyć coś zabawnego ;). Nie zwracaj uwagi na rymy!

##### Ćwiczenie 4
Swego czasu w NanoKarrin nie przywiązywało się tak wagi do akcentów. W ramach ćwiczeń posłuchaj kilku starszych piosenek i spróbuj znaleźć błędy.
Ułatwisz sobie zadanie, jeśli najpierw przeczytasz na głos tekst piosenki (i nagrasz), traktując go jak zwykłą prozę. Możesz też przesadnie podkreślać poprawne akcenty, co potem da lepszy kontrast z wersją z YT.

https://www.youtube.com/watch?v=LGl9wjGUqmo
https://www.youtube.com/watch?v=EqiH0voaGcQ
https://www.youtube.com/watch?v=D04kMxj88UA
https://www.youtube.com/watch?v=2ui342VkLFw
https://www.youtube.com/watch?v=WCKrlMql4ls

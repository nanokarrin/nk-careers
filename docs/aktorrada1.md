---
id: aktorrada1
title: Mikrofon
sidebar_label: Mikrofon
---

Chciałbyś zacząć podkładać głos swoim ulubionym postaciom w scenkach, ale nie wiesz od czego zacząć?

To proste – od mikrofonu! Oczywiście możesz puszczać bajki i mówić kwestie postaci na głos bez nagrywania tego. Jednak nie będziesz miał żadnego zapisu z tej zabawy głosem, a Twoje starania pójdą w eter i nigdy nie wrócą. Z kolei nagrywanie mikrofonem wmontowanym w komputer, słuchawki lub telefon może powodować różne problemy. Ich słaba jakość nagrywania może brzydko zniekształcić Twój cudowny głos albo zbierany szum może go zagłuszyć. A tego nie chcemy, prawda? Tak że warto zainwestować w mikrofon.

Jeśli to Twoje początki, to nie ma potrzeby wydawać od razu fortuny na wypasiony mikrofon. Przecież dopiero sprawdzasz, czy (fan)dubbing to Twoje pasja. Jeśli okazałoby się, że jednak nią nie jest, to potem zawsze możesz używać takich mikrofonów do komunikatorów głosowych, rozmowy przez Skype’a i tym podobnych, a kieszeń nie będzie Cię boleć od zakupu drogiego sprzętu. Mikrofony spełniające minimalne wymagania, żeby zadowalająco nagrywały głos, zwykle zaczynają się w granicach około 150 zł. Czasem osoby, które w miarę upływu lat przenoszą się na lepszy sprzęt, który jest dla nich bardziej odpowiedni z uwagi na ich doświadczenie, sprzedają swoje stare mikrofony. Takie mikrofony często dalej dobrze działają, tylko są już po prostu niepotrzebne poprzednim właścicielom, więc można polować również na tańsze, używane.

Jak chcesz poszukać na własną rękę, to jeden z członków NanoKarrin, Filipus Magnus, przygotował jakiś czas temu poradnik o mikrofonach i interfejsach audio:
- [Jaki mikrofon wybrać?](https://youtu.be/wpzK43WgMUA)

Jeśli znalazłeś kilka mikrofonów, które Cię zainteresowały, ale, na przykład, dalej masz problem z wyborem między nimi, to na [naszym serwerze Discordzie](https://discord.gg/nanokarrin) jest kanał #pytania-dubbingowe. Możesz tam poprosić o pomoc w tej sprawie. Na pewno znajdzie się tam ktoś, kto chętnie rozwieje Twoje wątpliwości.

A jeśli masz już zakupiony mikrofon, to Filipus stworzył również filmik o ustawieniach mikrofonu, który może Ci się przydać:
- [Jak ustawić mikrofon?](https://youtu.be/WT5pGO3pmpg)
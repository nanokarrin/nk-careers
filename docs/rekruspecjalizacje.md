---
id: rekruspecjalizacje
title: Specjalizacje NK
---

:::tip
Ostatnia zmiana: 27 stycznia 2021 r.
:::

## Czym są specjalizacje NK?
Specjalizacje to dodatkowe, **oznaczalne** rangi na Discordzie NK, które wskazują na konkretną posiadaną umiejętność członka NK. Ułatwiają znalezienie do projektu człowieka o sprecyzowanych umiejętnościach.

## Lista specjalizacji NK
Specjalizacje dzielą się na edytorskie i wykonawcze.

#### Edytorskie
Można o nie prosić na #zgłoszenia-problemy, ale nadanie musi zaakceptować realizator dźwięku NK.

- Miks - potrafi samodzielnie przygotować miks piosenki składający się z nagrań wokalnych i podkładu muzycznego; posiada młodszego realizatora NK lub wyżej
- Master - potrafi przygotować master miksu przygotowanego przez realizatora NK
- Tuning - potrafi poprawić nagrania wokalne przy użyciu programów korekcyjnych takich jak Celemony's Melodyne
- Timing - potrafi poprawić czas (timing) nagrań wokalnych tak, aby zgadzał się z rytmem w podkładzie muzycznym piosenki

#### Wykonawcze
Można o nie prosić na #zgłoszenia-problemy, nie wymagają akceptacji.
- Raper - ma dykcję i umiejętności wystarczające, by rapować po polsku
- Instrumentalista - potrafi przygotować dobrej jakości podkład instrumentalny pod wokal

Uwaga! Istnieją też rangi Raper NK i Instrumentalista NK. Obecnie nie prowadzimy rekrutacji na te rangi, natomiast warto pamiętać, że osoby, które je posiadają, mają umiejętności potwierdzone przez rekrutera NK.

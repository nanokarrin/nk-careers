---
id: dzwiekmuzykadziury
title: Technika łatania dziur
sidebar_label: Technika łatania dziur
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/lY_FDwxdbOA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Technika pozwalająca wykorzystać fragmenty oryginalnej ścieżki, nawet jeśli część z nich zawiera dialogi.
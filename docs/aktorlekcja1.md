---
id: aktorlekcja1
title: Rozgrzewka
sidebar_label: Rozgrzewka
---

Nawet jeśli może wydawać się, że w dubbingu używany jest tylko głos, to jest w to zaangażowany cały aparat mowy, czyli upraszczając - cała twarz. I tak jak biegacze muszą rozgrzać nogi przed startem, tak aktorzy głosowi powinni rozgrzać swoje mięśnie twarzy. Dobrze jest rozgrzać również resztę ciała, bo, na przykład, przepona znajdująca się w tułowiu też pracuje w trakcie mówienia, a nawet samego oddychania.

## Przykład 1
1. Masaż zawiasów szczęki, krążenie
2. Policzki, usta
3. Język
4. Artykulacja
5. Łamańce językowe

Wersja utrudniona dla 5. podpunktu, to trzymanie, na przykład, korka między zębami i dalsze próby wymawiania wszystkich łamańców językowych bardzo wyraźnie. Opis tych ćwiczeń z ich zaprezentowaniem masz [tutaj](https://youtu.be/PcrRKcWJ5cQ?t=829) od 13:49. Dla artykulacji i łamańców masz dodatkowo ćwiczenia w [Lekcji 2](aktorlekcja2) i [Lekcji 3](aktorlekcja3) w Szkółce aktorstwa.

## Przykład 2
1. Ćwiczenia fizyczne
 * rozgrzewka, rozluźnienie krążenia głową, biodrami
2. Rozgrzewka twarzy
 * patrz punkt 1., 2. i 3. w pierwszym przykładzie
3. Dykcja
 * bra, bre, bri, bro, bru itp., łamańce językowe
4. Ćwiczenia na przeponę – najpierw staracie się nabrać powietrze do przepony („do brzucha”), potem do płuc
 * na jednym oddechu każdy z punktów pojedynczo aż do wyczerpania powietrza nabranego za jednym razem i tak po kilka razy:
  * krótkie, przerywane „s”
  * długie, ciągłe „s”
  * krótkie, przerywane wydechy
  * długi, ciągły wydech
 * dla zwiększenia świadomości jak działa Twoja przepona:
  * połóż się na plecach, książka na brzuch i powoli wypuszczaj powietrze nabrane w wyżej wymieniony sposób
5. „Ijaaa!” zmieniając wysokość głosu – najpierw „ijaaa!” od niskiego przechodząc wyżej, a potem to samo „ijaaa!” tylko z wysokiego i stopniowo obniżaj głos
6. Rozśpiewka, czyli powtarzacie to, co słyszycie, dostosowując wysokość swojego głosu
 * [5-minutowa](https://youtu.be/YCLyAmXtpfY)
 * [8-minutowa](https://youtu.be/MsKZok6cXTI)

Jak możesz zauważyć Przykład 2 jest dłuższy i bardziej czasochłonny od Przykładu 1, bo nie skupia się wyłącznie na twarzy, a rozgrzaniu całego Twojego ciała. Wersja skrócona drugiego przykładu to wykonanie jedynie 4. i 6. podpunktu.

## Przykład 3
1. Internet ;)

Na YouTubie jest pełno rozgrzewek. Możesz również poszukać rozgrzewek wokalnych, bo w końcu i dubbingowcy i wokaliści muszą rozgrzewać swój głos.
---
id: realizacja6
title: Przydatne linki
sidebar_label: Przydatne linki
---

Poniżej znajduje się zbiór linków do różnych przydatnych materiałów, kursów i artykułów.

### Kursy

- [Dobre brzmienie w sieci (PL)](https://lhts.pl/e-kursy/dobre-brzmienie-sieci/)  

- [Mixing Secrets For The Small Studio (ENG)](https://www.cambridge-mt.com/ms)

- [Mixing Secrets Multitrack Library (ENG)](https://www.cambridge-mt.com/ms/mtk/)

### Artykuły

- [The 6 dB of Headroom for Mastering Myth Explained (ENG)](https://theproaudiofiles.com/6-db-headroom-mastering-myth-explained/)  

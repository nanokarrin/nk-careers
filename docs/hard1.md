﻿---
id: hard1
title: Sprzęt audio
sidebar_label: Czym nagrywać
---

Zacznijmy od wiedzy koniecznej do zgłębienia tematu sprzętu audio w formie krótkiego wideo. (kliknij obrazek)

[
![Kliknij i obczaj](https://img.youtube.com/vi/wpzK43WgMUA/maxresdefault.jpg)](https://www.youtube.com/watch?v=wpzK43WgMUA)
## Mikrofony i interfejsy

Czytając opisy mikrofonów, musisz przedrzeć się przez różne informacje zawarte w specyfikacji sprzętowej, dlatego spróbujemy to troszeczkę wytłumaczyć.
### Charakterytyka kierunkowa
Charakterystyka kierunkowa zależy od obudowy mikrofonu i jest wykresem, prezentującym stosunek blablabla... Najprościej mówiąc, charakterystyka kierunkowa pokazuje nam, jak dźwięk będzie zbierany z różnych kątów i odległości.

#### Charakterystyka kardioidalna
Najpopularniejsza charakterystyka kierunkowa. Najgłośniejszy sygnał zbierzemy, mając usta skierowane prosto na membranę naszego mikrofonu.   
![enter image description here](https://i.pinimg.com/originals/50/66/9b/50669bfe42fbc71428445e77153dacaa.png)

#### Charakterystyka superkardioidalna
Bardzo podobna do kardioidalnej. Zbiera głośniejszy sygnał z przodu i mniejszy z boków. Efektem ubocznym jest mocniejsze zbieranie z tyłu.   
![enter image description here](https://upload.wikimedia.org/wikipedia/commons/a/ae/Polar_pattern_supercardioid.png)

#### Shotgun
Często spotykane w mikrofonach do aparatów. Charakterystyka mocno kierunkowa, co widać na wykresie poniżej.   
![enter image description here](https://upload.wikimedia.org/wikipedia/commons/3/3a/Polar_pattern_directional.png)

Oczywiście istnieje więcej charakterystyk kierunkowych, jednakże raczej nie sprawdzą się one w fandubbingu, dlatego je pominiemy.

### Impedancja
Przy mikrofonach dynamicznych powinna być podawana ich impedancja.   
Daje nam ona pogląd na to, jakiego wzmocniania będziemy potrzebowali do napędzenia mikrofonu. Im większa impedancja, tym więcej gaina potrzebujemy. Jeśli nabędziemy mikrofon dynamiczny ze zbyt dużą impedancją jak na nasz interfejs/mikser, to sygnał będzie bardzo cichy, a do tego może uwydadnić się szum własny. Uratować może nas przedwzmacniacz, istnieją zarówno takie zasilane z interfejsu, jak np. TritonAudio FetHead (zasilany za pomocą phnatom 48V) lub Cloudlifter CL-1 (zasilany tą samą metodą co fethead), a również wzmacniacze jako pełnoprawne osobne urządzenia. Nawet wzmacniacz gitarowy potocznie nazywany piecykiem da radę wzmocnić sygnał mikrofonu dynamicznego. Jeśli nie jesteście pewni, czy będziecie w stanie napędzić jakiś mikrofon, zawsze możecie się zapytać na forach związanych ze sprzętem audio lub w supporcie producenta.

### Osprzęt do mikrofonu
Z wielu rodzajów statywów mogę zarekomendować segment statywów:   
mikrofonowych prostych, z ramieniem, stołowych oraz ramię radiowe.   
Zwróćcię uwagę na ciężar waszego mikrofonu i udźwig wybranego przez was statywu/ramienia.   
Podczas nagrywania należy unikać tompnieć o podstawę starywu, uderzenia w ramię bądź uderzenia np: biurka, na którym stoi lub do którego jest przymocowany statyw.   
Nie zapomnijcie również, żeby zaopatrzyć się w popfiltr lub gąbkę.

## Adaptacja akustyczna
Jeśli zdecydowałeś się na mikrofon pojemnościowy, to warto zminimalizować pogłos w pomieszczeniu, w którym będziesz nagrywał.
Zacznijmy od biedorozwiązań:

 - **Bałagan pomaga** Dźwięk odbija się od gładkich, pustych powierzchni, więc posprzątaj pokój dopiero po nagraniach ;)
 - **Koce, kołdry itd.** Umieść poduszki za mikrofonem, narzuć na siebie i mikrofon kołdrę oraz koce. Uwaga, będzie gorąco.
 - **Kabina akustyczna** Wykorzystaj szafę lub inny mebel, aby upchać w nim koce, kołry, poduszki, ręczniki itd. i włóż do środka mikrofon.

Istnieją też bardziej profesjonalne rozwiązania, jednakże będą one was kosztować pieniądze.   
Przykład: taki panel akustyczny stawiany za mikrofonem (choć nie sprawdzi się dobrze w każdych warunkach).
![enter image description here](https://mozos.pl/wp-content/uploads/2018/09/8-1.jpg)

Dodatkowo łapcie kilka filmików:
 - [Pianki akustyczne (15 min)](https://www.youtube.com/watch?v=1rjHDtPQqLA)
 - [Jak zbudować studio (1,5h)](https://www.youtube.com/watch?v=2LJDyFAx-Go)
 - [Budowa studia podcastowego (10 min)](https://www.youtube.com/watch?v=zRgHOwe3Taw&t=14s)

## Odsłuch
**Czym jest direct monitoring?**   
Direct Monitoring to nic innego jak odsłuch bezpośredni, nisko opóźnieniowy. Służy do odsłuchu własnego głosu w czasie rzeczywistym podczas nagrań.   
Dzięki temu o wiele szybciej można wyłapać błędy i tym samym oszczędzić czas.  Większość interfejsów/mikserów, a także część mikrofonów na USB oferuję taką opcję.   
Istnieje również opcja odsłuchu z poziomu oprogramowania, jednakże w większości przypadków takie rozwiązanie wiąże się z dużymi opóźnieniami, które utrudniają nagrania.

**Czemu nie słuchawki otwarte i czemu nie nagrywać się na głośnikach?**   
W głównej mierze chodzi o tzw. "przebicia", czyli stan, w którym dźwięk z słuchawek jest wyłapywany przez mikrofon. Od strony realizacyjnej jest to duże utrudnienie, ponieważ ciężej (lub niemożliwym jest) znaleźć fragment ciszy, z której pobiera się próbki do oszumiania, utrudnia to także późniejszą edycję. Z tego powodu do nagrywania preferowane są słuchawki o konstrukcji półotwartej lub konstrukcji zamkniętej.   
[Lista, którą możecie się sugerować przy wyborze słuchawek](https://www.audiofanatyk.pl/polecane-sluchawki-nauszne-muzyczne-i-dla-graczy/)

---
id: dialogiwstep
title: Wstęp
sidebar_label: Wstęp
---

# Witaj w szkółce dialogowej

Szkółka zbiera w sobie informacje i zagadnienia mające wprowadzić w tematykę pisania dialogów.

Całość została podzielona na lekcje, które przedstawiają kilka powiązanych ze sobą zagadnień.

W razie pytań czy wątpliwości, szkółka jest do Waszej dyspozycji. Można zadawać pytania i zgłaszać uwagi odnośnie do treści. Aktywność może prowadzić do poprawienia jakości materiałów i tworzenia kolejnych lekcji, także zachęcam!

Dostępne lekcje:

- [Lekcja 0 - Technikalia](dialogi0)
- [Lekcja 1 - Pisanie pod kłapy](dialogi1)
- [Lekcja 2 - Długość kwestii](dialogi2)
- [Lekcja 3 - Zaawansowane dopasowanie](dialogi3)
- [Lekcja 4 - Korzystanie z okazji](dialogi4)
- [Lekcja 5 - O wysławianiu się](dialogi5)
- [Lekcja 6 - Upiększanie](dialogi6)

Dodatkowe materiały:
[Udostępnianie scenariusza](dialogigdrive)

Wszystko:
[Całość i dodatkowe materiały](https://drive.google.com/open?id=0B6hZM6Yqv0Y3NV8tbGJxOUQzYW8)
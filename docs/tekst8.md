---
id: tekst8
title: Korekta tekstu
sidebar_label: Korekta tekstu
---

### Udostępnienie testu w Google Drive
Powszechną praktyką wśród tekściarzy jest wysyłanie sobie tekstów do korekty. Udostępnianie tekstu przez Google Drive to najwygodniejszy sposób. Korektor może bezpośrednio odnosić się do odpowiednich wersów czy kolorem zaznaczyć rymy. To dużo wygodniejsze niż kopiowanie odpowiedniego fragmentu i opisywanie, co z nim nie tak.  
W takim dokumencie Google na samej górze zawsze:
* napisz, kto jest autorem tekstu (nie zawsze Ty jesteś tym, który wysyła link dalej, może to być też reżyser, łatwo się wtedy pogubić)
* daj link do oryginału
* daj link do dema

Jeśli w danym dokumencie trzymasz kilka wersji piosenki, najnowszą zawsze miej na górze i oddziel ją od pozostałych wyraźną linią lub zaznaczeniem, że dalej są wersje starsze.

Pamiętaj też, że poprawianie tekstu dzieje się w iteracjach. Ty piszesz tekst, korektor odsyła uwagi. Jeśli naniosłeś poprawki i chcesz, by korektor znów na nie zerknął, napisz do korektora. Często między kolejnymi iteracjami mijają miesiące, nie można oczekiwać od korektora, by codziennie przeglądał wszystkie pliki, żeby sprawdzić, czy akurat czegoś nie dopisałeś. Proś i pytaj każdorazowo, niech Ci nie będzie głupio  :).

---
id: dzwiekwstep
title: Wstęp
sidebar_label: Wstęp
---

## Witaj w szkółce dźwiękowej

Szkółka zbiera w sobie informacje i zagadnienia mające wprowadzić w tematykę obróbki audio w projektach aktorskich.

Całość została podzielona na materiały, które prezentują rozmaite zagadnienia związane z dźwiękiem i posługiwaniem się nim w projektach. Jest to zagadnienie bardzo obszerne, dlatego zaprezentowane techniki i przykłady mogą stanowić bazę, ale warto również eksperymentować samodzielnie i odkrywać nowe narzędzia i sposoby ich wykorzystania.

W razie pytań czy wątpliwości, szkółka jest do Waszej dyspozycji. Można zadawać pytania albo zgłaszać uwagi odnośnie do treści. Aktywność może prowadzić do poprawienia jakości materiałów i tworzenia kolejnych lekcji, także zachęcam!

Serwer dźwiękowy na Discordzie:
[NanoKarrin of Sound](https://discord.gg/d39BzAK)

## Dostępne materiały

- [Słownik pojęć](dzwiekslownik)
- [Proces](dzwiekproces)
- [Podstawy pracy z dialogami](dzwiekdialogi)
- [Warsztaty dźwiękowe](dzwiekwarsztaty)
- [Zaproponuj własny temat!](https://discord.gg/d39BzAK)

### Muzyka
- [Podstawy pracy z muzyką](dzwiekmuzykapodstawy)
- [Technika łatania dziur](dzwiekmuzykadziury)
- [Łagodne zakończenie muzyki](dzwiekmuzykazakonczenie)

### Ścieżka, folley, efekty
- [Organizacja plików dźwiękowych](dzwieksciezkaorganizacja)
- [Podstawy tworzenia ścieżki](dzwieksciezkapodstawy)

## Źródła i paczki SFX

- [Freesound](https://freesound.org/) - Bardzo bogaty zestaw przeróżnych dźwięków i muzyki. 
- [Sounds Resource](https://www.sounds-resource.com/) - SFXy z różnych gier, bardzo przydatna i bogata w dźwięki stronka.  
- [YouTube Audio Library](https://www.youtube.com/audiolibrary) - Bardzo bogata stronka z muzyką, którą spokojnie można wykorzystać na Youtube bez obaw o prawa autorskie.
Posiada również dość bogaty zestaw SFXów. 
- [BBC SFX](http://bbcsfx.acropolis.org.uk/) - Mocny zastrzyk przeróżnych SFXów używanych przez stację BBC. 
- [Game Audio GDC](https://sonniss.com/gameaudiogdc19/) - Potężna paczka dźwięków z licencją także do użytku komercyjnego.
- [Notification Sounds](https://notificationsounds.com/) - Dźwięki powiadomień do aplikacji, smartfonów etc. mogą być często przydatne do imitowania różnych animowych odgłosów, które ciężko znaleźć gdzie indziej.
- [Pawlik SFX](https://mega.nz/#F!a51zxaST!kpEknqviY_W25IN_gnqtUQ) - Paczki dźwięków używane przez Pawlika.
- [Nano SFX](https://drive.google.com/drive/u/0/folders/0B4HCbiqA6f_lOUI4Si1oSmhqalU) - Paczka dźwięków od Nanohy.
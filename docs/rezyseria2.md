---
id: rezyseria2
title: Check-lista przed zaakceptowaniem nagrań od wokalisty
sidebar_label: Check-lista przed zaakceptowaniem nagrań od wokalisty
---

- [ ] Czy przesłuchałem wszystkie nagrania od wokalisty przynajmniej raz?  
- [ ] Czy nagrania są w mono?  
- [ ] Czy nagrania są plikami WAV?  
- [ ] Czy instrumental ma jakość MP3 320kbps?  
- [ ] Czy nagrania są jasno i czytelnie ponazywane?  
- [ ] Czy nagrania są poprawnie wyeksportowane (posiadają kilka sekund ciszy przed i po końcu nagrania)?  
- [ ] Czy nagrania mają jednorodną barwę?  
- [ ] Czy nagrania są wolne od przesterów?  
- [ ] Czy nagranie nie mają przesadnie dużego, nieprzyjemnego pogłosu?  
- [ ] Czy aranżacja piosenki potrzebuje zmiany (dodatkowe nagrania)?  
- [ ] Czy nagranie jest wolne od popów/trzasków?  
- [ ] Czy sybilanty są na akceptowalnym poziomie?  
- [ ] Pytania związane ze śpiewem  
	- Czy wokal jest rytmiczny?  
	- Czy wokal jest w tonacji?  
	- Czy nie ma fałszy?  
	- Czy głos jest pewny?  
	- Czy jest poprawny dykcyjnie?  
	- Czy nie występują niechciane podjazdy?  
	- Czy akcenty rozkładają się prawidłowo w muzyce?  
- [ ] Jeżeli projekt ma chórki lub jest grupówką  
	- Czy chórki są w tej samej tonacji co wokal prowadzący?  
	- Czy chórki są w czasie z wokalem prowadzącym?  
	- Czy wszystkie głosy w grupówce nagrywały się pod jedno rytmicznie i melodycznie nagrane demo wokalu przewodniego?  

Oczywiście nie zawsze musi udać się odpowiedzieć twierdząco na każde z powyższych pytań. Niemniej jednak każda odpowiedź negatywna powinna zapalać **czerwoną** lampkę i skłaniać do refleksji nad możliwymi konsekwencjami jeśli takie nagranie zaakceptujemy.

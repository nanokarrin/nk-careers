---
id: tekst0
title: Wstęp
sidebar_label: Wstęp
---

### Witaj, adepcie tekściarstwa!  
(I ty, tak, ty! Co się tak chowasz? Widzę cię!)  
Szkółka to miejsce, gdzie znajdziesz garść informacji na temat tłumaczenia piosenek. Całość podzieliliśmy na małe fragmenty, lekcje, które pomogą ci stworzyć bazę teoretyczną do pisania tłumaczeń melicznych.

Poza lekcjami udostępniamy ci również __zbiór artykułów o tłumaczeniu piosenek__
(lub tematyce bardzo zbliżonej), w naszej opinii przydatny.
### [Link do materiałów o języku polskim i tłumaczeniu melicznym](https://drive.google.com/folderview?id=0B4MhoOifw1uDfktDV2JPVGFJSkdFYzVfUDVTV3VTVk43MVVBWnJPWHpGLW9RMjVMQ3pjcDQ&usp=sharing)

### Spis treści:
♬ Lekcja 1 - [Rola tekściarza](tekst1)  
♬ Lekcja 2 - [Warsztat tłumacza](tekst2)  
♬ Lekcja 3 - [Zapis formalny](tekst3)  
♬ Lekcja 4 - [Akcent](tekst4)  
♬ Lekcja 5 - [Struktura rymowa](tekst5)  
♬ Lekcja 6 - [W poszukiwaniu rymu](tekst6)  
♬ Lekcja 7 - [Brzmienie](tekst7)  
♬ Lekcja 8 - [Korekta tekstu](tekst8)  

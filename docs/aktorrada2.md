---
id: aktorrada2
title: Popfiltr
sidebar_label: Popfiltr
---

## Dlaczego go potrzebujesz?
Gdy nagrywasz swój głos, to zamiast czystego dźwięku mogą pojawiać się niechciane, dziwne odgłosy, które brzmią jak uderzenia lub plucie w mikrofon, które nie są przyjemne dla ucha. Dzieje się tak, ponieważ są w języku polskim spółgłoski wybuchowe takie jak p, b, d i t. Dla ich lepszego zrozumienia możesz ustawić wnętrze dłoni naprzeciwko twarzy i wypowiedzieć na głos te głoski. Zrobione? Zatem pewnie poczułeś, jak podmuch powietrza uderzył w Twoją rękę. Podobnie jest z mikrofonem. Takie uderzenia nazywa się popami. Stąd też nazwa tego urządzenia – pop-filtr, bo filtruje popy.
    
W mikrofonie jest membrana, która zbiera wibracje z powietrza, ale gdy dochodzi do niej podmuch, który poczułeś na dłoni, to pojawia się ten brzydki odgłos. Do rozwiązania tego problemu służy właśnie popfiltr. Poza tym, filtruje on dodatkową część wilgoci, dzięki czemu delikatna membrana mikrofonu nie zostanie zapluta.

## Czy trzeba go kupować?
Choć najbardziej podstawowe i najtańsze popfiltry w sklepach wahają się w okolicach 30 złotych, to popfiltr można też wykonać samemu w domowym zaciszu.

## Jak go zrobić samodzielnie?
W internecie jest mnóstwo polskich jak i zagranicznych poradników, jak stworzyć popfiltr drobnym kosztem lub z rzeczy, które już prawdopodobnie masz w domu. Poniżej zamieszczone są przykładowe:
* [How-to make a Pop filter under $5(EASY)](https://youtu.be/_rs15oxOIec) – po angielsku, autor: jakeanddar. Wszystko jest ładnie zaprezentowane, więc nawet bez znajomości angielskiego powinieneś móc zrozumieć, jak zrobić popfiltr tym sposobem z samego obejrzenia filmiku. 
* [POPFILTR - ZRÓB TO SAM 🔨 - Dlaczego go potrzebujesz?](https://youtu.be/_rs15oxOIec) – po polsku, autor: Ziemniak. Pokazane są tu dwa sposoby na stworzenie popfiltra. Z drugiego sposobu nawet sama skorzystałam na początku mojej przygody z fandubbingiem, więc mogę szczerze polecić.

## Chcesz wiedzieć więcej?
Bardziej szczegółowo działanie popfiltra opisał Ziemniak w filmie podlinkowanym powyżej, więc nawet jeśli nie skorzystasz z jego poradników na temat robienia popfiltra, to jest to dobry zastrzyk wiedzy teoretycznej o popfiltrze. W opisie tego wideo są również linki do sklepowych popfiltrów, jeśli zdecydowałbyś się jednak takowy zakupić.

## Ważne!
„Mimo że posiadasz popfiltr, słyszysz popy na nagraniu? Pamiętaj o odległości filtra od mikrofonu oraz tego z jakiej Ty akurat mówisz. Balansowanie odległością rozwiąże kłopot.” – Cearme, członek NanoKarrin podczas [Warsztatów aktorskich](https://youtu.be/PcrRKcWJ5cQ)
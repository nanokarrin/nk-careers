---
id: tekst1
title: Rola tekściarza
sidebar_label: Rola tekściarza
---

### Czym zajmuje się tekściarz? Tłumaczeniem czy pisaniem tekstu do piosenki?

Prawda leży gdzieś pośrodku, ponieważ __ tekściarz ma za zadanie w swoim tekście zawrzeć zarówno tłumaczenie oryginalnego tekstu, jak i sprawić, aby to tłumaczenie dało się wyśpiewać. __ Takie tłumaczenie nazywamy tłumaczeniem melicznym.

### Na czym polega pisanie tekstów?
Na pogodzeniu ze sobą wielu istotnych elementów tak, żeby wybrać najlepsze proporcje między nimi.
Te elementy to:
* oryginalne słowa
* styl
* poprawność akcentowa
* rytm i melodia
* rymy
* śpiewalność

### Czy lepiej stracić trochę oryginalnego sensu, by zdanie lepiej brzmiało, czy może lepiej zostawić oryginalny sens, ale gorzej brzmiące zdanie?
Na to pytanie nie ma jednej dobrej odpowiedzi. Wszystko zależy od tego, jak chcecie tłumaczyć. Wolicie, żeby tekst lepiej brzmiał, czy żeby był bardziej zgodny z oryginałem? Musicie zastosować odpowiednie proporcje między poszczególnymi elementami, a te proporcje zależą od tego, co uważacie za ważniejsze. Dlatego też każdy tekściarz pisze trochę inaczej, jeden w swoich tekstach ma więcej gier słownych, inny ma bardzo dokładne tłumaczenia. To, jak chcecie pisać, to wasz wybór. Pamiętajcie jednak, żeby nie postępować skrajnie - całkowicie zmieniać wydźwięk tekstu czy nie zwracać uwagi na styl, rymy, a nawet melodię, tylko po to, żeby tłumaczenie było zgodne z oryginałem.

Na początku raczej nie dacie rady zastosować idealnych proporcji między tekstem oryginalnym a śpiewalnością czy stylem, więc polecam wybrać jedną-dwie rzeczy na których się skupicie, a razem ze wzrostem umiejętności zaczniecie zwracać uwagę na coraz więcej kwestii.

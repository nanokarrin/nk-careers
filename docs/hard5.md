---
id: hard5
title: Bank głosów 2024
sidebar_label: Bank głosów 2024
---

Nowy rok, nowy ja, więc w 2024 zaczynamy budować bank głosów aktorów i wokalistów NanoKarrin.   
Każdy członek NK może wysłać jedną próbkę (jeden plik) - audio lub wideo.    

### Format nazwy pliku:
rola - nick na Discordzie (dodatkowe informacje)   

np.    
Aktor - Iksiński (niski głos, lektor)   
Aktorka - Zetowska (wesołe role, dzieci)   
Wokalista - Igrekowski (musicale, rockowe brzmienia)   
Wokalistka - Wuwoska (smęty, delikatne piosenki)   

### Stare pliki
Zachęcamy do update’owania plików, bo te starsze niż rok będą usuwane. Zależy nam na aktualnej bazie. 

### Link
Bank znajduje się pod linkiem:   
https://drive.google.com/drive/folders/1i3rziPmfh_O6sNQTvjZXjG-EooEQOFcP?usp=sharing

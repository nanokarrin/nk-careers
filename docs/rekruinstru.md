---
id: rekruinstru
title: Rekrutacja na instrumentalistę
---

:::tip
Ostatnia zmiana: 9 grudnia 2020 r.  
:::

:::warning
Rekrutacja wstrzymana.
:::

## Osoby decyzyjne
Opiekunem rekrutacji, czyli osobą sprawującą pieczę nad jej przebiegiem i  regulaminem, jest Orzecho (DiscordID: Orzecho#1720).

## Przebieg rekrutacji
Ze względu na specyfikę funkcji nie ma sformalizowanego procesu rekrutacji. W temacie rekrutacyjnym zaprezentuj podkład do piosenki, który przygotowałeś. Ogólne wymagania to umiejętność sprawnego posługiwania się instrumentem, rozczytanie aranżacji napisanej przez kogoś lub napisanie własnej i komunikatywność. Możliwość szybkiego i skutecznego nagrania się też jest w cenie.

## Jaki może być wynik rekrutacji?
### Pozytywny
Otrzymujesz rangę Instrumentalista NK.

### Negatywny
Nie otrzymujesz rangi.

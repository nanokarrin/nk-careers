module.exports = {
  title: 'Dokumentacja NanoKarrin',
  tagline: 'Regulaminy, rekrutacja, szkółka',
  url: 'https://rekrutacja.nanokarrin.pl/',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'nanokarrin', // Usually your GitHub org/user name.
  projectName: 'Rekrutacja NanoKarrin', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'NanoKarrin',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.png',
      },
      items: [
        {to: 'docs/regulamin/regulaminogolny1', label: 'Regulamin', position: 'left'},
        {to: 'docs/rekruogolne', label: 'Rekrutacja', position: 'left'},
        {to: 'docs/szkolka0', label: 'Szkółka', position: 'left'},
        {to: 'docs/hard3', label: 'Sprzęt i próbki NK', position: 'left'},

        {
          label: 'Youtube',
          href: 'https://youtube.com/fandubbing',
          position: 'right'
        },
        {
          label: 'Discord',
          href: 'https://discord.gg/nanokarrin',
          position: 'right'
        },
        {
          label: 'Facebook',
          href: 'https://facebook.com/nanokarrin',
          position: 'right'
        },
        {
          label: 'Forum',
          href: 'https://nanokarrin.pl/forum',
          position: 'right'
        }
      ],
    }
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js')
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};

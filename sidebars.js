module.exports = {
  docs: [ 'rekruogolne',
        {
    'Scenki':  ['rekruaktor', 'rekrudzwiek', 'rekrudialogi'],
    'Piosenki': ['rekruwokal', 'rekruteksciarz', 'rekrurealizator'],
    'Obraz': ['rekruilustrator', 'rekruanimator'],
  },
  'rekrufunkcja',
  {
    type: 'link',
    label: 'Złóż rekrutację na Discordzie!',
    href: 'https://discord.gg/nanokarrin'
  }
  ],
  szkolka: ['szkolka0',
      {
		  'Szkółka sprzętowa': ['hard1', 'hard2'],
		  'Szkółka realizacji dźwięku': ['realizacja0', 'realizacja1', 'realizacja2', 'realizacja3', 'realizacja4', 'realizacja5', 'realizacja6', 'realizacja7'],
		  'Szkółka reżyserii piosenek': ['rezyseria0','rezyseria1', 'rezyseria2', 'rezyseria3'],
          'Szkółka aktorska': ['aktorspistresci', 'aktorlekcja1', 'aktorlekcja2', 'aktorlekcja3',  'aktorrada1', 'aktorrada2', 'aktorrada3', 'aktorrada4', 'aktorrada5', 'aktorfaq'],
          'Szkółka dialogowa': ['dialogiwstep', 'dialogi0', 'dialogi1', 'dialogi2', 'dialogi3', 'dialogi4', 'dialogi5', 'dialogi6', 'dialogigdrive'],
		  'Szkółka dźwiękowa': ['dzwiekwstep','dzwiekproces','dzwiekdialogi','dzwiekmuzykapodstawy','dzwiekmuzykadziury','dzwiekmuzykazakonczenie','dzwieksciezkaorganizacja','dzwieksciezkapodstawy'],
          'Szkółka tekściarska': ['tekst0', 'tekst1', 'tekst2', 'tekst3', 'tekst4', 'tekst5', 'tekst6', 'tekst7', 'tekst8']
      }
    ],
  regulamin: ['regulamin/regulaminogolny1', 'regulamin/regulaminogolny2', 'regulamin/regulaminogolny3', 'regulamin/regulaminogolny4', 'regulamin/regulaminogolny6', 'regulamin/regulaminogolny7','regulamin/regulaminogolny5', 'regulamin/definicje', 'regulamin/role', 'regulamin/konwenty'],
  probki: ['hard3','hard5', 'hard4', 'instrumentalisci']
};
